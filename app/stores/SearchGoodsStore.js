import alt from "alt-instance";
import BaseStore from "./BaseStore";
import SearchGoodsActions from "../actions/SearchGoodsActions";
import { loadState, saveState } from "./LocalStorage";


class SearchGoodsStore extends BaseStore {
    constructor() {
        super();

        this.lsState = loadState("search");
        
        if (this.lsState) {
            this.searchResults = this.lsState
        } else {
            this.searchResults = {};
        }

        this.bindListeners({
            handleUpdateSearchResults: SearchGoodsActions.UPDATE_SEARCH_RESULTS // UPDATE_SEARCH_RESULTS ?
        });
    }

    handleUpdateSearchResults(searchResults) {
        this.searchResults = searchResults;
        console.log("SearchGoodsStore searchResults", this.searchResults );

        saveState("search", this.searchResults);
    }
}

export default alt.createStore(SearchGoodsStore, "SearchGoodsStore");