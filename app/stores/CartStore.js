import alt from "alt-instance";
import BaseStore from "./BaseStore";
import CartActions from "../actions/CartActions"
import { loadState, saveState } from "./LocalStorage";

class CartStore extends BaseStore {
    constructor() {
        super();

        let lsState = loadState("cart");
        // this.cart = this.lsState ? this.lsState : [];
        // this.cartItems = [];
        this.tax = 10;

        if (lsState && lsState.cartItems) {
            // console.log("lsState && lsState.cartItems");
            this.cart = lsState.cart; // should be saved to "this"
            this.cartItems = lsState.cartItems;
            this.referrer = null;
        } else {
            // console.log("else, new CartStore");
            this.cart = {};
            this.cartItems = [];
            this.cart.deliveryPrice = 0;
            this.cart.taxSum = 0;
            this.cart.productsTotal = 0;
            this.cart.newItemId = null;
            this.cart.newItem = false;
        }

        this.deliveryPrice = 0;

        this.bindListeners({
            // handleUpdateCart: CartActions.UPDATE_CART,
            handleAddProduct: CartActions.ADD_PRODUCT,
            handleRemoveProduct: CartActions.REMOVE_PRODUCT,
            handleAddQuantity: CartActions.ADD_QUANTITY,
            handleRemoveQuantity: CartActions.REMOVE_QUANTITY,
            // handleTotalWithTax: CartActions.TOTAL_WITH_TAX,
            handleClearCart: CartActions.CLEAR_CART,
            handlesetDeliveryPrice: CartActions.SET_DELIVERY_PRICE,
            handleUpdateTaxDeliveryTotal: CartActions.UPDATE_TAX_DELIVERY_TOTAL,
            handlesetReferrer: CartActions.SET_REFERRER
        });

        this.getDeliveryPrice = this.getDeliveryPrice.bind(this);
    }

    getDeliveryPrice() {
        // object to send for delivery count
        let delivery = {};
        delivery.order = {};
        delivery.order.items = this.cartItems; 
        delivery.cdek_code = "44";
        delivery.country_code = '';

        let deliveryReq = JSON.stringify(delivery);
        console.log("delivery obj", deliveryReq);

        fetch("https://ntz.team/countdelivery", { // https://marketru.ntz.team/countdelivery  https://dev.ntz.team/countdelivery
            method: 'POST',
            body: deliveryReq
        })
            .then(data => {
                console.log('delivery success:', data);
                var extractedData = data.json();
                // var extractedData = JSON.stringify(data) 
                return extractedData;
            })
            .then(extractedData => {
                console.log("delivery count", extractedData);
                this.deliveryPrice = extractedData;
                console.log("this.deliveryPrice", this.deliveryPrice);
                return this.deliveryPrice;
            })
            .catch((error) => {
                console.error("delivery is not sent, error", error);
            });
    }

    // handleUpdateCart(products) {
    //     this.cartItems = products;
    // }

    handleAddProduct(product) {
        this.cartItems = [...this.cartItems];
        this.cart.newItemId = product.id;
        this.lastItem = {...product};

        for (let i = 0; i < this.cartItems.length; i++) {
            if (this.cartItems[i].id === product.id) {
                // console.log("same id", product.id);
                this.cart.newItem = false;
                return;
            }
        }

        this.cart.newItem = true;
        // this.cart.newItemId = product.id;
        product.quantity = 1;
        product.quantitySum = product.quantity * product.price;

        this.cartItems.push(product);
        // console.log("this cartItems.push", this);
        // let stringifiedThis =JSON.stringify(this);

        // console.log("stringifiedThis", stringifiedThis);
        saveState("cart", this);
    }

    handleRemoveProduct(id) {
        this.cartItems.splice(this.cartItems.findIndex(product => product.id === id), 1);
        saveState("cart", this.cart);
    }

    handleAddQuantity(id) {
        for (let i = 0; this.cartItems.length > i; i++) {
            if (this.cartItems[i].id === id) {
                this.cartItems[i].quantity++;
                this.cartItems[i].quantitySum = this.cartItems[i].quantity * this.cartItems[i].price; //ntz_price, price
            }
        }

        saveState("cart", this.cart);
    }

    handleRemoveQuantity(id) {
        for (let i = 0; this.cartItems.length > i; i++) {
            if (this.cartItems[i].id === id) {
                if (this.cartItems[i].quantity > 1) {
                    this.cartItems[i].quantity--;
                    this.cartItems[i].quantitySum = this.cartItems[i].quantity * this.cartItems[i].price;
                }
            }
        }

        saveState("cart", this.cart);
    }

    handleClearCart() {
        this.cart = {};
        saveState("cart", this.cart);
    }

    handlesetDeliveryPrice(price) {
        this.cart.deliveryPrice = price;

        saveState("cart", this.cart);
    }

    handleUpdateTaxDeliveryTotal() {
        // this.cart.deliveryPrice = price;

        console.log("store handleUpdateTaxDeliveryTotal fired");

        let sum = this.cartItems.map(product => product.quantitySum).reduce((a, b) => a + b, 0);
        this.cart.productsTotal = Math.round(sum * 100) / 100;;
        let taxSum = this.cart.TaxDeliveryTotal / 100 * 10;
        this.cart.taxSum = Math.round(taxSum * 100) / 100;
        let total = sum + this.cart.deliveryPrice;
        this.cart.TaxDeliveryTotal = Math.round(total * 100) / 100;  // TaxDeliveryTotal

        saveState("cart", this);
    }

    handlesetReferrer(referrer) {
        this.referrer = referrer;
    }

}

export default alt.createStore(CartStore, "CartStore");