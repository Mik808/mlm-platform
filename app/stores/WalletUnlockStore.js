import alt from "alt-instance";
import WalletUnlockActions from "actions/WalletUnlockActions";
import SettingsActions from "actions/SettingsActions";
import WalletDb from "stores/WalletDb";
import ls from "common/localStorage";

const STORAGE_KEY = "__graphene__";
let ss = new ls(STORAGE_KEY);

class WalletUnlockStore {

    constructor() {
        this.bindActions(WalletUnlockActions);
        this.state = {
            locked: true,
            passwordLogin: ss.get("settings_v3").passwordLogin || false
        };
        this.walletLockTimeout = this._getTimeout(); // seconds (10 minutes)
        this.timeout = null;

        this.bindListeners({
            onChangeSetting: SettingsActions.changeSetting
        });

        // let timeoutSetting = this._getTimeout();

        // if (timeoutSetting) {
        //     this.walletLockTimeout = timeoutSetting;
        // }
    }

    onUnlock({resolve, reject}) {
        var unlockTime = ss.get("unlockTime");
        var walletLockTimeout = this.walletLockTimeout;
        var currentTime = Math.round(new Date().getTime()/1000);

        if(unlockTime && (currentTime - unlockTime > walletLockTimeout)){
            WalletDb.onLock()
            this.setState({resolve:null, reject:null, locked: WalletDb.isLocked()})
            resolve()
        }

        this._setLockTimeout();
        if( ! WalletDb.isLocked()) {
            this.setState({locked: false});
            resolve();
            return;
        }

        this.setState({resolve, reject, locked: WalletDb.isLocked()});
    }

    onLock({resolve}) {
        if(WalletDb.isLocked()) {
            resolve()
            return
        }
        WalletDb.onLock()
        this.setState({resolve:null, reject:null, locked: WalletDb.isLocked()})
        resolve()
    }

    onCancel() {
        //this.state.reject();
        this.setState({resolve:null, reject:null});
    }

    onChange() {
        this.setState({locked: WalletDb.isLocked()})
    }


    onChangeSetting(payload) {
        if (payload.setting === "walletLockTimeout") {
            this.walletLockTimeout = payload.value;
            this._clearLockTimeout();
            this._setLockTimeout();
        } else if (payload.setting === "passwordLogin") {
            this.setState({
                passwordLogin: payload.value
            });
        }
    }


    _setLockTimeout() {
        this._clearLockTimeout();
        /* If the timeout is different from zero, auto unlock the wallet using a timeout */
        if (!!this.walletLockTimeout) {
            this.timeout = setTimeout(() => {
                if (!WalletDb.isLocked()) {
                    WalletDb.onLock()
                    this.setState({locked: true})
                };
            }, this.walletLockTimeout * 1000);
        }
    }

    _clearLockTimeout() {
        if (this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
    }

    _getTimeout() {
        return parseInt(ss.get("lockTimeout", 300), 10);
    }

    onCheckLock() {
        this.setState({locked: WalletDb.isLocked()});
    }
}

export default alt.createStore(WalletUnlockStore, 'WalletUnlockStore')
