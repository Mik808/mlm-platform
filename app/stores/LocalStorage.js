export const loadState = (storeName) => {
    try {
        const serializedState = localStorage.getItem(storeName);
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveState = (storeName, store) => {
    try {
        // debugger;
        const serializedState = JSON.stringify(store);
        localStorage.setItem(storeName, serializedState);
        // console.log("serializedState", serializedState);
    } catch (err) {
        // ingnore write erorrs
    }
};