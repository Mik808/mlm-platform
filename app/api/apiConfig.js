export const blockTradesAPIs = {
    BASE: "https://api.blocktrades.us/v2",
    BASE_OL: "https://api.blocktrades.us/ol/v2",
    COINS_LIST: "/coins",
    ACTIVE_WALLETS: "/active-wallets",
    TRADING_PAIRS: "/trading-pairs",
    DEPOSIT_LIMIT: "/deposit-limits",
    ESTIMATE_OUTPUT: "/estimate-output-amount",
    ESTIMATE_INPUT: "/estimate-input-amount"
};

export const rudexAPIs = {
    BASE: "https://gateway.rudex.org/api/v0_1",
    COINS_LIST: "/coins",
    NEW_DEPOSIT_ADDRESS: "/new-deposit-address"
};

export const settingsAPIs = {
    DEFAULT_WS_NODE: "wss://ntz.team/ws",
    WS_NODE_LIST: [
		{url: "wss://ntz.team/ws", location: "Frankfurt"}
    ],
    DEFAULT_FAUCET: "https://faucet.ntz.team",
    RPC_URL: "wss://ntz.team/ws"
};

var chain_id = "e9fdd593150113f409c7a64462472b9fb5a877f4a9859add2f89b3273f862972";
var chain_id_short = chain_id.substr(0, 8);

export const NTZchainId = {
    CHAIN_ID: chain_id,
    CHAIN_ID_SHORT: chain_id_short
}