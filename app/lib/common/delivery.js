const getDeliveryPrice = async (items) => {
    let delivery = {};
    delivery.order = {};
    delivery.order.items = items; 
    delivery.cdek_code = "44";
    delivery.country_code = '';
    let deliveryReq = JSON.stringify(delivery);
    console.log("delivery obj", deliveryReq);
    let receivedData;

    const settings = {
        method: 'POST',
        body: deliveryReq
    };

    try {
        let response = await fetch("https://ntz.team/countdelivery", settings);
        receivedData = await response.json();
        console.log("delivery response", receivedData);
    } catch (err) {
        console.log("cat err", err);
    }

    return receivedData;
}

// export const getDeliveryPrice = (items) => {
//         let delivery = {};
//         delivery.order= {};
//         delivery.order.items = items; // id and quantity
//         delivery.cdek_code = "44";
//         delivery.country_code = '';

//         let deliveryReq = JSON.stringify(delivery);
//         console.log("delivery obj", deliveryReq);

//         fetch("https://ntz.team/countdelivery", { // https://marketru.ntz.team/countdelivery  www.ryuti.net https://ntz.team/countdelivery https://dev.ntz.team/countdelivery
//             method: 'POST',
//             body: deliveryReq
//         })
//         .then(data => {
//             console.log('delivery success:', data);
//             var extractedData = data.json();
//             // var extractedData = JSON.stringify(data) 
//             return extractedData;
//         })
//         .then(extractedData => {
//             console.log("delivery count", extractedData);
//         })
//         .catch((error) => {
//             console.error("delivery is not sent, error", error);
//         });
// }