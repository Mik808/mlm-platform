import {ChainStore} from "bitsharesjs/es";
import {Apis} from "bitsharesjs-ws";
import React from "react";
import IntlStore from "stores/IntlStore";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import IntlActions from "actions/IntlActions";
import NotificationStore from "stores/NotificationStore";
import intlData from "./components/Utility/intlData";
import alt from "alt-instance";
import { connect, supplyFluxContext } from "alt-react";
import {IntlProvider} from "react-intl";
import SyncError from "./components/SyncError";
import LoadingIndicator from "./components/LoadingIndicator";
import AccountBackLink from "components/Account/AccountBackLink";
import ReactTooltip from "react-tooltip";
import NotificationSystem from "react-notification-system";
import TransactionConfirm from "./components/Blockchain/TransactionConfirm";
import WalletUnlockModal from "./components/Wallet/WalletUnlockModal";
import Footer from "./components/Layout/Footer";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import CartActions from  "./actions/CartActions";


class App extends React.Component {
    constructor() {
        super();

        // Check for mobile device to disable chat
        const user_agent = navigator.userAgent.toLowerCase();
        let isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

        let syncFail = ChainStore.subError && (ChainStore.subError.message === "ChainStore sync error, please check your system clock") ? true : false;
        this.state = {
            loading: false,
            synced: ChainStore.subscribed,
            syncFail,
            theme: SettingsStore.getState().settings.get("themes"),
            disableChat: SettingsStore.getState().settings.get("disableChat", true),
            showChat: SettingsStore.getState().viewSettings.get("showChat", false),
            dockedChat: SettingsStore.getState().viewSettings.get("dockedChat", false),
            isMobile: !!(/android|ipad|ios|iphone|windows phone/i.test(user_agent) || isSafari),
            incognito: false,
            incognitoWarningDismissed: false
        };

        this._rebuildTooltips = this._rebuildTooltips.bind(this);
        this._onSettingsChange = this._onSettingsChange.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    componentWillUnmount() {
        NotificationStore.unlisten(this._onNotificationChange);
    }

    componentDidMount() {
        try {
            NotificationStore.listen(this._onNotificationChange.bind(this));
            SettingsStore.listen(this._onSettingsChange);
            ChainStore.subscribe(() => {
                if (ChainStore.subscribed !== this.state.synced || ChainStore.subError) {
                    let syncFail = ChainStore.subError && (ChainStore.subError.message === "ChainStore sync error, please check your system clock") ? true : false;
                    this.setState({
                        synced: ChainStore.subscribed,
                        syncFail
                    });
                }
            });

            AccountStore.tryToSetCurrentAccount();
        } catch(e) {
            console.error("e:", e);
        }
        this.props.router.listen(this._rebuildTooltips);

        this._rebuildTooltips();
        SettingsStore.unlisten(this._onSettingsChange);

        if (this.props.location.query.r) {
            CartActions.setReferrer(this.props.location.query.r);
        }
    }


    _onIgnoreIncognitoWarning(){
        this.setState({incognitoWarningDismissed: true});
    }

    _rebuildTooltips() {
        ReactTooltip.hide();

        setTimeout(() => {
            if (this.refs.tooltip) {
                this.refs.tooltip.globalRebuild();
            }
        }, 1500);
    }

    /** Usage: NotificationActions.[success,error,warning,info] */
    _onNotificationChange() {
        let notification = NotificationStore.getState().notification;
        if (notification.autoDismiss === void 0) {
            notification.autoDismiss = 10;
        }
        if (this.refs.notificationSystem) this.refs.notificationSystem.addNotification(notification);
    }

    _onSettingsChange() {
        let {settings, viewSettings} = SettingsStore.getState();
        if (settings.get("themes") !== this.state.theme) {
            this.setState({
                theme: settings.get("themes")
            });
        }
        if (settings.get("disableChat") !== this.state.disableChat) {
            this.setState({
                disableChat: settings.get("disableChat")
            });
        }

        if (viewSettings.get("showChat") !== this.state.showChat) {
            this.setState({
                showChat: viewSettings.get("showChat")
            });
        }

        if (viewSettings.get("dockedChat") !== this.state.dockedChat) {
            this.setState({
                dockedChat: viewSettings.get("dockedChat")
            });
        }

    }
    
    onClick() {
        ZfApi.publish("mobile-menu", "close");
    }

    showBackBtn() {
        let pathsHide = [ "/", "/cart", "/checkout", "/search", "/success"];
        let showBackbtn = true;

        // console.log("this.props.location.pathname", this.props.location.pathname);

        for (var i = 0; pathsHide.length > i; i++) {
            if (this.props.location.pathname === pathsHide[i]) {
                showBackbtn = false;
            } else if (this.props.location.pathname.indexOf("product") !== -1 || this.props.location.pathname.indexOf("category") !== -1) {
                showBackbtn = false;
            }
        }

        // console.log("showBackbtn", showBackbtn);
        return showBackbtn;
    }

    render() {
        let {theme} = this.state;
        let content = null;
        let currentAccount = AccountStore.getState().currentAccount;
        let showFooter = this.props.location.pathname.indexOf("market") === -1;


        if (this.state.syncFail) {
            content = (
                <SyncError />
            );
        } else if (this.state.loading) {
            content = <div className="grid-frame vertical">
                <LoadingIndicator loadingText="Загружаем"/>
            </div>;
        } else if (this.props.location.pathname === "/init-error") {
            content = <div className="grid-frame vertical">{this.props.children}</div>;
        } else {
            content = (
                <div className="grid-frame vertical">                    
                    <div className="grid-block" onClick={this.onClick}>
                        <div className="grid-block vertical">
                            {currentAccount && this.showBackBtn() ? <AccountBackLink/> : null}
                            
                            {this.props.children}
                        </div>
                    </div>
                    
                    {currentAccount && showFooter && this.props.location.pathname != "/" ? <Footer synced={this.state.synced}/> : null}
                    <ReactTooltip ref="tooltip" place="top" type={theme === "lightTheme" ? "dark" : "light"} effect="solid"/>
                </div>
            );
        }

        return (
            <div className={this.state.theme}>
                <div id="content-wrapper">
                    {content}
                    <NotificationSystem
                        ref="notificationSystem"
                        allowHTML={true}
                        style={{
                            Containers: {
                                DefaultStyle: {
                                    width: "300px"
                                }
                            }
                        }}
                    />
                    <TransactionConfirm/>
                    <WalletUnlockModal/>
                    {/* <BrowserSupportModal ref="browser_modal"/ */}
                </div>
            </div>
        );

    }
}

class RootIntl extends React.Component {
    componentWillMount() {
        IntlActions.switchLocale(this.props.locale);
    }

    render() {
        return (
            <IntlProvider
                locale={this.props.locale.replace(/cn/, "zh")}
                formats={intlData.formats}
                initialNow={Date.now()}
            >
                <App {...this.props}/>
            </IntlProvider>
        );
    }
}

RootIntl = connect(RootIntl, {
    listenTo() {
        return [IntlStore];
    },
    getProps() {
        return {
            locale: IntlStore.getState().currentLocale
        };
    }
});

class Root extends React.Component {
    static childContextTypes = {
        router: React.PropTypes.object,
        location: React.PropTypes.object
    }

    componentDidMount(){
        //Detect OS for platform specific fixes
        if(navigator.platform.indexOf('Win') > -1){
            var main = document.getElementById('content');
            var windowsClass = 'windows';
            if(main.className.indexOf('windows') === -1){
                main.className = main.className + (main.className.length ? ' ' : '') + windowsClass;
            }
        }
    }

    getChildContext() {
        return {
            router: this.props.router,
            location: this.props.location
        };
    }

    render() {
        return <RootIntl {...this.props} />;
    }
}

export default supplyFluxContext(alt)(Root);
