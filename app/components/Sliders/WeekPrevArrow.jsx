import React from 'react';
import Translate from "react-translate-component";

class WeekPrevArrow extends React.Component {

    constructor(props) {
        super(props);


        this.state = {

        }
    }

    render() {
        const { className, style, onClick } = this.props;

        return (
            <div className="week-offers__arrows week-offers__prev-arrow" onClick={onClick}>
                <div className="week-offers__arrow-img"></div>
                <Translate content="categories.prev" component="div" className="week-offers__arrow-text" />
            </div>
        )
    }
}

export default WeekPrevArrow;