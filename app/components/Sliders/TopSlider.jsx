import React from 'react';
import Slider from "react-slick";

class TopSlider extends React.Component {

    constructor(props) {
        super(props);

        this.settings = {
            autoplay: true,
            dots: true,
            arrows: false,
            infinite: true,
            speed: 1000,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1,
        };
    }

    render() {
        return (
            <Slider {...this.settings} className="top-slider">
                <div className="top-slider__slide"></div>
                <div className="top-slider__slide top-slider__slide--2"></div>
                <div className="top-slider__slide top-slider__slide--3"></div>
                <div className="top-slider__slide top-slider__slide--4"></div>
            </Slider>
        )
    }
}

export default TopSlider;