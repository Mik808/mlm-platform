import React from 'react';
import Translate from "react-translate-component";

class WeekNextArrow extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

        }
    }

    render() {
        const { className, style, onClick } = this.props;

        return (
            <div className="week-offers__arrows week-offers__next-arrow" onClick={onClick}>
                <Translate content="categories.next" component="div" className="week-offers__arrow-text" />
                <div className="week-offers__arrow-img"></div>
            </div>
        )
    }
}

export default WeekNextArrow;