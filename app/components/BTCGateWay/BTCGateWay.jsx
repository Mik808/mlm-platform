import React from "react";
import AccountStore from "stores/AccountStore";
import {connect} from "alt-react";
import {ChainStore} from "bitsharesjs/es";

class BTCGateWay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           filter: "All",
           assets: [{"uia": "DECN.BTC", "external": "BTC", "description": "Deposit BTC and recieve DECN.BTC to your account"}, {"uia": "DECN.LTC", "external": "LTC", "description": "Deposit LTC and recieve DECN.LTC to your account"}],
        };
    }
    
    componentWillMount() {
    	let currentAccount = AccountStore.getState().currentAccount;
    	let from_account = ChainStore.getAccount(currentAccount);
    	console.log("from_account", from_account.get("id"));

   		// const xhr = new XMLHttpRequest();

        // xhr.open('POST', 'http://denis.local:8899/api/v1/assets', true);
        // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        // xhr.send();

        // xhr.onreadystatechange = () => {
        //     if (xhr.status == 200) {
        //         console.log(xhr.responseText);
        //         this.setState({
	    //             assets: xhr.responseText,
	    //         });
        //         // window.location = xhr.responseText;
        //     }
        //     else {
        //         console.log("Error");
        //     }
        // }
    }
    _onChangeFilter(e) {
        this.setState({
            filter: e.target.value
        });
    }

    render() {
    	let assets = this.state.assets;

    	return (
            <div className="grid-block vertical">
                <div className="gateway__bts-inner">
                    <select data-place="left" className="gateway__list" value={this.state.filter} onChange={this._onChangeFilter.bind(this)}>
                        {assets.map(assets => <option key={assets.uia} id={assets.uia} className="gateway__item">{assets.description}</option>)}
                    </select>
                    <span className="gateway__wallet">NTZ7tjPTTuH9x3TWJTDE1fi1JaLkMoMfCBL4HMyEyH2Jy1EScyWdy
                    </span>
                </div>
            </div>
        	);
    	}
    }

export default connect(BTCGateWay);