import React from "react";
import Translate from "react-translate-component";
import AccountStore from "stores/AccountStore";

let auto = require("assets/app_img/promo/auto_program.png")
let sales = require("assets/app_img/promo/stable_sales.png")
let triple = require("assets/app_img/promo/triple.png")

class Promo extends React.Component {
    constructor(props) {
        super();
        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            promos: []
        };
    }

    componentDidMount() {
        fetch('https://ntz.team/get_promo', {
            method: 'GET'
        })
            .then(data => {
                var extractedData = data.json()
                return extractedData;
            }).then(extractedData => {
                console.log("promos extractedData", extractedData);
                this.setState({ promos: extractedData });
            })
            .catch((error) => {
                console.error('promo fetch error:', error);
            });
    }

    render() {
        return (
            <section className="promo">
                <div className="promo__content">
                    <Translate className="promo__title" component="h1" content="promo.title" />

                    <ul className="promo__list">
                        <li className="promo__item promo__item--reverse">
                            <div className="promo__img-wrap">
                                <img src={auto} alt="" />
                            </div>
                            <div className="promo__inner">
                                <span className="promo__subtitle">Авто Программа</span>
                                <span className="promo__important-txt">Сроки проведения акции:  1 марта 2020 - 31 декабря 2020</span>
                                <p className="promo__text">Закрой соответствующий уровень, подтверди его на следующий месяц и получи премию согласно рангу.</p>
                                <p className="promo__text">Личный Обьем в месяц выполнения квалификации должен доставлять 3500 руб/ 400 UAH/ 50 EUR (высылать Елене Агальцовой)</p>
                                <div className="promo__links-wrap">
                                    <a className="promo__link" href="https://drive.google.com/drive/u/0/folders/10hNP4tSBL0bU3Y2jgt41CWnU8pZEmBs9" target="_blank">
                                        <Translate content="promo.download-link" />
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li className="promo__item">
                            <div className="promo__img-wrap">
                                <img src={sales} alt="" />
                            </div>
                            <div className="promo__inner">
                                <span className="promo__subtitle">Стабильные продажи</span>
                                <span className="promo__important-txt">Срок действия: с 1 апреля по 1 января 2021 года</span>
                                <p className="promo__text">
                                    <span className="promo__important-txt">Участники:</span> все партнеры компании NTZ
                                </p>
                                <p className="promo__text">Для участия в промоушене нужно 3 месяца ПОДРЯД поддержать Личный Объем продаж       физических товаров, на уровне 10 000 NTZ/ 20 000 NTZ/ 30 000 NTZ в месяц
                                    После  выполнения условий вручается подарок 1го, 2го, 3го уровня соответственно
                                    За каждый следующий (подряд) месяц, в котором подтверждается Личный Объем продаж физических товаров, вручается подарок этого уровня.
                                </p>
                                <p className="promo__text">
                                    Каждый партнер выполнивший условия промоушена, а также подтвердивший их в 4й и последующие месяцы, получает доступ к закрытому клубному магазину «Стабильные продажи» со спец. предложениями. 
                                </p>
                                <ul className="promo__inner-list">
                                    <li>Доступ к магазину действует 1 месяц и блокируется при НЕВЫПОЛНЕНИИ условий промоушена в последующий месяц</li>
                                    <li>Kомпания оставляет за собой право менять подарок в одностороннем порядке</li>
                                </ul>
                                <div className="promo__links-wrap">
                                    <a className="promo__link" href="https://drive.google.com/drive/u/4/folders/1AtMDDw6-kGgwNYY1bC4ODnCmG7pclYYe" target="_blank">
                                        <Translate content="promo.download-link" />
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li className="promo__item">
                            <div className="promo__img-wrap">
                                <img src={triple} alt="" />
                            </div>
                            <div className="promo__inner">
                                <span className="promo__subtitle">Тройной успех  (3-9-27)</span>
                                <span className="promo__important-txt">Период: календарный месяц</span>
                                <p className="promo__text">
                                    <span className="promo__important-txt">Участники:</span> все действующие партнеры NTZ
                                </p>
                                <p className="promo__text">Шаг 1️⃣: В течение 1 календарного месяца зарегистрируй 3 новых партнеров в первую линию и получи дополнительно 3000 NTZ.</p>
                                <p className="promo__text">
                                    Шаг 2️⃣: Помоги своей первой линии зарегистрировать 3 человек и получи дополнительно 9000 NTZ. 
                                </p>
                                <p className="promo__text">💫Важно: Твои партнеры в первой линии получают по 3000 NTZ каждый!</p>
                                <p className="promo__text">Шаг 3️⃣: Помоги своей второй линии зарегистрировать троих и получи дополнительно 27 000 NTZ.</p>
                                <p className="promo__text">💫Важно: Твои партнеры во первом уровне получают по 9000 NTZ, а  партнеры в втором уровне получают по 3000 NTZ.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </section >
        )
    }
}

export default Promo;