import React from 'react';
import Translate from "react-translate-component";
import { Link } from "react-router/es";
import counterpart from "counterpart";

class Footer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubcription = this.handleSubcription.bind(this);
    }

    handleInputChange(event) {
        this.setState({ email: event.target.value });
    }

    handleSubcription(event) {
        event.preventDefault();

        if (this.state.email === "") {
            alert(counterpart.translate("validation.emptyEmail"));
            return false;
        }
        const emailRegExp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if (!emailRegExp.test(this.state.email)) {
            alert(counterpart.translate("validation.invalidEmail"));
            return false;
        }

        let emailObj =  { email: this.state.email };
        emailObj = JSON.stringify(emailObj);
        // console.log("emailObj", emailObj);

        fetch("https://ntz.team/email_subscription", {
            method: 'POST',
            body: emailObj,
        })
            .then(data => {
                // console.log('Success:', data);
                alert("Спасибо за подписку");
                var extractedData = data.json()
                return extractedData;
            }).then(extractedData => {
                // console.log("response:", extractedData);
            })
            .catch((error) => {
                console.error('Error:', error);
                alert("Ошибка отправки");
            });
    }

    render() {
        // console.log("this.state.email", this.state.email);
        return (
            <div className="ntz-footer">
                <div className="ntz-container">
                    <div className="ntz-footer__top">
                        <div className="ntz-footer__signup">
                            <Translate content="footer.signup" component="div" />
                            <Translate content="footer.get500" component="div" />
                        </div>
                        <form className="ntz-footer__signup-form" onSubmit={this.handleSubcription}>
                            <input type="text" className="ntz-input" value={this.state.email} onChange={this.handleInputChange} />
                            <button className="ntz-btn"><Translate content="footer.signup_btn" /></button>
                        </form>
                        <div className="ntz-footer__soc">
                            <Translate content="footer.soc" component="div" className="ntz-footer__soc-text" />
                            <div className="ntz-footer__soc-links">
                                <a href="https://www.facebook.com/netizensofficial1" target="_blank" className="ntz-footer__soc-img"></a>
                                <a href="https://www.youtube.com/user/Lamarazhabina" target="_blank" className="ntz-footer__soc-img ntz-footer__soc-img--youtube"></a>
                                <a href="https://www.instagram.com/netizensofficial/?hl=en" target="_blank" className="ntz-footer__soc-img ntz-footer__soc-img--inst"></a>
                                <a href="https://tlgg.ru/@Netizensstart_bot" target="_blank" className="ntz-footer__soc-img ntz-footer__soc-img--tel"></a>
                            </div>
                        </div>
                    </div>
                    <div className="ntz-footer__links">
                        <div className="ntz-footer__column">
                            <Link to={`/`} className="ntz-footer__logo">
                                <div className="ntz-footer__logo-img"></div>
                                <div className="ntz-footer__logo-text">NTZ</div>
                            </Link>
                            <Translate component="div" content="footer.ntz_description" className="ntz-footer__slogan" />
                            <div className="ntz-footer__phone-block">
                                <Link to="mailto:info@ntz.team" className="ntz-footer__phone-text" target="_blank">
                                    <Translate content="footer.contact" component="span" />
                                </Link>
                            </div>
                        </div>
                        <div className="ntz-footer__column">
                            <Translate component="div" content="landing.about" className="ntz-footer__sub-title" />
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.us" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.details" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.contacts" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.suppliers" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.addresses" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.pickup" /></Link>
                        </div>
                        <div className="ntz-footer__column">
                            <Translate component="div" content="footer.partners" className="ntz-footer__sub-title" />
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.advantages" /></Link>
                        </div>
                        <div className="ntz-footer__column">
                            <Translate component="div" content="footer.clients" className="ntz-footer__sub-title" />
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.order" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.payment" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.delivery" /></Link>
                            <Link to={`#`} className="ntz-footer__link"><Translate content="footer.rules" /></Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;