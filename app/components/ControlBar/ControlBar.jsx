import React from 'react';
import Search from "../Search/Search";
import CartBlock from "../Cart/CartBlock"
import { Link } from "react-router/es";
import AccountStore from "stores/AccountStore";
import AccountActions from "actions/AccountActions";
import Translate from "react-translate-component";
import WalletUnlockActions from "actions/WalletUnlockActions";
import CategoriesLeft from "../Categories/CategoriesLeft";
import ItemInCartModal from "../Modal/ItemInCartModal";

class ControlBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount
        }
    }

    loginForm() {
        WalletUnlockActions.unlock().then(() => {
            AccountActions.tryToSetCurrentAccount();

            let currentAccount = AccountStore.getState().currentAccount;

            if (window.innerWidth < 600) {
                // this.context.router.push(`mobile/account/${currentAccount}/dashboard`); 
                this.props.routes.push(`mobile/account/${currentAccount}/dashboard`);
            }
            else {
                // this.context.router.push(`account/${currentAccount}/dashboard`); // does not redirect
                this.props.routes.push(`account/${currentAccount}/dashboard`);
            }
        });
    }

    render() {
        // console.log("control bar props", this.props);

        return (
            <div className="ctrl-bar ntz-container">
                <div className="ctrl-bar__top">
                    <Link to={`/`} className="ctrl-bar__logo">
                        <div className="ctrl-bar__logo-pic"></div>
                        <div>NTZ</div>
                    </Link>
                    <div className="ctrl-bar__login">
                        <div onClick={this.loginForm.bind(this)} className="ctrl-bar__login-inner">
                            <div className="ctrl-bar__login-pic"></div>
                            <Translate component="span" content="landing.login" className="ctrl-bar__login-text" />
                        </div>
                        <span>/</span>
                        <Link to={`/registration`} className="ctrl-bar__reg">
                            <div className="ctrl-bar__reg-pic"></div>
                            <Translate component="span" content="landing.registration" className="ctrl-bar__reg-text" />
                        </Link>
                    </div>
                </div>
                <div className="ctrl-bar__bottom">
                    <CategoriesLeft />
                    <Search />
                    <div className="ctrl-bar__cart-block">
                        <CartBlock />
                        <ItemInCartModal />
                    </div>
                </div>
            </div>
        )
    }

}

export default ControlBar;