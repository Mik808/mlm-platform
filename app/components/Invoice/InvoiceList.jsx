import React from 'react';
import Translate from "react-translate-component";
import { Link } from "react-router/es";
import { Apis } from "bitsharesjs-ws";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import LinkToAccountById from "../Utility/LinkToAccountById";
import { invoice_create } from 'bitsharesjs/es/serializer/src/operations';
import { map } from 'bluebird-lst';
import LoadingIndicator from "../LoadingIndicator";
import InvoiceDetail2 from "./InvoiceDetail2";


class InvoiceList extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };
    constructor(props) {
        super(props);

        this.state = {
            userName: this.props.account.get('name'),
            userId: this.props.account.get('id'),
            renderInvoiceDetail: false,
            invoiceUnpaid: [],
            invoicePaid: [],
            userBalance: '',
            renderUnpaid: false,
            newRenderPaid: false,
        };
    }

    componentDidMount() {
        let userID = this.state.userId;
        let currentDate = Date.now();

        // PREW INVOICES ARRAY
        Apis.instance().db_api().exec("get_invoices", [userID, 0]).then(invoiceUnpaid => {
            for (var i = 0; i < invoiceUnpaid.length; i++) {
                if (Date.parse(invoiceUnpaid[i]['expiration']) > currentDate) {
                    var invoiceUnpaidActual = { id: invoiceUnpaid[i]['id'], merchant_order_id: invoiceUnpaid[i]['merchant_order_id'], merchant: invoiceUnpaid[i]['merchant'] }
                    this.state.invoiceUnpaid.push(invoiceUnpaidActual);
                    this.setState({
                        renderUnpaid: true
                    })
                }
            }
        }).catch(err => {
            console.log("error:", err);
        });

        // PREW PAID INVOICES ARRAY
        Apis.instance().db_api().exec("get_invoices", [userID, 1]).then(invoicePaid => {
            for (var i = 0; i < invoicePaid.length; i++) {
                var invoicePaidActual = { id: invoicePaid[i]['id'], merchant_order_id: invoicePaid[i]['merchant_order_id'], merchant: invoicePaid[i]['merchant'] }
                this.state.invoicePaid.push(invoicePaidActual);

                this.setState({
                    newRenderPaid: true
                })
            }
        }).catch(err => {
            console.log("error:", err);
        });

        // NEW INVOICES ARRAY
        Apis.instance().db_api().exec("new_get_invoices", [userID, 0]).then(invoiceUnpaid => {
            for (var i = 0; i < invoiceUnpaid.length; i++) {
                if (Date.parse(invoiceUnpaid[i]['expiration']) > currentDate) {
                    var invoiceUnpaidActual = { id: invoiceUnpaid[i]['id'], merchant_order_id: invoiceUnpaid[i]['merchant_order_id'], merchant: invoiceUnpaid[i]['merchant'] }
                    this.state.invoiceUnpaid.push(invoiceUnpaidActual);
                    this.setState({
                        renderUnpaid: true
                    })
                }
            }
        }).catch(err => {
            console.log("error:", err);
        });

        this.fetchPaidInvoices(userID);
    }

    fetchPaidInvoices(userID) {
        // NEW PAID INVOICES ARRAY
        Apis.instance().db_api().exec("new_get_invoices", [userID, 1]).then(newInvoicePaid => {
            for (var i = 0; i < newInvoicePaid.length; i++) {
                var invoicePaidActual = { id: newInvoicePaid[i]['id'], merchant_order_id: newInvoicePaid[i]['merchant_order_id'], merchant: newInvoicePaid[i]['merchant'] }
                this.state.invoicePaid.push(invoicePaidActual);

                this.setState({
                    newRenderPaid: true,
                })

                // console.log("invoicePaidActual", invoicePaidActual);
            }
        })
            .catch(err => {
                console.log("error:", err);
            });

    }

    _toggleLock(e) {
        e.preventDefault();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        }
        return true
    }

    render() {
        // console.log("this.state.invoicePaid.lenght", this.state.invoicePaid.length);
        // console.log("this.state.invoicePaid", this.state.invoicePaid);
        // console.log("this.state.newRenderPaid", this.state.newRenderPaid);
        // console.log("this.state.userId, invoiceList", this.state.userId);


        let account_name = this.state.userName;

        let invoiceUnpaid = this.state.invoiceUnpaid;
        let renderUnpaid = this.state.renderUnpaid;

        let invoicePaid = this.state.invoicePaid;
        let newRenderPaid = this.state.newRenderPaid;

        let isLocked = WalletDb.isLocked();

        return (
            <div>
                <div className="invoice__wrap">
                    <Translate content="invoice.title" className="invoice__title" component="h1" />

                    {/* UNPAIED ORDERS */}
                    <Translate content="invoice.unpaid_orders" className="invoice__subtitle" component="h3" />
                    {renderUnpaid ?
                        <ul className="invoice__list">
                            {invoiceUnpaid.map(invoice =>
                                <li className="invoice__item invoice__item--unpaid" key={invoice['id']} onClick={this._toggleLock.bind(this)}>
                                    {isLocked ?
                                        <a href="">
                                            <div className="invoice__short-text"><Translate content="invoice.order_num" component="span" />
                                                <span>{invoice['merchant_order_id']}</span>
                                                <Translate content="invoice.merchant" component="span" />
                                                <LinkToAccountById noLink={true} account={invoice['merchant']} />
                                            </div>

                                            <button className="invoice__pay-btn">
                                                <Translate content="invoice.pay" component="span" />
                                            </button>
                                        </a>
                                        :
                                        <Link to={`account/${account_name}/invoice/${invoice['id']}`}>
                                            <div className="invoice__short-text"><Translate content="invoice.order_num" component="span" />
                                                <span>{invoice['merchant_order_id']}</span>
                                                <Translate content="invoice.merchant" component="span" />
                                                <LinkToAccountById noLink={true} account={invoice['merchant']} />
                                            </div>

                                            <button className="invoice__pay-btn">
                                                <Translate content="invoice.pay" component="span" />
                                            </button>
                                        </Link>
                                    }
                                </li>)}
                        </ul>
                        : <Translate content="invoice.no_invoices" className="invoice__description" component="p" />
                    }

                    <Translate content="invoice.paid_orders" className="invoice__subtitle" component="h3" />
                    {newRenderPaid && this.state.invoicePaid ? (
                        this.state.invoicePaid.map((invoice) => {
                            return <InvoiceDetail2 key={invoice.id} invoiceId={invoice.id} userId={this.state.userId} className="blockChainInvoice" />
                        })
                    )
                        : <Translate content="invoice.no_invoices" className="invoice__description" component="p" />
                    }
                </div>
            </div>
        );
    }
}

export default InvoiceList;