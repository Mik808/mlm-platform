import React from 'react';
import Translate from "react-translate-component";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import PrivateKeyStore from "stores/PrivateKeyStore";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import { Apis } from "bitsharesjs-ws";
import { Link } from "react-router/es";
import LinkToAccountById from "../Utility/LinkToAccountById";
import ls from "common/localStorage";

const STORAGE_KEY = "__graphene__";
let ss = new ls(STORAGE_KEY);
var logo = require("assets/app_img/reg.png");

class InvoiceDetail extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };
    constructor(props) {
        super(props);
        let newInvoice = false;
        if (this.props.params.id.startsWith('1.17')) {
            newInvoice = true
        }
        this.state = {
            userName: this.props.account.get('name'),
            userId: this.props.account.get('id'),
            invoiceId: this.props.params.id,
            invoiceData: {},
            invoiceItem: [],
            coreBalance: 0,
            bonusBalance: 0,
            coreAmount: 0,
            bonusAmount: 0,
            newInvoice: newInvoice,
            locale: ss.get("settings_v3").locale,
        };


        this.onCoreValueChange = this.onCoreValueChange.bind(this);
        this.onBonusValueChange = this.onBonusValueChange.bind(this);
    }

    componentWillMount() {
        let invoiceId = this.state.invoiceId;

        Apis.instance().db_api().exec("get_objects", [[invoiceId]])
            .then((results) => {
                let memo = PrivateKeyStore.decodeMemo(results[0]['memo']);
                // console.log("memo", memo);
                let memoJson = "";
                try {
                    memoJson = JSON.parse(memo.text);
                    this.setState({
                        memoType: "JSON"
                    })
                } catch (err) {
                    memoJson = memo.text;
                    this.setState({
                        memoType: "string"
                    })
                }
                // console.log("results", results);
                // console.log('memoJson', memoJson);
                this.setState({
                    invoiceData: results[0],
                    invoiceItem: memoJson,
                    coreAmount: results[0]['amount']["amount"],
                })

                let userID = this.state.userId;
                let totalAmount = this.state.invoiceData.amount['amount'] ;

                Apis.instance().db_api().exec("get_account_balances", [
                    userID,
                    ['1.3.0', '1.3.1']]).then(userBalance => {
                        this.setState({
                            coreBalance: userBalance[0]["amount"],
                            bonusBalance: userBalance[1]["amount"]
                        });
                        let ntz_amount = totalAmount;
                        if (this.state.invoiceData.ntz_amount) {
                            ntz_amount=this.state.invoiceData.ntz_amount['amount'];
                        }
                        if ((this.state.bonusBalance) > 0) {
                            if ((this.state.bonusBalance) >= ntz_amount) {
                                this.setState({
                                    coreAmount: Math.round((totalAmount/100000-Math.floor(ntz_amount/100000))*100)/100,
                                    bonusAmount: Math.floor(ntz_amount/100000)
                                });
                            }
                            else {
                                // console.log(Math.round(totalAmount / 100000- Math.floor(this.state.bonusBalance / 100000)*100)/100);
                                this.setState({
                                    coreAmount: Math.round((totalAmount / 100000- Math.floor(this.state.bonusBalance / 100000))*100)/100,
                                    bonusAmount: Math.floor(this.state.bonusBalance / 100000)
                                });
                            }
                        }
                        else {
                            this.setState({
                                coreAmount: totalAmount/100000,
                                bonusAmount: 0
                            });
                        }
                    }).catch(err => {
                        console.log("error:", err);
                    });


            }).catch(e => {
                console.log("[get_objects] Error ----->", e);
            });
    }

    onCoreValueChange(e) {
        let totalAmount = this.state.invoiceData.amount['amount'];
        let ntz_amount = totalAmount;
        if (this.state.invoiceData.ntz_amount) {
            ntz_amount=this.state.invoiceData.ntz_amount['amount'];
        }
        let new_value = parseFloat(e.target.value)*100000;
        if (new_value < 0) { new_value = 0; }
        if (new_value > this.state.coreBalance*100000 && new_value < totalAmount) { new_value = this.state.coreBalance*100000; }
        if (new_value <= this.state.coreBalance*100000 && new_value > totalAmount) { new_value = totalAmount; }
        let new_bonus_value = totalAmount - new_value;
        if (new_bonus_value > this.state.bonusBalance*100000) { new_bonus_value = this.state.bonusBalance*100000 }
        if (new_bonus_value > ntz_amount) { new_bonus_value = ntz_amount }
        this.setState({
            coreAmount: new_value/100000,
            bonusAmount: new_bonus_value/100000
        });
    }

    onBonusValueChange(e) {
        let totalAmount = this.state.invoiceData.amount['amount'];
        let ntz_amount = totalAmount;
        console.log("TCL: InvoiceDetail -> onBonusValueChange -> ntz_amount", ntz_amount)
        if (this.state.invoiceData.ntz_amount) {
            ntz_amount=this.state.invoiceData.ntz_amount['amount'];
        }

        console.log("TCL: InvoiceDetail -> onBonusValueChange -> ntz_amount", ntz_amount)
        
        let new_value = parseFloat(e.target.value)*100000;
        if (new_value < 0) { new_value = 0; }
        if (new_value > this.state.bonusBalance*100000 && new_value < ntz_amount) { new_value = this.state.bonusBalance; }
        if (new_value <= this.state.bonusBalance*100000 && new_value > ntz_amount) { new_value = ntz_amount; }
        let new_core_value = totalAmount - new_value;
        if (new_core_value > this.state.coreBalance*100000) { new_core_value = this.state.coreBalance * 100000 }
        this.setState({
            bonusAmount: new_value/100000,
            coreAmount: new_core_value/100000
        });
    }

    onSubmit(e) {
        e.preventDefault();

        let invoice = this.state.invoiceId;
        let customer = this.state.invoiceData.customer;
        let merchant = this.state.invoiceData.merchant;

        let coreAmount = Math.floor(parseFloat(this.state.coreAmount) * 100000);
        let bonusAmount = Math.floor(parseFloat(this.state.bonusAmount) * 100000);
        let totalPrice = coreAmount + bonusAmount;

        if (this.state.newInvoice) {
            let payer = this.state.invoiceData.customer;

            AccountActions.newInvoicePay(
                invoice,
                customer,
                merchant,
                payer,
                coreAmount,
                bonusAmount
            ).then(() => {
                this.props.router.push(`/success`);
                if (totalPrice == this.state.invoiceData.amount['amount']) {
                    TransactionConfirmStore.unlisten(this.onTrxIncluded);
                    TransactionConfirmStore.listen(this.onTrxIncluded);
                }
            }).catch(err => {
                console.log("error:", err);
            });
        }
        else {
            AccountActions.invoicePay(
                invoice,
                customer,
                merchant,
                coreAmount,
                bonusAmount
            ).then(() => {
                this.props.router.push(`/success`);
                if (totalPrice == this.state.invoiceData.amount['amount']) {
                    TransactionConfirmStore.unlisten(this.onTrxIncluded);
                    TransactionConfirmStore.listen(this.onTrxIncluded);
                }
            }).catch(err => {
                console.log("error:", err);
            });
        }
    }


    render() {
        if (Object.getOwnPropertyNames(this.state.invoiceData).length > 0) {
            let account_name = this.state.userName;
            let items = this.state.invoiceItem;
            let totalAmount = this.state.invoiceData.amount['amount'] / 100000;
            let totalBalance = parseFloat(this.state.coreBalance + this.state.bonusBalance) / 100000;
            let tax = 0;
            let delivery =0;
            if(this.state.newInvoice) {
                totalAmount = totalAmount;
                tax = this.state.invoiceData.tax['amount'] / 100000;
                delivery = this.state.invoiceData.delivery['amount'] / 100000;
            }
            let locale = this.state.locale;

            // console.log("items", items);
            // console.log("this.state.invoiceItem", this.state.invoiceItem);
            // console.log("this.state.invoiceData", this.state.invoiceData);
            // console.log("this.state.invoiceData.merchant_order_id", this.state.invoiceData.merchant_order_id);
            // console.log("only this.state", this.state);
            // console.log("this.state.userId", this.state.userId)

            return (
                <div className="invoice-detail" id={this.state.invoiceData.invoice}>
                    <div className="invoice-detail__header">
                        <img className="invoice-detail__logo" width="40" height="40" src={logo} />

                        <Translate content="invoice.pay_invoice" className="invoice-detail__title" component="h1" />

                        <Translate content="invoice.invoice_num" className="invoice-detail__subtitle" component="span" />
                        <span className="invoice-detail__subtitle">{this.state.invoiceData.merchant_order_id}</span>
                    </div>

                    <div className="invoice-detail__data">
                        <div className="invoice-detail__inner">
                            <Translate content="invoice.customer" className="invoice-detail__descript" component="span" />
                            <LinkToAccountById account={this.state.invoiceData.customer} />
                        </div>
                        <div className="invoice-detail__inner">
                            <Translate content="invoice.merchant_descript" className="invoice-detail__descript" component="span" />
                            <LinkToAccountById account={this.state.invoiceData.merchant} />
                        </div>
                    </div>

                    <div className="invoice-detail__body">
                        <table className="invoice-detail__table">
                            <thead>
                                <tr className="invoice-detail__order-header">
                                    <th className="invoice-detail__text"><Translate content="invoice.item" component="span" /></th>
                                    <th className="invoice-detail__num invoice-detail__num--quantity"><Translate content="invoice.quantity" component="span" /></th>
                                    <th className="invoice-detail__num"><Translate content="invoice.price" component="span" /></th>
                                </tr>
                            </thead>
                            <tbody className="invoice-detail__content">
                                {this.state.memoType == "JSON" ?
                                    items.map((item, index) =>
                                        <tr key={index}>
                                            <td>{item.label}</td>  {/* name */}
                                            <td className="invoice-detail__num invoice-detail__num--quantity">{item.quantity}</td>
                                            <td className="invoice-detail__num">{item.price}</td>
                                        </tr>)
                                    :
                                    <tr>
                                        <td colSpan="3">
                                            {this.state.invoiceItem}
                                        </td>
                                    </tr>
                                }

                            </tbody>

                            <tfoot>
                                {this.state.newInvoice ?
                                    <tr>
                                        <td colSpan="3" className="invoice-detail__footer">
                                            <Translate content="invoice.tax" className="invoice-detail__text invoice-detail__text--total" component="span" />
                                            <span className="invoice-detail__text invoice-detail__text--total">{tax}</span>
                                        </td>
                                    </tr>
                                    : null
                                }
                                {this.state.newInvoice ?
                                    <tr>
                                        <td colSpan="3" className="invoice-detail__footer">
                                            <Translate content="invoice.delivery" className="invoice-detail__text invoice-detail__text--total" component="span" />
                                            <span className="invoice-detail__text invoice-detail__text--total">{delivery}</span>
                                        </td>
                                    </tr>
                                    : null
                                }
                                <tr>
                                    <td colSpan="3" className="invoice-detail__footer">
                                        <Translate content="invoice.total" className="invoice-detail__text invoice-detail__text--total" component="span" />
                                        <span className="invoice-detail__text invoice-detail__text--total">{totalAmount}</span>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        {totalAmount > totalBalance && locale == "ru" ?
                            <div>
                                <Translate content="invoice.add-funds" className="invoice__subtitle" component="h3" />
                                <Link className="invoice-pay__btn" to={`account/${account_name}/payment/?amount=${totalAmount - totalBalance}`}>
                                    <Translate content="account.balance" component="span" />
                                </Link>
                            </div>
                            :
                            <form
                                id="invoicePayForm"
                                onSubmit={this.onSubmit.bind(this)}
                                noValidate
                            >
                                <div className="invoice-pay__wrap">
                                    <div className="invoice-pay__inner">
                                        <span className="invoice-pay__text">NTZ:</span>
                                        <input className="invoice-pay__field"
                                            type="number"
                                            value={this.state.coreAmount}
                                            onChange={this.onCoreValueChange.bind(this)}
                                        />
                                    </div>
                                    {/* <div className="invoice-pay__inner">
                                        <span className="invoice-pay__text">NTL:</span>
                                        <input className="invoice-pay__field"
                                            type="number"
                                            value={this.state.bonusAmount}
                                            onChange={this.onBonusValueChange.bind(this)}
                                        />
                                    </div> */}
                                </div>

                                <button className="invoice-pay__btn">
                                    <Translate content="invoice.pay" component="span" />
                                </button>
                            </form>
                        }
                    </div>
                </div>
            );
        }
        else return null;
    }
}

export default InvoiceDetail;