import React from "react";
import Translate from "react-translate-component";
import AccountStore from "stores/AccountStore";
import { Link } from "react-router/es";
import ls from "common/localStorage";

const STORAGE_KEY = "__graphene__";
let ss = new ls(STORAGE_KEY);

var assembly = require("assets/app_img/events/assembly.png");

class Events extends React.Component {
    constructor(props) {
        super();
        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            locale: ss.get("settings_v3").locale,
            eventsArray: []
        };
        this.selectValue = this.selectValue.bind(this);
    }

    componentDidMount() {
        const xhr = new XMLHttpRequest();
        let url = 'https://api.ntz.team/get_events';
        let locale = this.state.locale;

        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        var data = 'lang=' + locale;
        xhr.send(data);

        xhr.onreadystatechange = () => {
            if (xhr.status == 200) {
                if (xhr.responseText != '') {
                    this.setState({
                        eventsArray: JSON.parse(xhr.responseText)
                    })
                    // console.log('eventsArray', this.state.eventsArray);
                }
            }
            else {
                alert("Error");
            }
        }
    }

    selectValue(id, e) {
        document.getElementById(id).select()
    }

    render() {
        let currentAccount = AccountStore.getState().currentAccount;
        let eventsArray = this.state.eventsArray;
        // console.log("TCL: Events -> render -> eventsArray", eventsArray)

        return (
            <div className="events__wrap">
                <div className="events__content">
                    <div className="events__inner">
                        <Translate className="events__title" component="h1" content="events.title" />

                        {/* <ul className="events__list">
                            <li className="events__item">
                                <a className="events__link-wrap" href="https://ntz.team/events/assembly">
                                    <div className="events__img-wrap">
                                        <img className="events__img" src={assembly} />
                                    </div>

                                    <div className="events__info-wrap">

                                        <Translate className="events__text events__name" component="p" content="events.name_assembly" />

                                        <Translate className="events__text events__description" component="p" content="events.description_assembly" />
                                        
                                        <Translate className="events__text events__text--date" component="p" content="assembly.date" />
                                    </div>
                                </a>
                            </li>
                        </ul> */}

                        {eventsArray.length > 0 ?
                            <ul className="events__list">
                                {eventsArray.map(event =>
                                    <li className="events__item" key={event['id']}>
                                        <span className="events__link-wrap">
                                            <div className="events__img-wrap">
                                                <img className="events__img" src={event['image']} alt={event['name']} />
                                                <a className="events__buy-btn" href={`https://${event['host']}/?ntz_login=${currentAccount}`}>
                                                    <Translate component="span" content="events.buy-btn" />
                                                </a>
                                            </div>

                                            <div className="events__info-wrap">
                                                <p className="events__text events__name">{event['name']}</p>

                                                <p className="events__text events__description">{event['description']}</p>

                                                <p className="events__text events__text--date">{event['date']}</p>

                                                <Translate className="events__description events__description--ref" component="p" content="assembly.ref-text" />
                                                <input
                                                    className="events__referral-link"
                                                    id="refLinkKoleso"
                                                    defaultValue={`https://${event['host']}/?r=${currentAccount}`}
                                                    onClick={this.selectValue.bind(this, 'refLinkKoleso')}
                                                    readOnly
                                                />
                                            </div>
                                        </span>
                                    </li>
                                )}
                            </ul>
                            : null}
                    </div>
                </div>
            </div>
        )
    }
}

export default Events;