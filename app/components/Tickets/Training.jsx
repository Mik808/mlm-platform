import React from 'react';
import Translate from "react-translate-component";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";


class Training extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            email: '',
            trainingTickets: 0
        };
        this.buyTraining = this.buyTraining.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
    }

    onValueChange() {
        this.setState({
            email: this.refs.email.value,
            trainingTickets: this.refs.trainingTickets.value
        });
    }

    _toggleLock() {
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        }
        return true
    }

    buyTraining() {
        this._toggleLock();

        var login = this.state.currentAccount;
        var email = this.state.email;
        var trainingTickets = this.state.trainingTickets;
  
        const xhr = new XMLHttpRequest();

        xhr.open('POST', 'https://api.ntz.team/assembly', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');


        var data = {
            "login": login,
            "email": email,
            "trainingTickets": trainingTickets
        };

        if (login && email.length > 0 && trainingTickets > 0) {
            xhr.send(JSON.stringify(data));

            xhr.onreadystatechange = () => {
                if (xhr.status == 200) {
                    this.props.router.push(`/account/${login}/my-orders`);
                }
            }
        }
        else {
            alert("Проверьте поле email и выберите хотя бы один билет");
        }
    }


    render() {
        return (
            <div className="training__wrap">
                <div className="training__content">
                    <div className="training__inner">
                        <div className="training__header">
                            <Translate content="training.title" className="training__title" component="h1" />
                            <div className="training__logo"></div>
                        </div>



                        <div className="training__text-wrap">
                            <div className="training__text-block">
                                <Translate unsafe content="training.description" className="training__description" component="span" />
                            </div>

                            <div className="training__data-block">
                                <Translate content="training.date" className="training__data training__data--info" component="span" />
                                <Translate content="training.date-info" className="training__data" component="span" />
                            </div>

                            <div className="training__data-block">
                                <Translate content="training.place" className="training__data training__data--info" component="span" />
                                <Translate content="training.place-info" className="training__data" component="span" />
                            </div>
                            <div className="training__data-block">
                                <Translate content="training.price" className="training__data training__data--info" component="span" />
                                <span className="training__data">18500 NTZ</span>
                            </div>

                            <Translate content="training.info" className="training__info" component="p" />
                        </div>

                        <div className="training__buy-wrap">

                            <div className="training__buy-block">
                                <Translate className="training__data-title" component="span" content="training.email" />
                                <input className="tickets__data-input" ref="email" type="email" onChange={this.onValueChange.bind(this)} />
                            </div>

                            <div className="training__buy-block">
                                <Translate className="training__data-title" component="span" content="training.tickets" />
                                <input type="number" name="trainingTickets" className="training__number" ref="trainingTickets" value={this.state.trainingTickets} onChange={this.onValueChange.bind(this)} />
                            </div>


                            <button className="training__pay-btn" onClick={this.buyTraining.bind(this)}>
                                <Translate content="training.buy" component="span" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Training;