import React from 'react';
import Translate from "react-translate-component";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";



class Netizens extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            email: '',
            ticketsStandartNtz: 0,
            ticketsBusinessNtz: 0,
            ticketVIPNtz: 0,
            ticketOnline: 0
        };

        this.buyTicket = this.buyTicket.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
    }

    onValueChange() {
        this.setState({
            email: this.refs.email.value,
            ticketsStandartNtz: this.refs.ticketsStandartNtz.value,
            ticketsBusinessNtz: this.refs.ticketsBusinessNtz.value,
            ticketVIPNtz: this.refs.ticketVIPNtz.value,
            ticketOnline: this.refs.ticketOnline.value
        });
    }

    _toggleLock() {
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        }
        return true
    }

    buyTicket() {
        this._toggleLock();

        var login = this.state.currentAccount;
        var email = this.state.email;
        var ticketsStandartNtz = this.state.ticketsStandartNtz;
        var ticketsBusinessNtz = this.state.ticketsBusinessNtz;
        var ticketVIPNtz = this.state.ticketVIPNtz;
        var ticketOnline = this.state.ticketOnline;

        const xhr = new XMLHttpRequest();

        xhr.open('POST', 'https://api.ntz.team/assembly', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');


        var data = {
            "login": login,
            "email": email,
            "vipNtz": ticketVIPNtz,
            "businessNtz": ticketsBusinessNtz,
            "standartNtz": ticketsStandartNtz,
            "onlineNtz": ticketOnline
        };

        if (login && email.length > 0 && (ticketVIPNtz > 0 || ticketsBusinessNtz > 0 ||  ticketsStandartNtz > 0 || ticketOnline > 0)) {
            xhr.send(JSON.stringify(data));

            xhr.onreadystatechange = () => {
                if (xhr.status == 200) {
                    this.props.router.push(`/account/${login}/my-orders`);
                }
            }
        }
        else {
            alert("Проверьте поле email и выберите хотя бы один билет");
        }
    }


    render() {

        return (
            <div className="tickets__content tickets__content--ntz">
                <div className="tickets__wrap">
                    <Translate content="events.name_ntz" className="tickets__title" component="h1" />

                    <ul className="tickets__data-list">
                        <li className="tickets__data-item">
                            <Translate className="tickets__data-title" component="span" content="assembly.place-title" />
                            <Translate className="tickets__data-text" component="span" content="events.place_ntz" />
                        </li>
                        <li className="tickets__data-item">
                            <Translate className="tickets__data-title" component="span" content="assembly.date-title" />
                            <Translate className="tickets__data-text" component="span" content="events.date_ntz" />
                        </li>
                        <li className="tickets__data-item">
                            <Translate className="tickets__data-title" component="span" content="assembly.date-time" />
                            <Translate className="tickets__data-text" component="span" content="assembly.time" />
                        </li>
                    </ul>

                    <div className="tickets__email-block">
                        <Translate className="tickets__data-title tickets__email" component="span" content="assembly.email" />
                        <input className="tickets__data-input" ref="email" type="email" onChange={this.onValueChange.bind(this)} />
                    </div>

                    <ul className="tickets__list">
                        <li className="tickets__item tickets__item--ntz">
                            <Translate className="tickets__name tickets__name--ntz" component="h2" content="assembly.ticket1" />
                            <div>
                                <p className="tickets__price-now tickets__price-now--ntz">2 000 NTZ</p>
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsStandartNtz" className="tickets__number tickets__number--ntz" ref="ticketsStandartNtz" value={this.state.ticketsStandartNtz} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn tickets__buy-btn--ntz" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>

                        <li className="tickets__item tickets__item--ntz">
                            <Translate className="tickets__name tickets__name--ntz" component="h2" content="assembly.ticket2" />
                            <div>
                                <p className="tickets__price-now tickets__price-now--ntz">4 000 NTZ</p>
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsBusinessNtz" className="tickets__number tickets__number--ntz" ref="ticketsBusinessNtz" value={this.state.ticketsBusinessNtz} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn tickets__buy-btn--ntz" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>

                        <li className="tickets__item tickets__item--ntz">
                            <Translate className="tickets__name tickets__name--ntz" component="h2" content="assembly.ticket4" />
                            <div>
                                <p className="tickets__price-now tickets__price-now--ntz">6 000 NTZ</p>
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketVIPNtz" className="tickets__number tickets__number--ntz" ref="ticketVIPNtz" value={this.state.ticketVIPNtz} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn tickets__buy-btn--ntz" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>

                        <li className="tickets__item tickets__item--ntz">
                            <Translate className="tickets__name tickets__name--ntz" component="h2" content="assembly.online" />
                            <div>
                                <p className="tickets__price-now tickets__price-now--ntz">5 000 NTZ</p>
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketOnline" className="tickets__number tickets__number--ntz" ref="ticketOnline" value={this.state.ticketOnline} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn tickets__buy-btn--ntz" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        );
    }
}

export default Netizens;