import React from 'react';
import Translate from "react-translate-component";
import WalletUnlockActions from "actions/WalletUnlockActions";
import WalletDb from "stores/WalletDb";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import { Link } from "react-router/es";

// this page is not obtained from react, it is replaced by the page which is on the server...
// route for this page is disabled

class Assembly extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            email: '',
            ticketsStandart: 0,
            ticketsBusiness: 0,
            ticketsPremium: 0,
            ticketsVIP: 0,
            ticketsOnline: 0
        };

        this.buyTicket = this.buyTicket.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.selectValue = this.selectValue.bind(this);
    }

    onValueChange() {
        this.setState({
            email: this.refs.email.value,
            ticketsStandart: this.refs.ticketsStandart.value,
            ticketsBusiness: this.refs.ticketsBusiness.value,
            ticketsPremium: 0,
            ticketsOnline: this.refs.ticketsOnline.value,
            ticketsVIP: 0
        });
    }

    _toggleLock() {
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        }
        return true
    }

    buyTicket() {
        this._toggleLock();

        var login = this.state.currentAccount;
        var email = this.state.email;
        var ticketsStandart = this.state.ticketsStandart;
        var ticketsBusiness = this.state.ticketsBusiness;
        var ticketsPremium = 0;
        var ticketsOnline = this.state.ticketsOnline;
        var ticketsVIP = 0;

        const xhr = new XMLHttpRequest();

        xhr.open('POST', 'https://api.ntz.team/assembly', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');


        var data = {
            "login": login,
            "email": email,
            "vip": ticketsVIP,
            "premium": ticketsPremium,
            "business": ticketsBusiness,
            "standart": ticketsStandart,
            "online": ticketsOnline
        };

        if (login && email.length > 0 && (ticketsVIP > 0 || ticketsStandart > 0 || ticketsPremium > 0 || ticketsBusiness > 0 || ticketsOnline > 0)) {
            xhr.send(JSON.stringify(data));

            xhr.onreadystatechange = () => {
                if (xhr.status == 200) {
                    this.props.router.push(`/account/${login}/my-orders`);
                }
            }
        }
        else {
            alert("Проверьте поле email и выберите хотя бы один билет");
        }
    }

    selectValue(id, e) {
        document.getElementById(id).select()
    }


    render() {

        return (
            <div className="tickets__content">
                <div className="tickets__wrap">
                    <Link to={`/events`}>
                        <Translate className="events__back-btn" component="span" content="events.btn-back" />
                    </Link>

                    <Translate content="assembly.title" className="tickets__title" component="h1" />

                    <ul className="tickets__data-list">
                        {/* <li className="tickets__data-item">
                            <Translate className="tickets__data-title" component="span" content="assembly.place-title" />
                            <Translate className="tickets__data-text" component="span" content="assembly.place" />
                        </li> */}
                        {/* <li className="tickets__data-item">
                            <Translate className="tickets__data-title" component="span" content="assembly.date-title" />
                            <Translate className="tickets__data-text" component="span" content="assembly.date" />
                        </li> */}
                        {/* <li className="tickets__data-item">
                            <Translate className="tickets__data-title" component="span" content="assembly.date-time" />
                            <Translate className="tickets__data-text" component="span" content="assembly.time-8" />
                        </li> */}
                    </ul>

                    <div className="tickets__ref-block">
                        <Translate className="tickets__ref-title" component="h1" content="assembly.ref-text" />
                        <input
                            className="tickets__referral-link"
                            id="refLink"
                            defaultValue={`https://iaone.pro/?r=${this.state.currentAccount
                                }`}
                            onClick={this.selectValue.bind(this, 'refLink')}
                            readOnly
                        />
                        <Translate className="tickets__ref-text" component="span" content="assembly.ref-text-2" />
                    </div>

                    <div className="tickets__email-block">
                        <Translate className="tickets__data-title tickets__email" component="span" content="assembly.email" />
                        <input className="tickets__data-input" ref="email" type="email" onChange={this.onValueChange.bind(this)} />
                    </div>

                    <ul className="tickets__list">
                        <li className="tickets__item">
                            <Translate className="tickets__name" component="h2" content="assembly.ticket1" />
                            <div>
                                <Translate className="tickets__today" component="span" content="assembly.today" />
                                <p className="tickets__price-now">4 500 NTZ</p>
                                {/* <Translate className="tickets__rise-date" component="span" content="assembly.rise-date" />
                                <p className="tickets__rise-new">3 000 NTZ</p> */}
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsStandart" className="tickets__number" ref="ticketsStandart" value={this.state.ticketsStandart} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>

                        <li className="tickets__item">
                            <Translate className="tickets__name" component="h2" content="assembly.ticket2" />
                            <div>
                                <Translate className="tickets__today" component="span" content="assembly.today" />
                                <p className="tickets__price-now">7 000 NTZ</p>
                                {/* <Translate className="tickets__rise-date" component="span" content="assembly.rise-date" />
                                <p className="tickets__rise-new">6 900 NTZ</p> */}

                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsBusiness" className="tickets__number" ref="ticketsBusiness" value={this.state.ticketsBusiness} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>

                        {/* <li className="tickets__item">
                            <Translate className="tickets__name" component="h2" content="assembly.ticket3" />
                            <div>
                                <p className="tickets__price-now">10 600 NTZ</p>
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsPremium" className="tickets__number" ref="ticketsPremium" value={this.state.ticketsPremium} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li> */}

                        <li className="tickets__item">
                            <Translate className="tickets__name" component="h2" content="assembly.ticket4" />
                            <div>
                                <Translate className="tickets__today" component="span" content="assembly.today" />
                                <p className="tickets__price-now">72 000 NTZ</p>
                                {/* <Translate className="tickets__rise-date" component="span" content="assembly.rise-date" />
                                <p className="tickets__rise-new">21 900 NTZ</p> */}
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsVIP" className="tickets__number" ref="ticketsVIP" value={this.state.ticketsVIP} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn" onClick={this.buyTicket.bind(this)} disabled="true">
                                    <Translate component="span" content="assembly.sold_out" />
                                </button>
                            </div>
                        </li>

                         <li className="tickets__item">
                            <Translate className="tickets__name" component="h2" content="assembly.online" />
                            <div>
                                <p className="tickets__price-now">4 500 NTZ</p>
                                <Translate component="p" className="tickets__num-text" content="assembly.num" />
                                <input type="number" name="ticketsOnline" className="tickets__number" ref="ticketsOnline" value={this.state.ticketsOnline} onChange={this.onValueChange.bind(this)} />

                                <button className="tickets__buy-btn" onClick={this.buyTicket.bind(this)}>
                                    <Translate component="span" content="assembly.buy-btn" />
                                </button>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        );
    }
}

export default Assembly;