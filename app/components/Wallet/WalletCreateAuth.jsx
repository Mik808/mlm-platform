import React, {Component} from "react";
import {Link} from "react-router/es";
import Translate from "react-translate-component";
import BrainkeyInput from "components/Wallet/BrainkeyInput";
import WalletDb from "stores/WalletDb";
import WalletManagerStore from "stores/WalletManagerStore";
import WalletActions from "actions/WalletActions";
import { connect } from "alt-react";
import cname from "classnames";
import SettingsActions from "actions/SettingsActions";

import {key} from "bitsharesjs/es";

class CreateNewWallet extends Component {

    static propTypes = {
        hideTitle: React.PropTypes.bool
    };

    constructor(props) {
        super();

        this.state = {
            wallet_public_name: "NTZ-default",
            valid_password: null,
            isValid: false,
            create_submitted: false,
            custom_brainkey: props.restoreBrainkey || false,
            brnkey: null,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(0, 45),
        };

        this.validate = this.validate.bind(this);
    }

    onPassword(valid_password) {
        this.state.valid_password = valid_password;
        this.setState({ valid_password }, this.validate);
    }

    onSubmit(e) {
        e.preventDefault();

        let {wallet_public_name, valid_password} = this.state;
        let generatedPass = this.refs.generatedPass.value;
        console.log(generatedPass);
        
//
//        WalletActions.setWallet(wallet_public_name, valid_password);
//        SettingsActions.changeSetting({
//            setting: "passwordLogin",
//            value: false
//        });
//        this.setState({create_submitted: true});
    }

    formChange(event) {
        let key_id = event.target.id;
        let value = event.target.value;
        if(key_id === "wallet_public_name") {
            //case in-sensitive
            value = value.toLowerCase();
            // Allow only valid file name characters
            if( /[^a-z0-9_-]/.test(value) ) return;
        }

        // Set state is updated directly because validate is going to
        // require a merge of new and old state
        this.state[key_id] = value;
        this.setState(this.state);
        this.validate();
    }

    validate(state = this.state) {
        let errors = state.errors;
        let {wallet_names} = this.props;
        errors.wallet_public_name =
            !wallet_names.has(state.wallet_public_name) ?
            null : `Wallet ${state.wallet_public_name.toUpperCase()} exists, please change the name`;

        var isValid = errors.wallet_public_name === null && state.valid_password !== null;
        if(state.custom_brainkey && isValid)
            isValid = state.brnkey !== null;
        this.setState({ isValid, errors });
    }


    render() {
        let state = this.state;
        let errors = state.errors;
        let has_wallet = !!this.props.current_wallet;

        if(this.state.create_submitted &&
            this.state.wallet_public_name === this.props.current_wallet) {
            return <div>
                <h4><Translate content="wallet.wallet_created" /></h4>
                <Link to="/dashboard">
                    <div className="button success"><Translate content="wallet.done" /></div>
                </Link>
            </div>;
        }

        return (
            <div>

            <form
                style={{maxWidth: "40rem"}}
                onSubmit={this.onSubmit.bind(this)}
                onChange={this.formChange.bind(this)} noValidate
            >

                <div
                    className="grid-content"
                    style={{
                        textAlign: "left"
                    }}
                >
                    {!this.props.restoreBrainkey ? <Translate component="p" content="wallet.create_importkeys_text" /> : null}
                    {!this.props.restoreBrainkey ? <Translate component="p" content="wallet.create_text" /> : null}
                </div>
                <div>
                    <label className="left-label"><Translate content="wallet.generated" /></label>
                    <div style={{paddingBottom: "0.5rem"}}>
                        <span className="inline-label">
                            <input style={{fontSize: "80%"}} disabled value={this.state.generatedPassword} type="text" ref="generatedPass"/>
                        </span>
                    </div>
                </div>
               
                <div className="no-overflow">
                    <br/>
                    <div>
                    <label><Translate content="wallet.name" /></label>
                    <input
                        tabIndex={3}
                        type="text"
                        id="wallet_public_name"
                        defaultValue={this.state.wallet_public_name}
                    />
                    </div>
                    <div className="has-error">{errors.wallet_public_name}</div>
                    <br/>
                </div>

                <div className="no-overflow">

                    { this.state.custom_brainkey ? (
                    <div>
                        <label><Translate content="wallet.brainkey" /></label>
                        <BrainkeyInput tabIndex={4} onChange={this.onBrainkey.bind(this)} errorCallback={(warn) => {
                            let {errors} = this.state;
                            errors.validBrainkey = warn;
                            this.setState({
                                errors
                            });
                        }}/>
                    </div>) : null}

                    <button className={cname("button")}>
                        <Translate content="wallet.create_wallet" />
                    </button>
                </div>
            </form>
        </div>);
    }
}

CreateNewWallet = connect(CreateNewWallet, {
    listenTo() {
        return [WalletManagerStore];
    },
    getProps() {
        return WalletManagerStore.getState();
    }
});

class WalletCreateAuth extends Component {
    render() {
        if(WalletDb.getWallet() && this.props.children) return <div>{this.props.children}</div>;

        return <CreateNewWallet {...this.props}/>;
    }
}


export { WalletCreateAuth };
