import React from "react";
import Trigger from "react-foundation-apps/src/trigger";
import BaseModal from "../Modal/BaseModal";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import PasswordInput from "../Forms/PasswordInput";
import notify from "actions/NotificationActions";
import Translate from "react-translate-component";
import AltContainer from "alt-container";
import WalletDb from "stores/WalletDb";
import WalletUnlockStore from "stores/WalletUnlockStore";
import AccountStore from "stores/AccountStore";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import SettingsActions from "actions/SettingsActions";
import { Apis } from "bitsharesjs-ws";
import utils from "common/utils";
import AccountSelector from "../Account/AccountSelector";


var reg_img = require("assets/app_img/reg.png");
var hidePass = require("assets/app_img/show-pass.png");
var showPass = require("assets/app_img/hide-pass.png");
let tabIndex = 1;

class WalletUnlockModal extends React.Component {

    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object
    }

    constructor(props, context) {
        super();
        this.state = this._getInitialState(props);
        this.state.passType = 'password';

        this.onPasswordEnter = this.onPasswordEnter.bind(this);
        this.state.active = context.location.pathname;
        this.showHidePass = this.showHidePass.bind(this);
        this.showRestoreModal = this.showRestoreModal.bind(this);
        this.hideRestoreModal = this.hideRestoreModal.bind(this);
    }

    _getInitialState(props = this.props) {
        return {
            password_error: null,
            password_input_reset: Date.now(),
            account_name: props.passwordAccount,
            account: null
        };
    }

    reset() {
        this.setState(this._getInitialState());
    }

    componentWillReceiveProps(np) {
        if (np.passwordAccount && !this.state.account_name) {
            this.setState({ account_name: np.passwordAccount });
        }
    }

    shouldComponentUpdate(np, ns) {
        return (
            !utils.are_equal_shallow(np, this.props) ||
            !utils.are_equal_shallow(ns, this.state)
        );
    }

    componentDidMount() {
        ZfApi.subscribe(this.props.modalId, (name, msg) => {
            if (name !== this.props.modalId)
                return;
            if (msg === "close") {
                WalletUnlockActions.cancel();
            } else if (msg === "open") {
                if (!this.props.passwordLogin) {
                    if (this.refs.password_input) {
                        this.refs.password_input.clear();
                        this.refs.password_input.focus();
                    }
                    if (WalletDb.getWallet() && Apis.instance().chain_id !== WalletDb.getWallet().chain_id) {
                        notify.error("This wallet was intended for a different block-chain; expecting " +
                            WalletDb.getWallet().chain_id.substring(0, 4).toUpperCase() + ", but got " +
                            Apis.instance().chain_id.substring(0, 4).toUpperCase());
                        ZfApi.publish(this.props.modalId, "close");
                        return;
                    }
                }
            }
        });

        if (this.props.passwordLogin) {
            if (this.state.account_name) {
                this.refs.password_input.focus();
            } else if (this.refs.account_input && this.refs.account_input.refs.bound_component) {
                this.refs.account_input.refs.bound_component.refs.user_input.focus();
            }
        }
    }

    componentDidUpdate() {
        if (this.props.resolve) {
            if (WalletDb.isLocked())
                ZfApi.publish(this.props.modalId, "open")
            else
                this.props.resolve()
        }
    }

    showHidePass(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            passType: this.state.passType === 'input' ? 'password' : 'input'
        })
    }

    onPasswordEnter(e) {
        const { passwordLogin } = this.props;
        e.preventDefault();
        const password = passwordLogin ? this.refs.password_input.value : this.refs.password_input.value();
        const account = passwordLogin ? this.state.account && this.state.account.get("name") : null;
        this.setState({ password_error: null });
        WalletDb.validatePassword(
            password || "",
            true, //unlock
            account
        );
        if (WalletDb.isLocked()) {
            this.setState({ password_error: true });
            return false;
        } else {
            if (!passwordLogin) {
                this.refs.password_input.clear();
            } else {
                this.refs.password_input.value = "";
                AccountActions.setPasswordAccount(account);
                AccountActions.setCurrentAccount(account);
            }
            ZfApi.publish(this.props.modalId, "close");
            this.props.resolve();
            WalletUnlockActions.change();
            this.setState({ password_input_reset: Date.now(), password_error: false });
        }
    }

    _toggleLoginType() {
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: !this.props.passwordLogin
        });
    }

    _onCreateWallet() {
        ZfApi.publish(this.props.modalId, "close");
        this.context.router.push("/create-account/password");
    }

    accountChanged(account_name) {
        if (!account_name) this.setState({ account: null });
        this.setState({ account_name, error: null });
    }

    onAccountChanged(account) {
        this.setState({ account, error: null });
    }

    showRestoreModal() {
        document.getElementById("unlockModal").classList.add("landing-modal--show");
    }

    hideRestoreModal() {
        document.getElementById("unlockModal").classList.remove("landing-modal--show");
    }

    renderWalletLogin() {
        return (
            <div>
                <div className="landing-form__header">
                    <img className="landing-form__img" width="60" height="60" src={reg_img} />
                    <Translate className="landing-form__login-text" component="span" content="landing.login-text" />
                </div>
                <form
                    className="landing-form"
                    id="landingFormWallModal"
                    noValidate
                >
                    <div className="landing-form__inner landing-form__inner--wall">
                        <label>
                            <Translate className="landing-form__tip" component="span" content="landing.pass-label" />
                            <PasswordInput
                                ref="password_input"
                                onEnter={this.onPasswordEnter}
                                key={this.state.password_input_reset}
                                wrongPassword={this.state.password_error}
                                noValidation
                            />
                        </label>
                    </div>

                    {/* Submit button */}
                    <button className="landing-form__btn" onClick={this.onPasswordEnter}>
                        <Translate component="span" content="landing.login-btn" />
                    </button>
                    <Trigger close={this.props.modalId}>
                        <div tabIndex={tabIndex++} className="landing-form__btn landing-form__btn--cancel"><Translate content="account.perm.cancel" /></div>
                    </Trigger>
                </form>
            </div>
        );
    }

    renderPasswordLogin() {
        let { account_name, from_error } = this.state;
        let tabIndex = 1;

        return (
            <div>
                <div className="landing-form__header">
                    <img className="landing-form__img" width="60" height="60" src={reg_img} />
                    <Translate className="landing-form__login-text" component="span" content="landing.login-text" />
                </div>
                <form
                    className="landing-form"
                    id="landingFormModal"
                    noValidate
                >
                    <div className="landing-form__inner">
                        <label htmlFor="landingFormModalLogin">
                            <Translate className="landing-form__tip" component="span" content="landing.login-label" />

                            <AccountSelector
                                ref="account_input"
                                accountName={account_name}
                                onChange={this.accountChanged.bind(this)}
                                onAccountChanged={this.onAccountChanged.bind(this)}
                                account={account_name}
                                error={from_error}
                                tabIndex={tabIndex++}
                                id="landingFormModalLogin"
                            />
                        </label>

                        <label className="landing-form__field-wrap" htmlFor="landingFormModalPass">
                            <Translate className="landing-form__tip" component="span" content="landing.pass-label" />
                            <input
                                ref="password_input"
                                type={this.state.passType}
                                id="landingFormModalPass" className="landing-form__field" />
                            <img
                                className="landing-form__pass-img"
                                src={this.state.passType == 'password' ? showPass : hidePass}
                                id="showPassBtn"
                                onClick={this.showHidePass}
                            />
                        </label>

                        <span className="landing-form__restoration" onClick={this.showRestoreModal.bind(this)}>
                            <Translate component="span" content="landing.restoration" />
                        </span>
                    </div>

                    {/* Submit button */}
                    <button className="landing-form__btn" onClick={this.onPasswordEnter}>
                        <Translate component="span" content="landing.login-btn" />
                    </button>
                    <Trigger close={this.props.modalId}>
                        <div tabIndex={tabIndex++} className="landing-form__btn landing-form__btn--cancel"><Translate content="account.perm.cancel" /></div>
                    </Trigger>
                </form>
                <div className="landing-modal" id="unlockModal">
                    <div className="landing-modal__inner">
                        <Translate component="span" className="landing-modal__text" content="landing.restore-text" />
                        <a className="landing-modal__link" href="mailto:support@ntz.team"> support@ntz.team</a>
                        <Translate component="span" className="landing-modal__text landing-modal__text--block" content="landing.same-email" />
                    </div>

                    <button className="landing-modal__btn" onClick={this.hideRestoreModal.bind(this)}>
                        <Translate component="span" content="landing.close" />
                    </button>
                </div>
            </div>
        );
    }



    render() {
        const { passwordLogin } = this.props;
        // DEBUG console.log('... U N L O C K',this.props)

        // Modal overlayClose must be false pending a fix that allows us to detect
        // this event and clear the password (via this.refs.password_input.clear())
        // https://github.com/akiran/react-foundation-apps/issues/34
        return (
            // U N L O C K
            <BaseModal id={this.props.modalId} ref="modal" overlay={true} overlayClose={false}>

                {passwordLogin ? this.renderPasswordLogin() : this.renderWalletLogin()}
            </BaseModal>
        );
    }

}

WalletUnlockModal.defaultProps = {
    modalId: "unlock_wallet_modal2"
};

class WalletUnlockModalContainer extends React.Component {
    render() {
        return (
            <AltContainer
                stores={[WalletUnlockStore, AccountStore]}
                inject={{
                    resolve: () => {
                        return WalletUnlockStore.getState().resolve;
                    },
                    reject: () => {
                        return WalletUnlockStore.getState().reject;
                    },
                    locked: () => {
                        return WalletUnlockStore.getState().locked;
                    },
                    passwordLogin: () => {
                        return WalletUnlockStore.getState().passwordLogin;
                    },
                    passwordAccount: () => {
                        return AccountStore.getState().passwordAccount || "";
                    }
                }}
            >
                <WalletUnlockModal {...this.props} />
            </AltContainer>
        );
    }
}
export default WalletUnlockModalContainer
