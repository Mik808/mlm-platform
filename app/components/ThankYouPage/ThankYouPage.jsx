import React from "react";
import AddToCartBtn from '../Buttons/AddToCartBtn';
import AboutMenu from "../IndexMain/AboutMenu";
import ControlBar from "../ControlBar/ControlBar";
import Translate from "react-translate-component";

class ThankYouPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <AboutMenu />
                <ControlBar />
                <div className="thank-page ntz-container">
                    <hr/>
                    <Translate component="div" content="thank_page.thanks" className="thank-page__title" />
                    <Translate component="div" content="thank_page.verify" className="thank-page__p" />
                    <button className="ntz-btn"><Translate component="span" content="thank_page.print" /></button>
                </div>
            </div>
        )
    }
}

export default ThankYouPage;