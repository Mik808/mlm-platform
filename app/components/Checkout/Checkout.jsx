import React from 'react';
import Translate from "react-translate-component";
import CartStore from "../../stores/CartStore";
import { connect } from "alt-react";
import counterpart from "counterpart";
import { AddressSuggestions } from 'react-dadata';
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import CartActions from "../../actions/CartActions";
import AboutMenu from "../IndexMain/AboutMenu"
import SearchResults from "../Search/SearchResults";
import Search from "../Search/Search";
import ControlBar from "../ControlBar/ControlBar";
import InputMask from 'react-input-mask';
import Footer from "../Footer/Footer";
import PhoneInput from "../Forms/PhoneInput";

class Checkout extends React.Component {

    constructor(props) {
        super(props);

        this.autoFillToken = "46df64baddc1048a2347f3d8814f9fe1b516e134";
        this.tax = 10;
        this.testDelivery = null;

        this.state = {
            userLogin: AccountStore.getState().currentAccount,
            referrer: this.props.referrer ? this.props.referrer : null,
            name: "",
            lastName: "",
            email: "",
            phone: "",
            address: "",
            country: "Россия",
            zipcode: "",
            city: "",
            street: "",
            building: "",
            apartment: "",
            comments: "",
            payWithNTZ: false,
            nameEror: "validation.emptyString",
            lastnameError: "validation.emptyString",
            emailError: "validation.emptyString",
            phoneError: "validation.emptyString",
            adddressError: "validation.emptyString",
            countryError: "validation.emptyString",
            zipcodeEror: "validation.emptyString",
            cityError: "validation.emptyString",
            city_kladr_id: null,
            deliveryType: "sdek",
            pickupPoints: null,
            selectedPickupPoint: null,
            selectedPickupPointId: null,
            userPickup: false
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSuggestionChange = this.handleSuggestionChange.bind(this);
        this.totalSum = this.totalSum.bind(this);
        this.calculateTax = this.calculateTax.bind(this);
        // this.totalWithTax = this.totalWithTax.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.handleRadioBtnChange = this.handleRadioBtnChange.bind(this);
        this.onPickupSet = this.onPickupSet.bind(this);
    }

    handleInputChange(event) {
        const name = event.target.name;
        let value = event.target.value;

        // console.log("change value", value);
        // console.log("event.target.getAttribute('data')",  event.target.getAttribute('data'))

        if (name === "referrer") {
            value = value.toLowerCase();
            // console.log("referrer", value);
        }

        if (name === "phone") {
            this.setState({ [name]: `${event.target.getAttribute('data')} ${value}` });
        } else {
            this.setState({ [name]: value });
        }
    }

    handleRadioBtnChange(event) {
        // console.log("event.target.name", event.target.name);
        this.setState({ deliveryType: event.target.name });

        if (event.target.name === "sdek" && this.state.city_kladr_id) {
            this.getSdekPrice(this.state.city_kladr_id);
        } else if (event.target.name === "pickup") {
            // console.log("event.target.name", event.target.name);
            // fetch pickup points from backend, save to the state

            if (!this.state.pickupPoints) {
                fetch('https://ntz.team/pickup_warehouses?lang=ru', {
                    method: 'GET',
                })
                    .then(data => {
                        var extractedData = data.json()
                        return extractedData;
                    }).then(extractedData => {
                        // console.log("backend data", extractedData);
                        // console.log("extractedData[0]", `${extractedData[0].id}, ${extractedData[0].name}, ${extractedData[0].address}`);
                        this.setState({ pickupPoints: extractedData });
                        // set default pickup point
                        this.setState({ selectedPickupPoint: `${extractedData[0].id}, ${extractedData[0].name}, ${extractedData[0].address}` })
                    })
                    .catch((error) => {
                        console.error("data is not sent, error", error);
                    });
            }
            CartActions.setDeliveryPrice(0);
            CartActions.updateTaxDeliveryTotal();
        }
        else {
            // console.log("not sdek, radio btn clicked");
            CartActions.setDeliveryPrice(0);
            CartActions.updateTaxDeliveryTotal();
        }
    }

    handleSuggestionChange(value) {
        // console.log("handleSuggestionChange value", value);
        // console.log("value.data.city", value.data.city);
        // console.log("value.data.streeе", value.data.street);
        // console.log("value.data.house", value.data.house);
        // console.log("value.data.city", value.data.city);

        // this.setState({ city: value.data.city }, 
        //     ()=>{ city: value.data.city  }
        // );

        this.setState({ city: value.data.city });
        this.setState({ street: value.data.street });
        this.setState({ zipcode: value.data.postal_code });
        this.setState({ building: value.data.house });
        this.setState({ city_kladr_id: value.data.city_kladr_id });

        if (value.data.city_kladr_id && this.state.deliveryType === "sdek") {
            this.getSdekPrice(value.data.city_kladr_id);
        }
    }

    getSdekPrice(city_kladr_id) {
        const cdekIdUrl = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/delivery";
        let cdekIdoptions = {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Token " + this.autoFillToken
            },
            body: JSON.stringify({ query: city_kladr_id })
        }

        const deliveryUrl = "https://ntz.team/countdelivery";

        fetch(cdekIdUrl, cdekIdoptions)
            .then(response => response.json())
            .then(result => {
                let delivery = {};
                delivery.order = {};
                delivery.order.items = this.props.cartItems;
                delivery.country_code = "";
                delivery.cdek_code = result.suggestions[0].data.cdek_id;
                // console.log("cdekID", delivery.cdek_code);
                let deliveryReq = JSON.stringify(delivery);

                let deliveryOptions = {
                    method: 'POST',
                    body: deliveryReq
                }
                // console.log("delivery obj", deliveryReq);

                return fetch(deliveryUrl, deliveryOptions);
            })
            .then(response => response.json())
            .then(result => {
                CartActions.setDeliveryPrice(result);
                CartActions.updateTaxDeliveryTotal();
                // console.log("delivery price", result);
            })
            .catch(error => console.log("delivery price error", error));
    }

    clearForm() {
        this.setState({ name: "" });
        this.setState({ lastName: "" });
        this.setState({ email: "" });
        this.setState({ phone: "" });
        this.setState({ address: "" });
        this.setState({ country: "" });
        this.setState({ zipcode: "" });
        this.setState({ city: "" });
    }

    handleSubmit(event) {
        event.preventDefault();
        // console.log("form submit fired");

        // vallidate form
        if (!(/[a-z]/i.test(this.state.referrer))) {
            alert(counterpart.translate("validation.onlyLatin"));
            return false;
        }
        if (this.state.email === "") {
            this.setState({ emailError: "validation.emptyEmail" });
            alert(counterpart.translate("validation.emptyEmail"));
            return false;
        }
        const emailRegExp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if (!emailRegExp.test(this.state.email)) {
            this.setState({ emailError: "validation.invalidEmail" });
            alert(counterpart.translate("validation.invalidEmail"));
            return false;
        }
        const letters = /[a-zA-Z]/;
        if (this.state.phone.length < 5 || letters.test(this.state.phone)) {
            this.setState({ phonelError: "validation.phoneError" });
            alert(counterpart.translate("validation.phoneError"));
            return false;
        }

        let request = { order: {} };
        let order = {};
        let items = this.props.cartItems;
        let type = "add_order";
        let host = "marketru.ntz.team";
        let suffix = this.state.payWithNTZ ? "FormNTZ" : "FormRUR"; // FormNTZ, FormRUR

        request.type = type;
        request.host = host;
        order.items = items;

        order.name = this.state.name;
        order.phone = this.state.phone;
        order.email = this.state.email;
        order.orderType = suffix;
        order.description = this.state.comments; // ?
        order.ntzLogin = this.state.referrer;

        let address = null;
        // do not send address fields if user chose self pickup
        if (this.state.deliveryType === "pickup") {
            order.pickup_warehouses = this.state.selectedPickupPoint;
            order.delivery_text = this.state.selectedPickupPoint;
        } else {
            order.userCountry = this.state.country;
            order.userCity = this.state.city;
            order.userStreet = this.state.street;
            order.userBuilding = this.state.building
            order.userPostal = this.state.zipcode;
            order.userFlat = this.state.apartment;
            const apartment = this.state.apartment ? `кв. ${this.state.apartment},` : "";
            address = `г. ${order.userCity}, ул. ${order.userStreet}, д. ${order.userBuilding}, ${apartment} ${order.userPostal ? order.userPostal : ""}`;

            switch (this.state.deliveryType) {
                case "mailRus":
                    order.delivery_text = `Почта России, ${address}`
                    break;
                case "onDelivery":
                    order.delivery_text = `Оплата доставки при получении заказа, ${address}`
                    break;
                default:
                    order.delivery_text = `Сдэк, ${address}`
            }
        }

        request.order = order;

        let orderRequest = JSON.stringify(request);
        console.log("orderRequest", orderRequest);


        fetch('https://ntz.team/add_order', {
            method: 'POST',
            body: orderRequest,
        })
            .then(data => {
                // console.log('Success:', data);
                this.clearForm();
                // alert("Заказ отправлен");
                var extractedData = data.json()
                return extractedData;
            }).then(extractedData => {
                // console.log("extractedData", extractedData);
                // CartActions.clearCart();
                window.location = extractedData;
            })
            .catch((error) => {
                console.error("data is not sent, error");
                console.error('Error:', error);
                alert("Ошибка отправки");
            });

    }

    handleCheckboxChange(e) {
        this.setState({ payWithNTZ: !this.state.payWithNTZ });
        // console.log(e.target.value);
    }

    _toggleLock(e) {
        e.preventDefault();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        }
        return true
    }

    totalSum() {
        let sum = this.props.cartItems.map(product => product.quantitySum).reduce((a, b) => a + b, 0);
        return Math.round(sum * 100) / 100;
    }

    calculateTax() {
        let sum = this.props.cartItems.map(product => product.quantitySum).reduce((a, b) => a + b, 0);
        let percents = sum / 100 * this.tax;
        return Math.round(percents * 100) / 100;
    }

    onPickupSet(e) {
        // console.log("e.target.value", e.target.value);
        this.setState({ selectedPickupPoint: e.target.value });

        // cut the id off, save the id to this.state.selectedPickupPointIdd
        let id = e.target.value.split(",");
        console.log("id[0]", id[0]);
        this.setState({ selectedPickupPointId: parseInt(id[0]) });
    }

    renderPickupPoints() {
        // console.log("this.state", this.state);

        // let selectedPointInfo;
        let selectedPointInfo;
        if(this.state.pickupPoints) {
            selectedPointInfo = this.state.pickupPoints[0];
            if (this.state.selectedPickupPointId) {
                selectedPointInfo = this.state.pickupPoints.find(point => point.id === this.state.selectedPickupPointId);
                // console.log("selectedPointInfo", selectedPointInfo);
            }
        }

        return (
            <div>
                {this.state.pickupPoints &&
                    <div>
                        <select className="bts-select" onChange={(e) => this.onPickupSet(e)} value={this.state.selectedPickupPoint ? this.state.selectedPickupPoint : ""}>
                            {this.state.pickupPoints.map((point, i) => { return <option key={i} value={`${point.id}, ${point.name}, ${point.address}`}>{`${point.name}, ${point.address}, ${point.working_hours}`}</option> })}
                        </select>
                            <div className="checkout__warehouse-info">
                                <div className="checkout__warehouse-title">{selectedPointInfo.name}</div>
                                <div className="checkout__warehouse-block"> 
                                    <Translate className="checkout__warehouse-subtitle" component="span" content="checkout.address" />: <span>{selectedPointInfo.address}</span>
                                </div>
                                <div className="checkout__warehouse-block">
                                    <Translate className="checkout__warehouse-subtitle" component="span" content="checkout.phone" />: <span>{selectedPointInfo.phone}</span>
                                </div>
                                <div className="checkout__warehouse-block">
                                    <Translate className="checkout__warehouse-subtitle" component="span" content="checkout.working_hours" />: <span>{selectedPointInfo.working_hours}</span>
                                </div>
                            </div>
                    </div>
                }
            </div>
        )
    }

    render() {
        let isLocked = WalletDb.isLocked();
        // console.log("checkout state.phone", this.state.phone);

        return (
            <div>
                <AboutMenu />
                <ControlBar routes={this.props.router} />
                <div className="ntz-container">
                    <form className="checkout" onSubmit={this.handleSubmit}>
                        <div className="checkout__inputs-wrapper">
                            <Translate component="h3" content="checkout.order_create" className="checkout__title" />
                            <div className="checkout__inputs">
                                <div className="checkout__user">
                                    <Translate className="checkout__sub-title" component="div" content="checkout.client_info" />
                                    <div className="checkout__input-block">
                                        <Translate className="checkout__input-label" component="label" content="checkout.referrer" />
                                        <input type="text" className="ntz-input" name="referrer" value={this.state.referrer ? this.state.referrer : ""} onChange={this.handleInputChange} required />
                                    </div>
                                    <div className="checkout__input-block">
                                        <Translate className="checkout__input-label" component="label" content="checkout.name" />
                                        <input type="text" className="ntz-input" name="name" value={this.state.name} onChange={this.handleInputChange} required />
                                        {/* <Translate className="checkout__error" component="span" content="validation.emptyEmail" /> */}
                                    </div>
                                    <div className="checkout__input-block">
                                        <Translate className="checkout__input-label" component="label" content="checkout.email" />
                                        <input type="text" className="ntz-input" className="ntz-input" name="email" value={this.state.email} onChange={this.handleInputChange} required />
                                        {/* <Translate className="checkout__error" component="span" content={this.state.emailError} /> */}
                                    </div>
                                    <div className="checkout__input-block">
                                        <PhoneInput getValue={this.handleInputChange} />

                                        {/* <Translate className="checkout__input-label" component="label" content="checkout.phone" />
                                        <InputMask mask="+9 999 999 99 99" maskChar="X" className="ntz-input" type="tel" name="phone" value={this.state.phone} onChange={this.handleInputChange} required /> */}
                                        {/* <input type="tel" className="ntz-input" name="phone" value={this.state.phone} onChange={this.handleInputChange} required /> */}
                                        {/* <Translate className="checkout__error" component="span" content={this.state.phoneError} /> */}
                                    </div>
                                </div>

                                <div className="checkout__radio-btns">
                                    <div className="checkout__radio-block">
                                        <input
                                            type="radio"
                                            id="sdek"
                                            name="sdek"
                                            value={this.state.deliveryType}
                                            checked={"sdek" === this.state.deliveryType}
                                            onChange={this.handleRadioBtnChange}
                                        />
                                        <label htmlFor="sdek">
                                            <Translate className="checkout__radio-label" component="span" content="checkout.sdek" />
                                        </label>
                                    </div>
                                    <div className="checkout__radio-block">
                                        <input
                                            type="radio"
                                            id="mailRus"
                                            name="mailRus"
                                            value={this.state.deliveryType}
                                            checked={"mailRus" === this.state.deliveryType}
                                            onChange={this.handleRadioBtnChange}
                                        />
                                        <label htmlFor="mailRus">
                                            <Translate className="checkout__radio-label" component="span" content="checkout.mail_rus" />
                                        </label>
                                    </div>
                                    <div className="checkout__radio-block">
                                        <input
                                            type="radio"
                                            id="onDelivery"
                                            name="onDelivery"
                                            value={this.state.deliveryType}
                                            checked={"onDelivery" === this.state.deliveryType}
                                            onChange={this.handleRadioBtnChange}
                                        />
                                        <label htmlFor="onDelivery">
                                            <Translate className="checkout__radio-label" component="span" content="checkout.on_delivery" />
                                        </label>
                                    </div>
                                    <div className="checkout__radio-block">
                                        <input
                                            type="radio"
                                            id="pickup"
                                            name="pickup"
                                            value={this.state.deliveryType}
                                            checked={"pickup" === this.state.deliveryType}
                                            onChange={this.handleRadioBtnChange}
                                        />
                                        <label htmlFor="pickup">
                                            <Translate className="checkout__radio-label" component="span" content="checkout.pickup" />
                                        </label>
                                    </div>
                                </div>

                                {this.state.deliveryType === "pickup" ? this.renderPickupPoints() : null}

                                {this.state.deliveryType !== "pickup" ?
                                    <div className="checkout__delivery">
                                        <Translate className="checkout__sub-title" component="div" content="checkout.delivery_info" />
                                        <div className="checkout__input-block checkout__input-block--address">
                                            <Translate className="checkout__input-label" component="label" content="checkout.address" />
                                            {/* <input type="text" className="ntz-input" name="address" value={this.state.address} onChange={this.handleInputChange} /> */}
                                            <AddressSuggestions className="ntz-input" name="address" token={this.autoFillToken} value={this.state.address} onChange={this.handleSuggestionChange} />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.adddressError} /> */}
                                        </div>
                                        <div className="checkout__input-block">
                                            <Translate className="checkout__input-label" component="label" content="checkout.country" />
                                            <input type="text" className="ntz-input" name="country" value={this.state.country} onChange={this.handleInputChange} required />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.countryError} /> */}
                                        </div>
                                        <div className="checkout__input-block">
                                            <Translate className="checkout__input-label" component="label" content="checkout.zip_code" />
                                            <input type="text" className="ntz-input" name="zipcode" value={this.state.zipcode} type="number" required />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.zipcodeEror} /> */}
                                        </div>
                                        <div className="checkout__input-block">
                                            <Translate className="checkout__input-label" component="label" content="checkout.city" />
                                            <input type="text" className="ntz-input" name="city" value={this.state.city} required />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.cityError} /> */}
                                        </div>
                                        <div className="checkout__input-block">
                                            <Translate className="checkout__input-label" component="label" content="checkout.street" />
                                            <input type="text" className="ntz-input" name="city" value={this.state.street} required />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.cityError} /> */}
                                        </div>
                                        <div className="checkout__input-block">
                                            <Translate className="checkout__input-label" component="label" content="checkout.building" />
                                            <input type="text" className="ntz-input" name="city" value={this.state.building} required />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.cityError} /> */}
                                        </div>
                                        <div className="checkout__input-block">
                                            <Translate className="checkout__input-label" component="label" content="checkout.apartment" />
                                            <input type="text" className="ntz-input" name="apartment" value={this.state.apartment} onChange={this.handleInputChange} />
                                            {/* <Translate className="checkout__error" component="span" content={this.state.cityError} /> */}
                                        </div>
                                    </div>
                                    : null}

                                <div className="checkout__textarea-wrapper">
                                    <Translate className="checkout__input-label" component="label" content="checkout.extra" />
                                    <textarea className="checkout__textarea" placeholder={counterpart.translate("checkout.comments")} name="comments" value={this.state.comments} onChange={this.handleInputChange} cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="checkout__bill">
                            <Translate component="div" content="checkout.your_order" className="checkout__bill-title" />

                            <div className="checkout__items-wrapper">
                                <div className="checkout__items-header">
                                    <Translate component="div" content="checkout.products" className="checkout__prod-names" />
                                    <Translate component="div" content="checkout.sum" className="checkout__prod-prices" />
                                </div>
                                <div className="checkout__items-table">
                                    {this.props.cartItems.map((product) => {
                                        return (
                                            <div className="checkout__items" key={product.id}>
                                                <div className="checkout__prod-names">
                                                    <span>{product.quantity}</span>
                                                    <span className="checkout__multiply">x</span>
                                                    <span>{product.name}</span>
                                                </div>
                                                <div className="checkout__prod-prices">{`${product.quantitySum}`} </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>

                            <div className="checkout__bill-subtotal">
                                <div>
                                    <Translate component="div" content="checkout.total_sum" />
                                    <div className="checkout__subtotal">{`${this.totalSum()}`} <span className="rub-sign">&#8381;</span></div>
                                </div>
                                <div>
                                    <Translate component="div" content="checkout.delivery" />
                                    <div>{`${this.props.cart.deliveryPrice}`} <span className="rub-sign">&#8381;</span></div>
                                </div>
                                <div>
                                    <div className="checkout__tax-text"><Translate component="div" content="checkout.tax" /> <span>{`(${this.tax}%)`}</span></div>
                                    <div>{this.props.cart.taxSum ? `${this.props.cart.taxSum}`: ""} <span className="rub-sign">&#8381;</span></div>
                                </div>
                            </div>

                            <div className="checkout__bill-total">
                                <div className="checkout__total-wrapper">
                                    <Translate component="div" content="checkout.total" />
                                    <div className="checkout__total-tax">{this.props.cart.TaxDeliveryTotal ? `${this.props.cart.TaxDeliveryTotal}` : 0} <span className="rub-sign">&#8381;</span></div>
                                </div>
                                <div className="checkout__promo-wrapper">
                                    <input type="checkbox" />
                                    <Translate component="div" content="checkout.promo_code" />
                                </div>
                            </div>

                            <div className="checkout__checkbox-wrapper">
                                <input type="checkbox" onClick={this.handleCheckboxChange} />
                                <Translate component="div" content="checkout.pay_ntz" />
                            </div>
                            <Translate className="ntz-btn" component="button" content="checkout.pay" type="submit" />
                        </div>
                    </form>
                </div>
                <Footer />
            </div>
        )
    }
}

Checkout = connect(Checkout, {
    listenTo() {
        return [CartStore];
    },
    getProps() {
        return CartStore.getState();
    }
})

export default Checkout;