import React from "react";
import Translate from "react-translate-component";
import { Link } from "react-router/es";
import ControlBar from "../ControlBar/ControlBar";

class CategoriesLeft extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            showCategories: false,
            width: window.innerWidth,
        };

        this.getCategories = this.getCategories.bind(this);
        this.toggleCategories = this.toggleCategories.bind(this);
        this.setBtnClass = this.setBtnClass.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.getCategories();
        window.addEventListener("resize", this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindowDimensions);
    }

    getCategories() {
        const allCategoriesUrl = "https://ntz.team/get_main_menu?lang=ru";

        fetch(allCategoriesUrl, {
            method: "GET"
        }).then(data => {
            var extractedData = data.json();
            return extractedData;
        }).then(extractedData => {
            // console.log("extractedData", extractedData);
            this.setState({ categories: extractedData });
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    toggleCategories() {
        this.setState({ showCategories: !this.state.showCategories })
    }

    setBtnClass() {
        return this.state.showCategories ? "categories-left__all-btn categories-left__all-btn--arrow-up" : "categories-left__all-btn";
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth});
    }

    render() {
        // console.log("this.state.width", this.state.width);

        return (
            <div className="categories-left">
                <Translate content="categories.all" component="div" className={this.setBtnClass()} onClick={this.toggleCategories} />
                {this.state.showCategories ?
                    <div className="categories-left__list-wrapper" onClick={this.state.width < 768 ? this.toggleCategories : null}>
                        <div className="categories-left__arrow-border">
                            <div className="categories-left__border-line"></div>
                            <div className="categories-left__triangle"></div>
                            <div className="categories-left__border-line"></div>
                        </div>
                        <div className="categories-left__list">
                            {this.state.categories.map((category, i) => {
                                return <Link to={`/category/${category.name}`} key={i} className="categories-left__item">{category.title}</Link>
                            })}
                        </div>
                    </div>
                    : null}
            </div>
        )
    }

}

export default CategoriesLeft;