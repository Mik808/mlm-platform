import React from 'react';
import Translate from "react-translate-component";
import { Waypoint } from 'react-waypoint';
import { Link } from "react-router/es";
import AddToCartBtn from "../Buttons/AddToCartBtn";
import WeekOffers from "./WeekOffers";

class CategoriesAll extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            isfetched: false,
            productsIndexLoaded: 19
        }

        this.fetchCategories = this.fetchCategories.bind(this);
        this.loadMore = this.loadMore.bind(this);
    }

    componentDidMount() {
        this.fetchCategories();
    }

    async fetchCategories() {
        try {
            // https://ntz.team/search?lang=ru&category=items_list
            let response = await fetch("https://ntz.team/search_items?lang=ru&category=main_categories_current_items_list");
            let data = await response.json();
            this.setState({ categories: data });
            // console.log("all categories", data);
        } catch (err) {
            // catches errors both in fetch and response.json
            console.log("cat err", err);
        }
    }

    loadMore() {
        console.log("waypoint fired");
        this.setState({ productsIndexLoaded: this.state.productsIndexLoaded + 20 });
    }

    render() {
        // console.log("this.state.productsIndexLoaded", this.state.productsIndexLoaded);

        return (
            <div className="categories-all ntz-container">
                <Translate component="div" content="categories.slogan" className="categories-all__slogan" />
                <div className="categories-all__inner">
                   
                    {this.state.categories ? <div className="categories-all__products">
                        <WeekOffers />
                        {this.state.categories.filter((value, i) => {
                            if (i < this.state.productsIndexLoaded) {
                                return value;
                            }
                        }).map((product, i) => {
                            return (
                                <div className="categories-all__product" key={i}>
                                    <Link to={`/product/${product.id}`} key={product.id} className="categories-all__product-link" >
                                        <div className="categories-all__image-wrapper">
                                            <img src={`https://${product.image_full_url}`} className="categories-all__image" alt="img" />
                                        </div>
                                        <div className="categories-all__price">{`${product.price}`} <span className="rub-sign">&#8381;</span></div>
                                        <div className="categories-all__name">{product.name}</div>
                                    </Link>
                                    <AddToCartBtn product={product} class="ntz-cart-btn categories-all__add" />
                                </div>
                            )
                        })
                        }
                    </div> : null
                    }
                </div>

                <Waypoint onEnter={this.loadMore} bottomOffset="-20px" />
            </div>
        )
    }
}

export default CategoriesAll;