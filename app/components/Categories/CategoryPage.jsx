import React from "react";
import AboutMenu from "../IndexMain/AboutMenu";
import ControlBar from "../ControlBar/ControlBar";
import { Link } from "react-router/es";
import AddToCartBtn from '../Buttons/AddToCartBtn';
import Footer from "../Footer/Footer";

class CategoryPage extends React.Component {

    constructor(props) {
        super(props);

        this.categoryUrl = "https://ntz.team/search_items?lang=ru&category=";

        this.state = {
            category: null
        }
    }

    componentDidMount() {
        this.fetchCategory(this.props.params.name);
    }

    componentDidUpdate(prevProps) {
        if (this.props.params.name !== prevProps.params.name) {
            this.fetchCategory(this.props.params.name);
        }
    }

    fetchCategory(name) {
        fetch(`${this.categoryUrl}${name}`)
            .then(response => response.json())
            .then(result => {
                // console.log("category fetched", JSON.stringify(result));
                this.setState({ category: result });
            })
            .catch(error => console.log("category error", error));
    }

    render() {
        // console.log("this.props.params.name", this.props.params.name);
        // console.log("this.state", this.state);

        return (
            <div>
                <AboutMenu />
                <ControlBar routes={this.props.router} />

                <div className="ntz-container">
                    <div className="category-page__wrapper">
                        <div className="category-page__list">
                            {this.state.category && this.state.category.map((product, i) => {
                                return (
                                    <div className="category-page__card" key={product.id}>
                                        <Link to={`/product/${product.id}`} >
                                            <li className="category-page__image-wrapper">
                                                <img src={`https://${product.image_full_url}`} className="category-page__image" alt="img" />
                                            </li>
                                            <li className="category-page__price">{product.price} <span className="rub-sign">&#8381;</span></li>
                                            <li className="category-page__name">{product.name}</li>
                                        </Link>
                                        <AddToCartBtn product={product} class="ntz-cart-btn" />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
                <Footer />
            </div>

        )
    }


}

export default CategoryPage;