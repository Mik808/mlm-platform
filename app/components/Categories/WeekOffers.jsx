import React from 'react';
import Slider from "react-slick";
import Translate from "react-translate-component";
import { Link } from "react-router/es";
import WeekNextArrow from "../Sliders/WeekNextArrow";
import WeekPrevArrow from "../Sliders/WeekPrevArrow";
import Timer from "../Timer/Timer";

// import "slick-carousel/slick/slick.css"; // can be imported here instead of inside scss file
// import "slick-carousel/slick/slick-theme.css";

class WeekOffers extends React.Component {

    constructor(props) {
        super(props);

        this.url = "https://ntz.team/search_items?lang=ru&category=main_categories_offer_week";
        this.idleTimer = null;

        this.settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <WeekNextArrow />,
            prevArrow: <WeekPrevArrow />
        };

        this.state = {
            weeklyOffers: null
        }

        this.fetchWeeklyOffers = this.fetchWeeklyOffers.bind(this);
    }

    componentDidMount() {
        this.fetchWeeklyOffers();
    }

    async fetchWeeklyOffers() {
        try {
            let response = await fetch(`${this.url}`);
            // console.log(`url: ${this.url}${name}`);
            let data = await response.json();
            this.state.weeklyOffers = data;
            // console.log("data", data);
        } catch (err) {
            // catches errors both in fetch and response.json
            console.log("cat err", err);
        }
    }

    render() {
        // console.log("weekly offers state", this.state);

        return (
            <div className="week-offers">
                <Translate content="categories.weekly_offers" component="div" className="week-offers__title" />
                {this.state.weeklyOffers &&
                    <Slider {...this.settings} >
                        {this.state.weeklyOffers.map((offer, i) => {
                            return (
                                <Link key={i} to={`/product/${offer.id}`}>
                                    <div className="week-offers__price-wrapper">
                                        <div className="week-offers__price">{offer.price} <span className="rub-sign">&#8381;</span></div>
                                        <div className="week-offers__old-price">{offer.old_price} <span className="rub-sign">&#8381;</span></div>
                                        <div className="week-offers__saving">
                                            <Translate content="categories.saving" className="week-offers__saving-text" />
                                            <div className="week-offers__saving-num">{offer.old_price - offer.price}  <span className="rub-sign">&#8381;</span></div>
                                        </div>
                                    </div>
                                    <div className="week-offers__img-wrapper">
                                        <img className="week-offers__img" src={`https://${offer.image_full_url}`} alt="img" />
                                    </div>
                                </Link>
                            )
                        })}
                    </Slider>
                }

                <div className="week-offers__timer">
                    <Timer />
                </div>
            </div>
        )
    }
}

export default WeekOffers;