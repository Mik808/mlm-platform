import React from 'react';
// import Carousel from 'nuka-carousel';

class PromoCategories extends React.Component {

    constructor(props) {
        super(props);

        this.categories = ["main_categories_new_items", "main_categories_sale"];
        this.url = "https://ntz.team/search?lang=ru&category=";

        this.state = {
            categories: {}
        }

        this.fetchCategories = this.fetchCategories.bind(this);
    }

    componentDidMount() {
        this.fetchCategories("main_categories_new_items");
    }

    async fetchCategories(name) {
        try {
            let response = await fetch(`${this.url}${name}`);
            // console.log(`url: ${this.url}${name}`);
            let data = await response.json();
            this.state.categories[name] = data;
            // console.log("data", data);
        } catch (err) {
            // catches errors both in fetch and response.json
            console.log("cat err", err);
        }
    }

    render() {
        // console.log("promo categories state", this.state);
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
          };

        return (
            <div>
                <div>PromoCategories Component</div>
                {/* <Carousel>
                    <div className="promo-categories__slide"> 
                        <h3>1</h3>
                    </div>
                    <div className="promo-categories__slide">
                        <h3>2</h3>
                    </div>
                    <div className="promo-categories__slide">
                        <h3>3</h3>
                    </div>
                    <div className="promo-categories__slide">
                        <h3>4</h3>
                    </div>
                    <div className="promo-categories__slide">
                        <h3>5</h3>
                    </div>
                    <div className="promo-categories__slide">
                        <h3>6</h3>
                    </div>
                </Carousel> */}

            </div>
        )
    }
}

export default PromoCategories;