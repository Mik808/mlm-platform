import React from 'react';
import { connect } from "alt-react";
import CartStore from "../../stores/CartStore";
import Translate from "react-translate-component";
import { Link } from "react-router/es";
import counterpart from "counterpart";

class ItemInCartModal extends React.Component {
    constructor(props) {
        super(props);

        this.closeModal = this.closeModal.bind(this);

        this.state = {
            showModal: false,
            itemId: null,
            newProduct: null,
        }
    }

    componentDidUpdate(prevProps) {
        // console.log("prevProps.lastItem === this.props.lastItem", prevProps.lastItem === this.props.lastItem);
        if (prevProps.lastItem !== this.props.lastItem) {
            console.log("prevProps.lastItem !== this.props.lastItem");
            if (!this.props.cart.newItem) {
                // console.log("this.props.cart.newItem", this.props.cart.newItem);
                clearTimeout(this.modalTimeout);
                this.setState({ showModal: true });
                this.setState({ newProduct: this.props.cartItems.find(product => product.id === this.props.cart.newItemId) })
                this.modalTimeout = setTimeout(() => this.setState({showModal: false}), 3000);
            } else {
                // console.log("this.props.cart.newItem", this.props.cart.newItem);
                clearTimeout(this.modalTimeout);
                this.setState({ showModal: true });
                this.setState({ newProduct: this.props.cartItems.find(product => product.id === this.props.lastItem.id) });
                this.modalTimeout = setTimeout(() => this.setState({showModal: false}), 3000);
            }
        }
    }

    closeModal() {
        this.setState({ showModal: false });
    }

    render() {
        // console.log("item modal props", this.props);
        // console.log("item modal state", this.state);

        return (
            <div>
                {this.state.showModal ?
                    <div className="item-modal">
                        <div className="item-modal__header">
                            <div className="item-modal__text">{this.props.cart.newItem ? counterpart.translate("modals.in_cart") : counterpart.translate("modals.already")}</div>
                            {/* <Translate className="item-modal__text" component="div" content={this.state.title} /> */}
                            <div className="item-modal__close" onClick={this.closeModal}></div>
                        </div>
                        {this.state.newProduct &&
                            <div className="item-modal__body">
                                <img className="item-modal__img" src={`https://${this.state.newProduct.image_full_url}`} alt="img" />
                                <div className="item-modal__data">
                                    <div className="item-modal__name">{this.state.newProduct.name}</div>
                                    <div className="item-modal__quantity">
                                        <Translate content="exchange.quantity" component="span" />: <span>{this.state.newProduct.quantity}</span>
                                    </div>
                                    <div className="item-modal__price">{this.state.newProduct.price} <span className="rub-sign">&#8381;</span></div>
                                </div>
                            </div>
                        }
                        <Link to={`/checkout`}>
                            <Translate component="button" className="ntz-btn item-modal__btn" content="cart.go_to_checkout" />
                        </Link>
                    </div> :
                    null
                }
            </div>
        )
    }
}

ItemInCartModal = connect(ItemInCartModal, {
    listenTo() {
        return [CartStore];
    },
    getProps() {
        return CartStore.getState();
    }
})

export default ItemInCartModal;