import React from "react";
import BalanceComponent from "../Utility/BalanceComponent";
import AccountActions from "actions/AccountActions";
import Translate from "react-translate-component";
import AccountSelector from "../Account/AccountSelector";
import AmountSelector from "../Utility/AmountSelector";
import utils from "common/utils";
import counterpart from "counterpart";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import Immutable from "immutable";
import { ChainStore } from "bitsharesjs/es";
import { checkFeeStatusAsync, checkBalance } from "common/trxHelper";
import { debounce } from "lodash";
import classnames from "classnames";
import { Asset } from "common/MarketClasses";



class Message extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            to_name: this.props.to_name,
            from_account: this.props.from_account,
            to_account: null,
            amount: "",
            asset_id: null,
            asset: null,
            memo: "",
            error: null,
            propose: false,
            propose_account: "",
            feeAsset: null,
            fee_asset_id: "1.3.0",
            feeAmount: new Asset({ amount: 0 }),
            feeStatus: {}
        };

        this.onTrxIncluded = this.onTrxIncluded.bind(this);
        this._updateFee = debounce(this._updateFee.bind(this), 250);
        this._checkFeeStatus = this._checkFeeStatus.bind(this);
        this._checkBalance = this._checkBalance.bind(this);
        this.closeMessageModal = this.closeMessageModal.bind(this);
    }

    componentWillMount() {
        this.nestedRef = null;
        this._updateFee();
    }

    shouldComponentUpdate(np, ns) {
        let { asset_types: current_types } = this._getAvailableAssets();
        let { asset_types: next_asset_types } = this._getAvailableAssets(ns);

        if (next_asset_types.length === 1) {
            let asset = ChainStore.getAsset(next_asset_types[0]);

            if (next_asset_types[0] !== this.state.fee_asset_id) {
                if (asset && this.state.fee_asset_id !== next_asset_types[0]) {
                    this.setState({
                        feeAsset: asset,
                        fee_asset_id: next_asset_types[0]
                    });
                }
            }
        }
        return true;
    }

    _checkBalance() {
        const { feeAmount, amount, asset } = this.state;
        let from_account = this.props.from_account;
        if (!asset) return;
        const balanceID = from_account.getIn(["balances", asset.get("id")]);
        const feeBalanceID = from_account.getIn(["balances", feeAmount.asset_id]);
        if (!asset || !from_account) return;
        if (!balanceID) return this.setState({ balanceError: true });
        let balanceObject = ChainStore.getObject(balanceID);
        let feeBalanceObject = feeBalanceID ? ChainStore.getObject(feeBalanceID) : null;
        if (!feeBalanceObject || feeBalanceObject.get("balance") === 0) {
            this.setState({ fee_asset_id: "1.3.0" }, this._updateFee);
        }
        if (!balanceObject || !feeAmount) return;
        const hasBalance = checkBalance(amount, asset, feeAmount, balanceObject);
        if (hasBalance === null) return;
        this.setState({ balanceError: !hasBalance });
    }

    _checkFeeStatus() {
        let account = this.state.from_account;

        const assets = Object.keys(account.get("balances").toJS()).sort(utils.sortID);
        let feeStatus = {};
        let p = [];
        assets.forEach(a => {
            p.push(checkFeeStatusAsync({
                accountID: account.get("id"),
                feeID: a,
                options: ["price_per_kbyte"],
                data: {
                    type: "memo",
                    content: this.state.memo
                }
            }));
        });
        Promise.all(p).then(status => {
            assets.forEach((a, idx) => {
                feeStatus[a] = status[idx];
            });
            if (!utils.are_equal_shallow(this.state.feeStatus, feeStatus)) {
                this.setState({
                    feeStatus
                });
            }
            this._checkBalance();
        }).catch(err => {
            console.error(err);
        });
    }

    _updateFee(state = this.state) {
        let { fee_asset_id, from_account } = state;
        const { fee_asset_types } = this._getAvailableAssets(state);
        if (fee_asset_types.length === 1 && fee_asset_types[0] !== fee_asset_id) {
            fee_asset_id = fee_asset_types[0];
        }
        if (!from_account) return null;
        checkFeeStatusAsync({
            accountID: from_account.get("id"),
            feeID: fee_asset_id,
            options: ["price_per_kbyte"],
            data: {
                type: "memo",
                content: state.memo
            }
        })
            .then(({ fee, hasBalance, hasPoolBalance }) => {
                this.setState({
                    feeAmount: fee,
                    fee_asset_id: fee.asset_id,
                    hasBalance,
                    hasPoolBalance,
                    error: (!hasBalance || !hasPoolBalance)
                });
            });
    }

    onToAccountChanged(to_account) {
        this.setState({ to_account, error: null });
    }

    onFeeChanged({ asset }) {
        this.setState({ feeAsset: asset, fee_asset_id: asset.get("id"), error: null }, this._updateFee);
    }

    onMemoChanged(e) {
        this.setState({ memo: e.target.value }, this._updateFee);
        document.getElementById("messageBtn"+this.props.to_id).disabled = false;
    }

    onTrxIncluded(confirm_store_state) {
        if (confirm_store_state.included && confirm_store_state.broadcasted_transaction) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        } else if (confirm_store_state.closed) {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.reset();
        }
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({ error: null });

        document.getElementById("messageBtn"+this.props.to_id).disabled = true;

        AccountActions.sendMessage(
            this.state.from_account.get('id'),
            this.state.to_account.get("id"),
            this.state.memo ? new Buffer(this.state.memo, "utf-8") : this.state.memo,
            "1.3.0"
        ).then(() => {
            TransactionConfirmStore.unlisten(this.onTrxIncluded);
            TransactionConfirmStore.listen(this.onTrxIncluded);
            this.closeMessageModal();
        }).catch(e => {
            let msg = e.message ? e.message.split('\n')[1] : null;
            console.log("error: ", e, msg);
            this.setState({ error: msg });
        });
    }

    setNestedRef(ref) {
        this.nestedRef = ref;
    }

    _setTotal(asset_id, balance_id) {
        const { feeAmount } = this.state;
        let balanceObject = ChainStore.getObject(balance_id);
        let transferAsset = ChainStore.getObject(asset_id);

        let balance = new Asset({ amount: balanceObject.get("balance"), asset_id: transferAsset.get("id"), precision: transferAsset.get("precision") });

        if (balanceObject) {
            if (feeAmount.asset_id === balance.asset_id) {
                balance.minus(feeAmount);
            }
            this.setState({ amount: balance.getAmount({ real: true }) }, this._checkBalance);
        }
    }

    _getAvailableAssets(state = this.state) {
        const { feeStatus } = this.state;
        function hasFeePoolBalance(id) {
            if (feeStatus[id] === undefined) return true;
            return feeStatus[id] && feeStatus[id].hasPoolBalance;
        }

        function hasBalance(id) {
            if (feeStatus[id] === undefined) return true;
            return feeStatus[id] && feeStatus[id].hasBalance;
        }

        const {from_error } = state;
        let from_account = this.props.account;

        let asset_types = [], fee_asset_types = [];
        if (!(from_account && from_account.get("balances") && !from_error)) {
            return { asset_types, fee_asset_types };
        }
        let account_balances = from_account.get("balances").toJS();

        asset_types = Object.keys(account_balances).sort(utils.sortID);
        fee_asset_types = Object.keys(account_balances).sort(utils.sortID);
        for (let key in account_balances) {
            let balanceObject = ChainStore.getObject(account_balances[key]);
            if (balanceObject && balanceObject.get("balance") === 0) {
                asset_types.splice(asset_types.indexOf(key), 1);
                if (fee_asset_types.indexOf(key) !== -1) {
                    fee_asset_types.splice(fee_asset_types.indexOf(key), 1);
                }
            }
        }

        fee_asset_types = fee_asset_types.filter(a => {
            return hasFeePoolBalance(a) && hasBalance(a);
        });

        return { asset_types, fee_asset_types };
    }

    closeMessageModal() {
        document.getElementById("messageOverlay_"+this.props.to_id).classList.remove('message__overlay--active');
    }

    render() {
        let from_error = null;
        let { to_account, feeAmount, to_name,  memo, feeAsset, fee_asset_id, balanceError } = this.state;
        let from_account = this.props.account;

        let { asset_types, fee_asset_types } = this._getAvailableAssets();
        let balance = null;

        // Estimate fee
        let fee = this.state.feeAmount.getAmount({ real: true });
        if (from_account && from_account.get("balances") && !from_error) {

            let account_balances = from_account.get("balances").toJS();
            if (asset_types.length === 1) asset = ChainStore.getAsset(asset_types[0]);
            if (asset_types.length > 0) {
                let current_asset_id = asset ? asset.get("id") : asset_types[0];
                let feeID = feeAsset ? feeAsset.get("id") : "1.3.0";
                balance = (<span style={{ borderBottom: "#A09F9F 1px dotted", cursor: "pointer" }} onClick={this._setTotal.bind(this, current_asset_id, account_balances[current_asset_id], fee, feeID)}><Translate component="span" content="transfer.available" />: <BalanceComponent balance={account_balances[current_asset_id]} /></span>);
            } else {
                balance = "No funds";
            }
        }
        const isToAccountValid = to_account && to_account.get("name") === to_name;
        const isSendNotValid = !isToAccountValid || memo.length == 0 || from_error || balanceError;
        let accountsList = Immutable.Set();
        accountsList = accountsList.add(from_account);
        let tabIndex = 1;

        return (
            <div className="message__overlay" id={"messageOverlay_"+this.props.to_id}>
                <div className="message__wrap">
                    <span className="message__close--btn" onClick={this.closeMessageModal}>×</span>
                    <form className="message__form" onSubmit={this.onSubmit.bind(this)} noValidate>
                        {/*  T O  */}
                        <div className="content-block">
                            <AccountSelector
                                label="transfer.to"
                                accountName={this.state.to_name}
                                onAccountChanged={this.onToAccountChanged.bind(this)}
                                account={this.state.to_name}
                                size={60}
                                tabIndex={tabIndex++}
                                allowUppercase={true}
                            />
                        </div>
                        {/*  M E M O  */}
                        <div className="content-block transfer-input">
                            {memo && memo.length ? <label className="right-label">{memo.length}</label> : null}
                            <Translate className="left-label tooltip" component="label" content="transfer.message" data-place="top" data-tip={counterpart.translate("tooltip.memo_tip")} />
                            <textarea style={{ marginBottom: 0 }} rows="6" value={memo} tabIndex={tabIndex++} onChange={this.onMemoChanged.bind(this)} />

                        </div>

                        {/*  F E E   */}
                        <div className="content-block transfer-input fee-row">
                            <div className="message__field--hidden">
                                <AmountSelector

                                    refCallback={this.setNestedRef.bind(this)}
                                    label="transfer.price"
                                    disabled={true}
                                    amount={fee}
                                    onChange={this.onFeeChanged.bind(this)}
                                    asset={fee_asset_types.length && feeAmount ? feeAmount.asset_id : (fee_asset_types.length === 1 ? fee_asset_types[0] : fee_asset_id ? fee_asset_id : fee_asset_types[0])}
                                    assets={fee_asset_types}
                                    tabIndex={tabIndex++}
                                    error={this.state.hasPoolBalance === false ? "transfer.errors.insufficient" : null}
                                />
                            </div>

                            <button className={classnames("button", { disabled: isSendNotValid })} type="submit" value="Submit" tabIndex={tabIndex++}  id={"messageBtn"+this.props.to_id}>
                                <Translate component="span" content="transfer.send" />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Message;