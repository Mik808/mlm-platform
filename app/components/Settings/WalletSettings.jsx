import React from "react";
import { ChangeActiveWallet, WalletDelete} from "../Wallet/WalletManager";
import BalanceClaimActive from "../Wallet/BalanceClaimActive";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import WalletDb from "stores/WalletDb";

export default class WalletSettings extends React.Component {

    constructor() {
        super();

        this.state = {
            lookupActive: false,
            resetMessage: null
        };
    }

    onLookup() {
        this.setState({
            lookupActive: true
        });
    }

    onResetBrainkeySequence() {
        WalletDb.resetBrainKeySequence();
        this.setState({
            resetMessage: counterpart.translate("wallet.brainkey_reset_success")
        });
    }

    render() {
        return (
            <div>
                <ChangeActiveWallet />

                <section style={{marginTop: "20px"}} className="settings__item">
                    <header><Translate content="wallet.brainkey_seq_reset" />:</header>
                </section>
                <div style={{paddingBottom: 10}}>

                    <p><Translate unsafe content="wallet.brainkey_seq_reset_text" /></p>
                    <div className="settings__btn-cancel" onClick={this.onResetBrainkeySequence.bind(this)}>
                            <Translate content="wallet.brainkey_seq_reset_button" />
                    </div>
                    {this.state.resetMessage ? <p style={{paddingTop: 10}} className="facolor-success">{this.state.resetMessage}</p> : null}
                </div>
                
                <WalletDelete />
            </div>
        );
    }
};
