import React from "react";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import SettingsActions from "actions/SettingsActions";
import AssetName from "../Utility/AssetName";


export default class SettingsEntry extends React.Component {

    constructor() {
        super();

        this.state = {
            message: null
        };
    }

    _setMessage(key) {
        this.setState({
            message: counterpart.translate(key)
        });

        this.timer = setTimeout(() => {
            this.setState({ message: null });
        }, 4000);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    render() {
        let { defaults, setting, settings, apiServer } = this.props;
        let options, optional, confirmButton, value, input, selected = settings.get(setting);
        let noHeader = false;

        switch (setting) {
            case "locale":
                value = selected;
                options = defaults.map(entry => {
                    let translationKey = "languages." + entry;
                    let value = counterpart.translate(translationKey);

                    return <option key={entry} value={entry}>{value}</option>;
                });

                break;

            case "walletLockTimeout":
                value = selected;
                input = <input type="text" className="settings__timeout" value={selected} onChange={this.props.onChange.bind(this, setting)} />;
                break;

            case "reset":
                value = true;

                input = <div
                    className="settings__btn-cancel"
                    onClick={() => { SettingsActions.clearSettings().then(() => { this._setMessage("settings.restore_default_success"); }); }}
                >
                    {counterpart.translate("settings.reset")}
                </div>;

                noHeader = true;
                break;

            default:

                if (typeof selected === "number") {
                    value = defaults[selected];
                }
                else if (typeof selected === "boolean") {
                    if (selected) {
                        value = defaults[0];  
                    } else {
                        value = defaults[1];
                    }
                }
                else if (typeof selected === "string") {
                    value = selected;
                }


                if (defaults) {
                    options = defaults.map((entry, index) => {
                        let option = entry.translate ? counterpart.translate(`settings.${entry.translate}`) : entry;
                        if (setting === "unit") {
                            option = <AssetName name={entry} />;
                        }
                        let key = entry.translate ? entry.translate : entry;
                        return <option value={entry.translate ? entry.translate : entry} key={key}>{option}</option>;
                    });
                } else {
                    input = <input className="settings__item" type="text" defaultValue={value} onBlur={this.props.onChange.bind(this, setting)} />;
                }
                break;
        }
        if ((typeof value !== "number" && !value) && !options) return null;

        if (value && value.translate) {
            value = value.translate;
        }

        return (
            <section className="settings__wrap">
                {noHeader ? null : <header className="settings__header"><Translate component="span" content={`settings.${setting}`} /></header>}
                {options ? <ul className="settings__list">   
                    <li>
                        {optional}
                        <select className="settings__item" value={value} onChange={this.props.onChange.bind(this, setting)}>
                            {options}
                        </select>
                        {confirmButton}
                    </li>
                </ul> : null}
                {input ? <ul className="settings__list"><li>{input}</li></ul> : null}

                <div className="facolor-success">{this.state.message}</div>
            </section>
        );
    }
}
