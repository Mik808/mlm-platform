import React from "react";
import Translate from "react-translate-component";
import AccountStore from "stores/AccountStore";
import ls from "common/localStorage";


var health = require("assets/app_img/merchants/health.png");
var education = require("assets/app_img/merchants/education.png");
var finance = require("assets/app_img/merchants/finance.png");
var relations = require("assets/app_img/merchants/relations.png");
var selfDev = require("assets/app_img/merchants/self-dev.png");
var travel = require("assets/app_img/merchants/travel.png");


const STORAGE_KEY = "__graphene__";
let ss = new ls(STORAGE_KEY);


class MarketPlace extends React.Component {
    constructor(props) {
        super();
        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            locale: ss.get("settings_v3").locale,
            merchantsArray: []
        };
        this.selectValue = this.selectValue.bind(this);
    }

    componentDidMount() {
        const xhr = new XMLHttpRequest();
        let url = 'https://api.ntz.team/get_merchants';
        let locale = this.state.locale;

        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        var data = 'lang=' + locale;
        xhr.send(data);

        xhr.onreadystatechange = () => {
            if (xhr.status == 200) {
                if (xhr.responseText != '') {
                    this.setState({
                        merchantsArray: JSON.parse(xhr.responseText)
                    })
                }
            }
            else {
                alert("Error");
            }
        }
    }

    selectValue(id, e) {
        document.getElementById(id).select()
    }

    render() {
        let currentAccount = AccountStore.getState().currentAccount;
        let merchantsArray = this.state.merchantsArray;

        // make certain stores appear first in the list
        let filteredMerchants = [];
        filteredMerchants.push( merchantsArray.find(merchant => merchant.name === "Быстрый старт") );
        filteredMerchants.push( merchantsArray.find(merchant => merchant.name === "Магазин Здоровья NTZ") );
        filteredMerchants.push( merchantsArray.find(merchant => merchant.name === "Магазин Красоты NTZ") );
        filteredMerchants = filteredMerchants.filter(merchant => !(merchant == undefined) )

        merchantsArray =  merchantsArray.filter(merchant => !(merchant.name === "Быстрый старт" || merchant.name === "Магазин Здоровья NTZ" || merchant.name === "Магазин Красоты NTZ"));
        filteredMerchants = filteredMerchants.concat(merchantsArray);

        return (
            <div className="marketplace__wrap">
                <div className="marketplace__content">
                    <div className="marketplace__inner">
                        <Translate className="marketplace__title" component="h1" content="marketplace.title" />
                        <Translate className="marketplace__description" component="p" content="marketplace.description" />
                        {filteredMerchants.length > 0 ?
                            <ul className="marketplace__list">
                                {filteredMerchants.map(merchant => 
                                    <li className="marketplace__item" key={merchant['id']}>
                                        <div>
                                            <div className="marketplace__logo-wrap">
                                                <img className="marketplace__logo" src={merchant['logo']} alt={merchant['name']} />
                                            </div>
                                            <p className="marketplace__name">{merchant['name']}</p>

                                            <span className="marketplace__detail">
                                                {merchant['description']}
                                            </span>
                                        </div>

                                        <div>
                                            <div className="marketplace__icons-wrap">
                                                <img className={merchant['health'] ? "marketplace__img marketplace__img--active" : "marketplace__img"} src={health} />
                                                <img className={merchant['education'] ? "marketplace__img marketplace__img--active" : "marketplace__img"} src={education} />
                                                <img className={merchant['finance'] ? "marketplace__img marketplace__img--active" : "marketplace__img"} src={finance} />
                                                <img className={merchant['relations'] ? "marketplace__img marketplace__img--active" : "marketplace__img"} src={relations} />
                                                <img className={merchant['self_dev'] ? "marketplace__img marketplace__img--active" : "marketplace__img"} src={selfDev} />
                                                <img className={merchant['travel'] ? "marketplace__img marketplace__img--active" : "marketplace__img"} src={travel} />
                                            </div>
                                            {merchant['has_referral'] ?
                                                <div>
                                                    <Translate className="marketplace__ref-text" component="p" content="marketplace.ref_link" />
                                                    <input
                                                        className="marketplace__referral-link"
                                                        id={"refLink_" + merchant['id']}
                                                        defaultValue={`https://${merchant['host']}/?r=${this.state.currentAccount
                                                            }`}
                                                        onClick={this.selectValue.bind(this, "refLink_" + merchant['id'])}
                                                        readOnly
                                                    />
                                                </div>
                                                : null}
                                            <a className="marketplace__link" href={`https://${merchant['host']}/?ntz_login=${currentAccount}`} target="_blank">
                                                <Translate content="marketplace.link" />
                                            </a>
                                        </div>
                                    </li>)}
                            </ul>
                            : null}
                    </div>
                </div>
            </div>
        )
    }
}

export default MarketPlace;