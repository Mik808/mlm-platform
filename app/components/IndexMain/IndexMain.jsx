import React from "react";
import AboutMenu from "./AboutMenu"
import WalletUnlockActions from "actions/WalletUnlockActions";
import SettingsStore from "stores/SettingsStore";
import AccountStore from "stores/AccountStore";
import AccountActions from "actions/AccountActions";
import Translate from "react-translate-component";
import CategoriesAll from "../Categories/CategoriesAll";
import ControlBar from "../ControlBar/ControlBar";
import Footer from "../Footer/Footer";
import TopSlider from "../Sliders/TopSlider";
import CartActions from "../../actions/CartActions";

class IndexMain extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount
        };
    }

    componentDidMount() {
        if (this.state.currentAccount) {
            this.props.router.push({query: {u: `${this.state.currentAccount}`, }});
        }
    }

    loginForm() {
        WalletUnlockActions.unlock().then(() => {
            AccountActions.tryToSetCurrentAccount();

            let currentAccount = AccountStore.getState().currentAccount;

            if (window.innerWidth < 600) {
                // this.context.router.push(`mobile/account/${currentAccount}/dashboard`); 
                this.props.router.push(`mobile/account/${currentAccount}/dashboard`);
            }
            else {
                // this.context.router.push(`account/${currentAccount}/dashboard`); // does not redirect
                this.props.router.push(`account/${currentAccount}/dashboard`);
            }
        });
    }

    render() {
        let currentAccount = this.state.currentAccount;

        return (
            <div className="index-main">
                <AboutMenu />
                {/* <TopMenu /> */}
                <ControlBar routes={this.props.router} />

                {/* <SearchResults /> */}

                <div className="ntz-container">
                    {/* <div className="index-main__banner"></div> */}
                    <TopSlider />
                    <div className="index-main__promo-bar">
                        <div className="index-main__promo-tile">
                            <div className="index-main__promo-pic"></div>
                            <div><Translate component="span" content="index_main.delivery" /> <span className="rub-sign">&#8381;</span></div>
                        </div>
                        <div className="index-main__promo-tile">
                            <div className="index-main__promo-pic index-main__promo-pic--shopping"></div>
                            <Translate component="div" content="index_main.shopping" />
                        </div>
                        <div className="index-main__promo-tile">
                            <div className="index-main__promo-pic index-main__promo-pic--range"></div>
                            <Translate component="div" content="index_main.product_range" />
                        </div>
                        <div className="index-main__promo-tile">
                            <div className="index-main__promo-pic index-main__promo-pic--profit"></div>
                            <Translate component="div" content="index_main.profit" />
                        </div>
                    </div>
                </div>
                {/* <PromoCategories /> */}
                {/* <WeekOffers /> */}

                <CategoriesAll />
                {/* <About /> */}
                {/* <Cart /> */}

                <Footer />
            </div>
        )
    }

}

export default IndexMain;