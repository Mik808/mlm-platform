import React from "react";
import { Link } from "react-router/es";
import Translate from "react-translate-component";


class AboutMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        return (
            <div className="about-menu">
                <div className="ntz-container about-menu__wrapper">
                    <div className="about-menu__list">
                        <Link to={`/about/ntz`} className="about-menu__item">
                            <Translate component="span" content="landing.header.item_01" />
                        </Link>
                        <Link to={`/about/cooperation/`} className="about-menu__item"  >
                            <Translate component="span" content="landing.header.item_02" />
                        </Link>
                        <Link to={`/about/education/`} className="about-menu__item" >
                            <Translate component="span" content="landing.header.item_03" />
                        </Link>
                        <Link to={`/about/goods/`} className="about-menu__item" >
                            <Translate component="span" content="landing.header.item_04" />
                        </Link>
                        <Link to={`/about/technology/`} className="about-menu__item" >
                            <Translate component="span" content="landing.header.item_05" />
                        </Link>
                    </div>
                </div>
            </div>
        )

    }
}

export default AboutMenu;