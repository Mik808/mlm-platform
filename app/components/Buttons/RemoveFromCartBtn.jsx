import React from 'react';
import Translate from "react-translate-component";
import CartActions from "../../actions/CartActions";

class RemoveFromCartBlock extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

        }

        this.removeFromCart = this.removeFromCart.bind(this);
    }

    removeFromCart(product) {
        console.log("RemoveFromCartBlock btn clicked", product);
        CartActions.removeProduct(product); 
        // CartActions.totalWithTax();
        CartActions.updateTaxDeliveryTotal();
    }

    render() {
        console.log("this.props", this.props);
        return (
            <button className={this.props.cssClass} onClick={() => this.removeFromCart(this.props.product)}></button>
        )
    }
}

export default RemoveFromCartBlock;