import React from 'react';
import Translate from "react-translate-component";
import CartActions from "../../actions/CartActions";
import { getDeliveryPrice } from "../../lib/common/delivery";
import { connect } from "alt-react";
import CartStore from "../../stores/CartStore";

class AddToCartBtn extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

        }

        this.addtoCart = this.addtoCart.bind(this);
    }

    addtoCart(product) {
        // console.log("addtoCart btn clicked", product);
        CartActions.addProduct(product, product.id); 
        CartActions.updateTaxDeliveryTotal();
    }

    render() {
        // console.log("addToartBtn props", this.props);
        return (
            <button className={this.props.class} onClick={() => this.addtoCart(this.props.product)}><Translate component="span" content="shop.toShopcart" /></button>
        )
    }
}

AddToCartBtn = connect(AddToCartBtn, {
    listenTo() {
        return [CartStore];
    },
    getProps() {
        return CartStore.getState();
    }
})

export default AddToCartBtn;