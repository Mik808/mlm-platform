import React from 'react';
import SearchGoodsStore from "../../stores/SearchGoodsStore";
import { connect } from "alt-react";
import Translate from "react-translate-component";
import FilterSearch from "./FilterSearch";
import { Link } from "react-router/es";
// import CartActions from "../../actions/CartActions";
import AddToCartBtn from '../Buttons/AddToCartBtn';
import AboutMenu from "../IndexMain/AboutMenu";
import ControlBar from "../ControlBar/ControlBar";
import Footer from "../Footer/Footer";

class SearchResults extends React.Component {

    constructor(props) {
        super(props);

        // this.addtoCart = this.addtoCart.bind(this);
    }

    // componentDidMount() {
    //     SearchGoodsStore.listen(this.onChange);
    // }

    // componentWillUnmount() {
    //     SearchGoodsStore.unlisten(this.onChange);
    // }

    // addtoCart(product) {
    //     CartActions.addProduct(product); // Dispatch.dispatch(...): Cannot dispatch in the middle of a dispatch.
    // }

    render() {
        console.log("SearchResults props", this.props);

        return (
            <div>
                <AboutMenu />
                <div className="ntz-container">
                    <ControlBar />
                    {this.props.searchResults.length ?
                        <div className="search-results">
                            {/* <FilterSearch /> */}
                            <div className="search-results__list">{this.props.searchResults.map((product) => {
                                return (
                                    <div className="search-results__card" key={product.id}>
                                        <Link to={`/product/${product.id}`} >
                                            <li className="search-results__image-wrapper">
                                                <img src={`https://${product.image_full_url}`} className="search-results__image" alt="img" />
                                            </li>
                                            <li className="search-results__price">{product.price} <span className="rub-sign">&#8381;</span></li>
                                            <li className="search-results__name">{product.name}</li>
                                            {/* <button className="search-results__card-btn" onClick={this.addtoCart(product)}><Translate component="span" content="shop.toShopcart" /></button> */}
                                        </Link>
                                        <AddToCartBtn product={product} class="ntz-cart-btn" />
                                    </div>
                                )
                            })}
                            </div>
                        </div> : 
                        <Translate content="search.no_results" component="div" />
                    }
                </div>
                <Footer />
            </div>
        )
    }
}

SearchResults = connect(SearchResults, {
    listenTo() {
        return [SearchGoodsStore];
    },
    getProps() {
        return SearchGoodsStore.getState();
    }
});

export default SearchResults;