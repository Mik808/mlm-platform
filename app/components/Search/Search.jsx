import React from 'react';
import SearchGoodsActions from "../../actions/SearchGoodsActions";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import { withRouter } from "react-router";

class Search extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchQuery: "",
            searchResult: null
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({ searchQuery: event.target.value });
    }

    onFormSubmit(e) {
        e.preventDefault();

        const productSearch = "https://ntz.team/search_items?lang=ru&q=";
        let query = this.state.searchQuery;

        fetch(`${productSearch}${query}`)
            .then(data => {
                var searchResult = data.json()
                return searchResult;
            }).then(searchResult => {
                SearchGoodsActions.updateSearchResults(searchResult);
                // window.location = "http://localhost:8080/search";  // https://dev.ntz.team/search http://localhost:8080/search
                this.props.router.push(`/search`);
            })
            .catch((error) => {
                console.error(' search error:', error);
            });

        // async function sendSearchQuery() {
        //     let response = await fetch(`${productSearch}${query}`);
        //     let searchResult = await response.json();

        //     // console.log("searchResult", searchResult);

        //     SearchGoodsActions.updateSearchResults(searchResult);
        // }

        // sendSearchQuery();
        // this.props.history.push('/home'); // does not work
        
    }

    render() {
        // console.log("search props", this.props);
        return (
            <form onSubmit={this.onFormSubmit} className="search">
                <input type="text" value={this.state.searchQuery} onChange={this.handleInputChange} placeholder={counterpart.translate("search.buy")} className="search__input" />
                <button className="search__submit-btn" type="submit"><Translate content="search.find" /></button>
            </form>
        )
    }
}

export default withRouter(Search);