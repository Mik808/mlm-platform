import React from "react";
import Translate from "react-translate-component";
import { Link } from "react-router/es";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";


var feature_01 = require("assets/app_img/landing/feature_01.png");
var feature_02 = require("assets/app_img/landing/feature_02.png");
var feature_03 = require("assets/app_img/landing/feature_03.png");
var feature_04 = require("assets/app_img/landing/feature_04.png");
var feature_05 = require("assets/app_img/landing/feature_05.png");
var feature_06 = require("assets/app_img/landing/feature_06.png");

var sphere_01 = require("assets/app_img/landing/sphere_01.png");
var sphere_02 = require("assets/app_img/landing/sphere_02.png");
var sphere_03 = require("assets/app_img/landing/sphere_03.png");
var sphere_04 = require("assets/app_img/landing/sphere_04.png");
var sphere_05 = require("assets/app_img/landing/sphere_05.png");
var merchant_01 = require("assets/app_img/landing/merchant_01.png");
var merchant_02 = require("assets/app_img/merchants/neoron.png");
var merchant_03 = require("assets/app_img/landing/merchant_03.png");
var merchant_04 = require("assets/app_img/landing/merchant_04.png");
var merchant_05 = require("assets/app_img/merchants/empireo.png");
var dariLogo = require("assets/app_img/merchants/dari.png");
var ecoidealLogo = require("assets/app_img/merchants/ecoideal.png");
var emperioLogo = require("assets/app_img/merchants/emperio.png");
var eramineralsLogo = require("assets/app_img/merchants/era-minerals.png");
var festivesunriseLogo = require("assets/app_img/merchants/festive-sunrise.png");
var haznaLogo = require("assets/app_img/merchants/hazna.png");
var intechLogo = require("assets/app_img/merchants/intech.png");
var kerasysLogo = require("assets/app_img/merchants/kerasys.png");
var lamaraLogo = require("assets/app_img/merchants/lamara.png");
var lavazzaLogo = require("assets/app_img/merchants/lavazza.png");
var limecosmeticsLogo = require("assets/app_img/merchants/lime-cosmetics.png");
var rekrevitLogo = require("assets/app_img/merchants/rekrevit.png");
var sollLogo = require("assets/app_img/merchants/soll.png");
var stressreliefLogo = require("assets/app_img/merchants/stressrelief.png");
var ntzLogo = require("assets/logo-ico-rc.png");

var tech_01 = require("assets/app_img/landing/tech_01.png");
var tech_02 = require("assets/app_img/landing/tech_02.png");
var tech_03 = require("assets/app_img/landing/tech_03.png");
var tech_04 = require("assets/app_img/landing/tech_04.png");
var tech_05 = require("assets/app_img/landing/tech_05.png");
var tech_06 = require("assets/app_img/landing/tech_06.png");
var tech_07 = require("assets/app_img/landing/tech_07.png");
var tech_08 = require("assets/app_img/landing/tech_08.png");
var tech_09 = require("assets/app_img/landing/tech_09.png");
var tech_10 = require("assets/app_img/landing/tech_10.png");
var tech_11 = require("assets/app_img/landing/tech_11.png");

class About extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentAccount: AccountStore.getState().currentAccount,
            referralAccount: "",
        };
    }

    componentDidMount() {
        if (AccountStore.getState().referralAccount) {
            this.setState({
                referralAccount: AccountStore.getState().referralAccount
            })
        }
    }

    loginForm() {
        WalletUnlockActions.unlock().then(() => {
            AccountActions.tryToSetCurrentAccount();

            let currentAccount = AccountStore.getState().currentAccount;

            if (window.innerWidth < 600) {
                this.context.router.push(`mobile/account/${currentAccount}/dashboard`);
            }
            else
                this.context.router.push(`account/${currentAccount}/dashboard`);
        });
    }

    renderAbout() {
        switch (this.props.params.section) {
            case "ntz":
                return (
                    <section className="start-about" ref="aboutBlock">
                        <div className="start__container">
                            <div className="start-about__inner">
                                <Translate className="start-about__text" component="p" content="landing.about-block.text_01" />
                                <Translate className="start-about__text start-about__text--mission" component="p" content="landing.about-block.mission" />
                                <Translate className="start-about__text start-about__text--highlighted" component="p" content="landing.about-block.text_02" />

                            </div>
                            {this.state.currentAccount ?
                                <span onClick={this.loginForm.bind(this)} className="start-header__btn start-header__btn--about">
                                    <Translate component="span" content="landing.login" />
                                </span>
                                :
                                <Link to={`/registration`} className="start-header__btn start-header__btn--about">
                                    <Translate component="span" content="landing.registration" />
                                </Link>
                            }
                        </div>
                    </section>
                )
                break;
            case "cooperation":
                return (
                    <section className="features" ref="featuresBlock">
                        <div className="start__container">
                            <Translate className="features__title" component="p" content="landing.features.title" />
                            <Translate className="features__intro" component="p" content="landing.features.intro" />
                            <Translate className="features__description" component="p" content="landing.features.description" />

                            <ul className="features__list">
                                <li className="features__item">
                                    <div className="features__img-wrap">
                                        <img src={feature_01} alt="" />
                                    </div>
                                    <div className="features__inner">
                                        <Translate className="features__subtitle" component="span" content="landing.features.title_01" />
                                        <Translate className="features__text" component="p" content="landing.features.text_01" />
                                    </div>
                                </li>
                                <li className="features__item features__item--reverse">
                                    <div className="features__img-wrap">
                                        <img src={feature_02} alt="" />
                                    </div>
                                    <div className="features__inner">
                                        <Translate className="features__subtitle" component="span" content="landing.features.title_02" />
                                        <Translate className="features__text" component="p" content="landing.features.text_02" />
                                    </div>
                                </li>
                                <li className="features__item">
                                    <div className="features__img-wrap">
                                        <img src={feature_03} alt="" />
                                    </div>
                                    <div className="features__inner">
                                        <Translate className="features__subtitle" component="span" content="landing.features.title_03" />
                                        <Translate className="features__text" component="p" content="landing.features.text_03" />
                                    </div>
                                </li>
                                <li className="features__item features__item--reverse">
                                    <div className="features__img-wrap">
                                        <img src={feature_04} alt="" />
                                    </div>
                                    <div className="features__inner">
                                        <Translate className="features__subtitle" component="span" content="landing.features.title_04" />
                                        <Translate className="features__text" component="p" content="landing.features.text_04" />
                                    </div>
                                </li>
                                <li className="features__item">
                                    <div className="features__img-wrap">
                                        <img src={feature_05} alt="" />
                                    </div>
                                    <div className="features__inner">
                                        <Translate className="features__subtitle" component="span" content="landing.features.title_05" />
                                        <Translate className="features__text" component="p" content="landing.features.text_05" />
                                    </div>
                                </li>
                                <li className="features__item features__item--reverse">
                                    <div className="features__img-wrap">
                                        <img src={feature_06} alt="" />
                                    </div>
                                    <div className="features__inner">
                                        <Translate className="features__subtitle" component="span" content="landing.features.title_06" />
                                        <Translate className="features__text" component="p" content="landing.features.text_06" />
                                    </div>
                                </li>
                            </ul>

                            <div className="features__footer">
                                <Translate className="features__footer-text" component="p" content="landing.features.footer" />
                                {this.state.currentAccount ?
                                    <span onClick={this.loginForm.bind(this)} className="start-header__btn">
                                        <Translate component="span" content="landing.login" />
                                    </span>
                                    :
                                    <Link to={`/registration`} className="start-header__btn">
                                        <Translate component="span" content="landing.registration" />
                                    </Link>
                                }
                            </div>
                        </div>
                    </section>
                )
                break;
            case "education":
                return (
                    <section className="aducation" ref="aducationBlock">
                        <div className="start__container">
                            <Translate className="aducation__title" component="p" content="landing.aducation.title" />
                            <Translate className="aducation__intro" component="p" content="landing.aducation.intro" />
                            <div className="aducation__inner">
                                <div className="aducation__left">
                                    <Translate className="aducation__description" component="p" content="landing.aducation.description" />
                                    <Translate className="aducation__text" component="p" content="landing.aducation.text" />
                                    <a href="https://ntz.team/events" target="_blank"><Translate component="span" content="landing.aducation.events" /></a>
                                </div>

                                <div className="aducation__right">
                                    <ul className="aducation__list">
                                        <li className="aducation__item">
                                            <img src={sphere_01} alt="" />
                                        </li>
                                        <li className="aducation__item aducation__item--odd">
                                            <img src={sphere_02} alt="" />
                                        </li>
                                        <li className="aducation__item">
                                            <img src={sphere_03} alt="" />
                                        </li>
                                        <li className="aducation__item aducation__item--odd">
                                            <img src={sphere_04} alt="" />
                                        </li>
                                        <li className="aducation__item">
                                            <img src={sphere_05} alt="" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {this.state.referralAccount ?
                                <a href={`https://life-balance.ntz.team/?r=${this.state.referralAccount}`} className="start-header__btn">
                                    <Translate component="span" content="landing.aducation.btn" />
                                </a>
                                :
                                <a href="https://life-balance.ntz.team" className="start-header__btn">
                                    <Translate component="span" content="landing.aducation.btn" />
                                </a>
                            }


                            <Translate unsafe className="aducation__info" component="p" content="landing.aducation.info" />
                        </div>
                    </section>
                )
                break;
            case "goods":
                return (
                    <section className="partners" ref="partnersBlock">
                        <div className="start__container">
                            <div className="partners__header">
                                <Translate className="partners__title" component="p" content="landing.partners.title" />
                                <Translate className="partners__description" component="p" content="landing.partners.description1" />
                                <Translate className="partners__description" component="p" content="landing.partners.description2" />
                                <Translate className="partners__description" component="p" content="landing.partners.description3" />
                                <Translate className="partners__description" component="p" content="landing.partners.description4" />
                            </div>
                            <ul className="partners__list">
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={merchant_04} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Nollam Lab</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.merchant_04" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={dariLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Dari</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.dari" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={limecosmeticsLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Lime cosmetics</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.lime" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={emperioLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Emperio</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.emperio" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={ecoidealLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Ecoideal</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.ecoideal" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={ntzLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">NTZ</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.ntz" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={festivesunriseLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Festive Sunrise</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.sunrise" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={lamaraLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Lamara</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.lamara" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={rekrevitLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Rekrevit</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.rekrevit" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={sollLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Соллл</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.soll" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={haznaLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Hazna</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.hazna" />
                                    </div>
                                </li>                         
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={lavazzaLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Lavazza</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.lavazza" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={kerasysLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Kerasys</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.kerasys" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={eramineralsLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Era Minerals</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.era" />
                                    </div>
                                </li>                               
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={intechLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Intech</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.intech" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={stressreliefLogo} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Stressrelief</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.stressrelief" />
                                    </div>
                                </li>



                                {/* <li className="partners__item">
                                    <img className="partners__img-logo" src={merchant_01} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Tooligram</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.merchant_01" />
                                    </div>
                                </li>
                                <li className="partners__item">
                                    <img className="partners__img-logo" src={merchant_03} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Eco-Ideal</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.merchant_03" />
                                    </div>
                                </li>

                                <li className="partners__item">
                                    <img className="partners__img-logo" src={merchant_05} alt="" />
                                    <div className="partners__item-inner">
                                        <span className="partners__name">Empireo</span>
                                        <Translate className="partners__item-text" component="span" content="landing.partners.merchant_05" />
                                    </div>
                                </li> */}
                            </ul>

                            <div className="partners__inner">
                                <div className="partners__left">
                                    <Translate className="partners__title partners__title--left" component="span" content="landing.partners.reg-title" />

                                    {this.state.currentAccount ?
                                        <span onClick={this.loginForm.bind(this)} className="start-header__btn">
                                            <Translate component="span" content="landing.login" />
                                        </span>
                                        :
                                        <Link to={`/registration`} className="start-header__btn">
                                            <Translate component="span" content="landing.registration" />
                                        </Link>
                                    }
                                </div>
                                <div className="partners__right">
                                    <Translate className="partners__reg-text partners__reg-text--top" component="span" content="landing.partners.reg-text_01" />
                                    <Translate className="partners__reg-text" component="span" content="landing.partners.reg-text_02" />
                                    <a className="partners__email" href="mailto: submit@ntz.team ">submit@ntz.team </a>
                                </div>
                            </div>
                        </div>
                    </section>
                )
                break;
            case "technology":
                return (
                    <section className="techno" ref="technoBlock">
                        <div className="start__container">
                            <Translate className="techno__title" component="p" content="landing.techno.title" />
                            <ul className="techno__list">
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_01} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_01" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_02} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_02" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_03} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_03" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_04} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_04" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_05} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_05" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_06} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_06" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_07} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_07" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_08} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_08" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_09} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_09" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_10} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_10" />
                                </li>
                                <li className="techno__item">
                                    <img className="techno__img" src={tech_11} alt="" />
                                    <Translate className="techno__text" component="span" content="landing.techno.text_11" />
                                </li>
                            </ul>
                        </div>
                        <div className="techno__join-wrap">
                            {this.state.currentAccount ?
                                <span >
                                    <Translate className="techno__join" component="p" content="landing.techno.join" />
                                </span>
                                :
                                <Link to={`/registration`}>
                                    <Translate className="techno__join" component="p" content="landing.techno.join" />
                                </Link>
                            }
                        </div>
                    </section>
                )
                break;
            default:
                return (
                    <section className="start-about" ref="aboutBlock">
                        <div className="start__container">
                            <div className="start-about__inner">
                                <Translate className="start-about__text" component="p" content="landing.about-block.text_01" />
                                <Translate className="start-about__text start-about__text--mission" component="p" content="landing.about-block.mission" />
                                <Translate className="start-about__text start-about__text--highlighted" component="p" content="landing.about-block.text_02" />

                            </div>
                            {this.state.currentAccount ?
                                <span onClick={this.loginForm.bind(this)} className="start-header__btn start-header__btn--about">
                                    <Translate component="span" content="landing.login" />
                                </span>
                                :
                                <Link to={`/registration`} className="start-header__btn start-header__btn--about">
                                    <Translate component="span" content="landing.registration" />
                                </Link>
                            }
                        </div>
                    </section>
                )
        }
    }

    render() {
        return (
            <div>
                {this.renderAbout()}
            </div>


        )
    }
}

export default About;

