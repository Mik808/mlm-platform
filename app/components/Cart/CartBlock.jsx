import React from 'react';
import { Link } from "react-router/es";
import { connect } from "alt-react";
import CartStore from "../../stores/CartStore";
import Translate from "react-translate-component";

class CartBlock extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

        }

        this.totalProductsQuantity = this.totalProductsQuantity.bind(this);
    }

    totalProductsQuantity(cartLegnth, cartItems) {
        if (cartLegnth === 0) {
            return 0;
        }
        let totalQuantity = 0;

        cartItems.forEach(product => {
            totalQuantity += product.quantity;
            return totalQuantity;
        })

        return totalQuantity;
    }

    render() {
        // console.log("CartBlock props", this.props);
        // console.log("this.props.cartItems.quantity", this.props.cartItems.quantity);
        // console.log("this.props.cartItems.quantitySum", this.props.cartItems.quantitySum);

        return (
            <Link to={`/cart`} className="cart-block">
                <div className="cart-block__quantity">
                    {this.props.cartItems && <span>{this.totalProductsQuantity(this.props.cartItems.length, this.props.cartItems)}</span>}
                </div>
                <div>
                    <Translate component="div" content="cart.cart" className="cart-block__cart-text" />
                    {this.props.cart && <div className="cart-block__sum">{`${this.props.cart.productsTotal}`} <span className="rub-sign">&#8381;</span></div>}
                </div>
            </Link>
        )
    }
}


CartBlock = connect(CartBlock, {
    listenTo() {
        return [CartStore];
    },
    getProps() {
        return CartStore.getState();
    }
})

export default CartBlock