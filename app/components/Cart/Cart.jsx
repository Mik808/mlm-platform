import React from 'react';
import CartStore from "../../stores/CartStore";
import { connect } from "alt-react";
import Translate from "react-translate-component";
import RemoveFromCartBtn from "../Buttons/RemoveFromCartBtn"
import CartActions from "../../actions/CartActions";
import { Link } from "react-router/es";
import AboutMenu from "../IndexMain/AboutMenu";
import ControlBar from "../ControlBar/ControlBar";
import Footer from "../Footer/Footer";

class Cart extends React.Component {

    constructor(props) {
        super(props);

        this.tax = 10;

        this.state = {

        }

        this.incrementItems = this.incrementItems.bind(this);
        this.decrementItems = this.decrementItems.bind(this);
        this.totalSum = this.totalSum.bind(this);
        this.calculateTax = this.calculateTax.bind(this);
        this.totalCart = this.totalCart.bind(this);
    }

    incrementItems(id) {
        CartActions.addQuantity(id);
        // CartActions.totalWithTax();
        CartActions.updateTaxDeliveryTotal();
    }

    decrementItems(id) {
        CartActions.removeQuantity(id);
        // CartActions.totalWithTax();
        CartActions.updateTaxDeliveryTotal();
    }

    totalSum() {
        let sum = this.props.cartItems.map(product => product.quantitySum).reduce((a, b) => a + b, 0);
        return Math.round(sum * 100) / 100;
    }

    // remove
    calculateTax() {
        let sum = this.props.cartItems.map(product => product.quantitySum).reduce((a, b) => a + b, 0);
        let percents = sum / 100 * this.tax;
        return Math.round(percents * 100) / 100;
    }

    // remove
    totalCart() {
        let sum = this.props.cartItems.map(product => product.quantitySum).reduce((a, b) => a + b, 0);
        return Math.round(sum * 100) / 100;
    }

    render() {
        // console.log("cart props", this.props); // this.props.cartItems - empty array
        return (
            <div>
                <AboutMenu />
                <div className="ntz-container">
                    <ControlBar routes={this.props.router} />
                </div>
                <div className="cart ntz-container">
                    <div className="cart__table">
                        <div className="cart__table-header cart__table-row">
                            <Translate component="div" content="cart.cart_products" className="cart__table-cell cart__product-header" />
                            <Translate component="div" content="cart.price" className="cart__table-cell cart__price-header" />
                            <Translate component="div" content="cart.quantity" className="cart__table-cell cart__quantity-header" />
                            <Translate component="div" content="cart.total" className="cart__table-cell cart__total-header" />
                        </div>
                        {this.props.cartItems.map((product) => {
                            return (
                                <div className="cart__table-row" key={product.id}>
                                    <div className="cart__table-cell cart__product">
                                        <div className="cart__img-wrapper"><img className="cart__img" src={`https://${product.image_full_url}`} alt="img" /> </div>
                                    </div>
                                    <div className="cart__table-cell cart__prod-name">{product.name}</div>
                                    <div className="cart__table-cell cart__price"><span>{product.price}</span><span className="rub-sign"> &#8381;</span></div>
                                    <div className="cart__table-cell cart__quantity-wrapper">
                                        <div className="cart__quantity">
                                            <div className="cart__quantity-digit">{product.quantity}</div>
                                            <button className="cart__plus" onClick={() => this.incrementItems(product.id)}>+</button>
                                            <button className="cart__minus" onClick={() => this.decrementItems(product.id)}>&mdash;</button>
                                        </div>
                                    </div>
                                    <div className="cart__table-cell cart__sum">{`${product.quantitySum}`}<span className="rub-sign">&#8381;</span></div>
                                    <div className="cart__table-cell cart__remove-wrapper">
                                        <RemoveFromCartBtn product={product} cssClass="cart__remove-product" />
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    <div className="cart__bill">
                        <Translate component="div" content="cart.order_sum" className="cart__bill-title" />

                        <div className="cart__bill-subtotal">
                            <div>
                                <Translate component="div" content="checkout.total_sum" />
                                <div className="cart__bill-value">{`${this.totalSum()}`} <span className="rub-sign">&#8381;</span></div>
                            </div>
                            <div>
                                <Translate component="div" content="checkout.delivery" />
                                <div></div>
                            </div>
                            <div>
                                <Translate component="div" content="checkout.tax" />
                                <div className="cart__bill-value">{`${this.calculateTax()}`} <span className="rub-sign">&#8381;</span></div>
                            </div>
                        </div>

                        <div className="cart__bill-total">
                            <div className="cart__total-wrapper">
                                <Translate component="div" content="checkout.total" />
                                <div className="cart__total-tax">{`${this.totalCart()}`} <span className="rub-sign">&#8381;</span></div>
                            </div>
                            <div className="cart__promo-wrapper">
                                <input type="checkbox" />
                                <Translate component="div" content="checkout.promo_code" />
                            </div>
                        </div>

                        <Link to={`/checkout`}>
                            <Translate component="button" className="ntz-btn cart__buy-btn" content="cart.go_to_checkout" />
                        </Link>

                        {/* <div className="cart__bill-footer">
                            <Translate component="div" className="cart__refund" content="cart.refund" />
                            <Translate component="div" className="cart__safe" content="cart.safe" />
                            <Translate component="div" className="cart__phone" content="cart.phone" />
                        </div> */}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

Cart = connect(Cart, {
    listenTo() {
        return [CartStore];
    },
    getProps() {
        return CartStore.getState();
    }
})

export default Cart;