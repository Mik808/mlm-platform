import React from 'react';
import CartActions from "../../actions/CartActions";
import CartStore from "../../stores/CartStore";
import { connect } from "alt-react";

class CartQuantity extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            quantity: 1,
        }        
        
        this.incrementItems = this.incrementItems.bind(this);
        this.decrementItems = this.decrementItems.bind(this);
        this.getQuantity = this.getQuantity.bind(this);
    }

    incrementItems(id, product) {
        CartActions.addProduct(product); 
        CartActions.updateTaxDeliveryTotal();

        CartActions.addQuantity(id);
        CartActions.updateTaxDeliveryTotal();
    }

    decrementItems(id) {
        CartActions.removeQuantity(id);
        CartActions.updateTaxDeliveryTotal();
    }

    getQuantity(id) {
        let quantity = 1;
        // console.log("this.props.cartItems", this.props.cartItems);
        this.props.cartItems.find(product => {
            // console.log("getQuantity", id);
            if (product.id === id) {
                // console.log("getQuantity inside if,", product.quantity);
                quantity = product.quantity;
            }
        })
        return <div className="cart__quantity-digit">{quantity}</div>
    }

    render() {
        return (
            <div className="cart__quantity">
                {this.getQuantity(this.props.id)}
                <button className="cart__plus" onClick={() => this.incrementItems(this.props.id, this.props.product)}>+</button>
                <button className="cart__minus" onClick={() => this.decrementItems(this.props.id)}>&mdash;</button>
            </div>
        )
    }
}

CartQuantity = connect(CartQuantity, {
    listenTo() {
        return [CartStore];
    },
    getProps() {
        return CartStore.getState();
    }
})

export default CartQuantity;