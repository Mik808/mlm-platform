import React from 'react';
import CartQuantity from "../Cart/CartQuantity";
import AddToCartBtn from "../Buttons/AddToCartBtn";
import Translate from "react-translate-component";
// import { connect } from "alt-react";
// import CartStore from "../../stores/CartStore";
import AboutMenu from "../IndexMain/AboutMenu";
import ControlBar from "../ControlBar/ControlBar";
import Footer from "../Footer/Footer";
import AccountStore from "stores/AccountStore";
import counterpart from "counterpart";

class ProductPage extends React.Component {

    constructor(props) {
        super(props);

        this.productUrl = "https://ntz.team/search_items?item_id=";

        this.state = {
            product: null,
            currentAccount: AccountStore.getState().currentAccount,
            linkCopied: false
        }

        this.showDescription = this.showDescription.bind(this);
    }

    componentDidMount() {
        fetch(`${this.productUrl}${this.props.params.id}`)
            .then(response => response.json())
            .then(result => {
                // console.log("product fetched", JSON.stringify(result));
                this.setState({ product: result });
            })
            .catch(error => console.log("product error", error));
    }

    showDescription(description) {
        if (description.search(/\r\n\r\n/g) > -1) {
            let paragraphs = description.split(/(?=\r\n\r\n)/g);

            return paragraphs.map((paragraph, i) => {
                // console.log("paragraph", paragraph);
                return (
                    <div className="product-page__paragraph" key={i}>
                        {paragraph.split(/(?=\r\n)/g).map((line, i) => {
                            // console.log("line", line);
                            return (<div className="product-page__line" key={i}>{line}</div>)
                        })}
                    </div>
                )
            })
        } else {
            let paragraphs = description.split(/(?=\r\n)/g);
            return paragraphs.map((line, i) => {
                return (<div className="product-page__line" key={i}>{line}</div>)
            })
        }
    }

    copyClick() {
        clearTimeout(copiedTimeout);
        navigator.clipboard.writeText( `${window.location.href.split('?')[0]}?r=${this.state.currentAccount}` );
        this.setState({linkCopied: true});
        const copiedTimeout = setTimeout(() => this.setState({linkCopied: false}), 2000);
    }

    render() {
        return (
            <div>
                <AboutMenu />
                <div className="ntz-container">
                    <ControlBar routes={this.props.router} />
                    {this.state.product ? (
                        <div className="product-page">
                            <div className="product-page__top">
                                <div className="product-page__img-wrapper">
                                    <img className="product-page__img" src={`https://${this.state.product.image_full_url}`} alt="img" />
                                </div>
                                <div className="product-page__about">
                                    <div className="product-page__name">{this.state.product.name}</div>
                                    <div className="product-page__description">{this.showDescription(this.state.product.description)}</div>
                                    <div className="product-page__prices">
                                        <div className="product-page__price">{`${this.state.product.price}`} <span className="rub-sign">&#8381;</span></div>
                                        {this.state.product.old_price && <div className="product-page__old-price">{`${this.state.product.old_price}`} <span className="rub-sign">&#8381;</span></div>}
                                        {this.state.product.ntz_price && <div className="product-page__mkt-price">{`Mkt ${this.state.product.ntz_price}`} <span className="rub-sign">&#8381;</span></div>}
                                    </div>
                                    <div className="product-page__controls">
                                        <CartQuantity id={this.state.product.id} product={this.state.product} />
                                        <AddToCartBtn product={this.state.product} class="ntz-cart-btn ntz-cart-btn--product" />
                                    </div>
                                </div>
                            </div>
                            <div className="product-page__bottom">
                                <div className="product-page__tab-bar">
                                    <Translate component="div" content="product.details" className="product-page__tab-title" />
                                </div>
                                <div className="product-page__details-wrapper">
                                    {this.state.currentAccount &&
                                        <div className="product-page__ref-wrapper">
                                            <div className="product-page__ref-title"><Translate content="account.ref-link" /><span>:</span></div>
                                            <div className="product-page__ref">{`${window.location.href.split('?')[0]}?r=${this.state.currentAccount}`}</div>
                                            <button className="product-page__ref-btn" onClick={() => this.copyClick()}>
                                                <span>{counterpart.translate( this.state.linkCopied ? "account.copied" : "account.ref-copy")}</span>
                                            </button>
                                        </div>
                                    }
                                    <div className="product-page__details">
                                        {this.showDescription(this.state.product.description2)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : null}
                </div>
                <Footer />
            </div>
        )
    }
}

// ProductPage = connect(ProductPage, {
//     listenTo() {
//         return [CartStore];
//     },
//     getProps() {
//         return CartStore.getState();
//     }
// })

export default ProductPage;