import React from "react";
import Translate from "react-translate-component";


class IndexFooter extends React.Component {
    render() {
        return (
            <div className="landing-footer">
                <div className="landing-footer__inner">
                    <div className="landing-footer__contacts">
                        <a className="landing-footer__text" href="tel:+79039610952">+7&nbsp;(903)&nbsp;961-09-52</a>
                        <a className="landing-footer__text" href="mailto:myecoideal@gmail.com ">myecoideal@gmail.com </a>
                    </div>
                    <ul className="landing-footer__list">
                        {/* <li className="landing-footer__item">
                            <a className="landing-footer__link" href="docs/policy.pdf" download>
                                <Translate content="landing.license_01" component="span" />
                            </a>
                        </li> */}
                        <li className="landing-footer__item">
                            <a className="landing-footer__link" href="docs/policy.pdf" download>
                                <Translate content="landing.license_02" component="span" />
                            </a>
                        </li>
                        <li className="landing-footer__item">
                            <a className="landing-footer__link" href="https://beta.companieshouse.gov.uk/company/11309904" target="_blank">&copy; Netizens Group Limited</a>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default IndexFooter;