import React from "react";
import { connect } from "alt-react";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import Translate from "react-translate-component";
import { ChainStore, FetchChain, key } from "bitsharesjs/es";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountSelector from "../Account/AccountSelector";
import { Link } from "react-router/es";
import SettingsStore from "stores/SettingsStore";
import IntlActions from "actions/IntlActions";
import counterpart from "counterpart";
import ReactDOM from "react-dom";
import Search from "../Search/Search"

var reg_img = require("assets/app_img/reg.png");
var hidePass = require("assets/app_img/show-pass.png");
var showPass = require("assets/app_img/hide-pass.png");
var lang = require("assets/app_img/lang-icon.png");

var feature_01 = require("assets/app_img/landing/feature_01.png");
var feature_02 = require("assets/app_img/landing/feature_02.png");
var feature_03 = require("assets/app_img/landing/feature_03.png");
var feature_04 = require("assets/app_img/landing/feature_04.png");
var feature_05 = require("assets/app_img/landing/feature_05.png");
var feature_06 = require("assets/app_img/landing/feature_06.png");

var sphere_01 = require("assets/app_img/landing/sphere_01.png");
var sphere_02 = require("assets/app_img/landing/sphere_02.png");
var sphere_03 = require("assets/app_img/landing/sphere_03.png");
var sphere_04 = require("assets/app_img/landing/sphere_04.png");
var sphere_05 = require("assets/app_img/landing/sphere_05.png");

var merchant_01 = require("assets/app_img/landing/merchant_01.png");
var merchant_02 = require("assets/app_img/merchants/neoron.png");
var merchant_03 = require("assets/app_img/landing/merchant_03.png");
var merchant_04 = require("assets/app_img/landing/merchant_04.png");
var merchant_05 = require("assets/app_img/merchants/empireo.png");

var tech_01 = require("assets/app_img/landing/tech_01.png");
var tech_02 = require("assets/app_img/landing/tech_02.png");
var tech_03 = require("assets/app_img/landing/tech_03.png");
var tech_04 = require("assets/app_img/landing/tech_04.png");
var tech_05 = require("assets/app_img/landing/tech_05.png");
var tech_06 = require("assets/app_img/landing/tech_06.png");
var tech_07 = require("assets/app_img/landing/tech_07.png");
var tech_08 = require("assets/app_img/landing/tech_08.png");
var tech_09 = require("assets/app_img/landing/tech_09.png");
var tech_10 = require("assets/app_img/landing/tech_10.png");
var tech_11 = require("assets/app_img/landing/tech_11.png");



class Index extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super();
        this.state = {
            locales: SettingsStore.getState().defaults.locale,
            currentLocale: SettingsStore.getState().settings.get("locale"),
            events_list: [],
            referralAccount: "",
            currentAccount: AccountStore.getState().currentAccount
        };

        this.handleLanguageSelect = this.handleLanguageSelect.bind(this);
        this.pageScrollTo = this.pageScrollTo.bind(this);
    }

    componentWillMount() {
        if (!WalletDb.getWallet()) {
            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: true
            });
        }
    }

    componentDidMount() {
        if (AccountStore.getState().referralAccount) {
            this.setState({
                referralAccount: AccountStore.getState().referralAccount
            })
        }
        ReactTooltip.rebuild();

        const xhr = new XMLHttpRequest();
        xhr.responseType = 'json';

        xhr.open('GET', 'https://eapi.ntz.team/', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send();

        xhr.onreadystatechange = () => {
            if (xhr.status == 200) {
                this.setState({
                    events_list: xhr.response
                })
            }
            else {
                alert("Error");
            }
        }
    }

    handleLanguageSelect(e) {
        IntlActions.switchLocale(e.target.value);
        SettingsActions.changeSetting({ setting: "locale", value: e.target.value });
        this.setState({
            currentLocale: e.target.value
        })
    }

    pageScrollTo(scrolID) {
        switch (scrolID) {
            case 'aboutBlock':
                var aboutBlock = ReactDOM.findDOMNode(this.refs.aboutBlock)
                aboutBlock.scrollIntoView();

                break

            case 'featuresBlock':
                var featuresBlock = ReactDOM.findDOMNode(this.refs.featuresBlock)
                featuresBlock.scrollIntoView();

                break

            case 'aducationBlock':
                var aducationBlock = ReactDOM.findDOMNode(this.refs.aducationBlock)
                aducationBlock.scrollIntoView();

                break

            case 'partnersBlock':
                var partnersBlock = ReactDOM.findDOMNode(this.refs.partnersBlock)
                partnersBlock.scrollIntoView();

                break

            case 'technoBlock':
                var technoBlock = ReactDOM.findDOMNode(this.refs.technoBlock)
                technoBlock.scrollIntoView();

                break

            default:
                var headerBlock = ReactDOM.findDOMNode(this.refs.headerBlock)
                headerBlock.scrollIntoView();

                break
        }
    }

    loginForm() {
        WalletUnlockActions.unlock().then(() => {
            AccountActions.tryToSetCurrentAccount();

            let currentAccount = AccountStore.getState().currentAccount;

            if (window.innerWidth < 600) {
                this.context.router.push(`mobile/account/${currentAccount}/dashboard`);
            }
            else
                this.context.router.push(`account/${currentAccount}/dashboard`);
        });
    }



    render() {
        let referralAccount = this.state.referralAccount;
        let currentAccount = this.state.currentAccount;
        let events_list = this.state.events_list;

        return (
            <div className="start">
                <header className="start-header" ref="headerBlock">
                    {/* <Search /> */}

                    <nav className="start-header__wrap">
                        <div className="start__container">
                            {window.innerWidth > 700 ?
                                <ul className="start-header__list">
                                    <li className="start-header__item" onClick={this.pageScrollTo.bind(this, "aboutBlock")}>
                                        <Translate className="start-header__text" component="span" content="landing.header.item_01" />
                                    </li>
                                    <li className="start-header__item" onClick={this.pageScrollTo.bind(this, "featuresBlock")} >
                                        <Translate className="start-header__text" component="span" content="landing.header.item_02" />
                                    </li>
                                    <li className="start-header__item" onClick={this.pageScrollTo.bind(this, "aducationBlock")}>
                                        <Translate className="start-header__text" component="span" content="landing.header.item_03" />
                                    </li>
                                    <li className="start-header__item start-header__item--logo">
                                    </li>
                                    <li className="start-header__item" onClick={this.pageScrollTo.bind(this, "partnersBlock")}>
                                        <Translate className="start-header__text" component="span" content="landing.header.item_04" />
                                    </li>
                                    <li className="start-header__item" onClick={this.pageScrollTo.bind(this, "technoBlock")}>
                                        <Translate className="start-header__text" component="span" content="landing.header.item_05" />
                                    </li>
                                    <li className="start-header__item">
                                        <div className="landing-lang__wrap">
                                            <img className="landing-lang__img" src={lang} />
                                            <select
                                                className="landing-lang__selector"
                                                onChange={this.handleLanguageSelect}
                                                value={this.state.currentLocale}>
                                                {this.state.locales.map(locale => (
                                                    <option
                                                        key={locale}
                                                        value={locale}
                                                    >
                                                        {counterpart.translate("languages." + locale)}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                    </li>
                                </ul>
                                :
                                <div className="start-mobile__wrap">
                                    <img className="start-mobile__img" src={reg_img} />
                                    <div className="landing-lang__wrap">
                                        <img className="landing-lang__img" src={lang} />
                                        <select
                                            className="landing-lang__selector"
                                            onChange={this.handleLanguageSelect}
                                            value={this.state.currentLocale}>
                                            {this.state.locales.map(locale => (
                                                <option
                                                    key={locale}
                                                    value={locale}
                                                >
                                                    {counterpart.translate("languages." + locale)}
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                            }
                        </div>
                    </nav>

                    {window.innerWidth > 700 ?
                        <div className="start-header__img-wrap">
                            <img className="start-header__img" src={reg_img} />
                        </div>
                    : null}

                    <div className="start__container">
                        <div className="start-header__container">
                            <div className="start-header__inner">
                                <Translate className="start-header__intro" component="span" content="landing.header.intro" />

                                <h1 className="start-header__title">NTZ</h1>

                                <Translate className="start-header__description" component="p" content="landing.header.description" />

                                {currentAccount ?
                                    <span onClick={this.loginForm.bind(this)} className="start-header__btn">
                                        <Translate component="span" content="landing.login" />
                                    </span>
                                    :
                                    <Link to={`/registration`} className="start-header__btn">
                                        <Translate component="span" content="landing.registration" />
                                    </Link>
                                }
                            </div>
                        </div>
                    </div>
                </header>

                <section className="start-about" ref="aboutBlock">
                    <div className="start__container">
                        <div className="start-about__inner">
                            <Translate className="start-about__text" component="p" content="landing.about-block.text_01" />
                            <Translate className="start-about__text start-about__text--mission" component="p" content="landing.about-block.mission" />
                            <Translate className="start-about__text start-about__text--highlighted" component="p" content="landing.about-block.text_02" />

                        </div>
                        {currentAccount ?
                            <span onClick={this.loginForm.bind(this)} className="start-header__btn start-header__btn--about">
                                <Translate component="span" content="landing.login" />
                            </span>
                            :
                            <Link to={`/registration`} className="start-header__btn start-header__btn--about">
                                <Translate component="span" content="landing.registration" />
                            </Link>
                        }
                    </div>
                </section>

                <section className="features" ref="featuresBlock">
                    <div className="start__container">
                        <Translate className="features__title" component="p" content="landing.features.title" />
                        <Translate className="features__intro" component="p" content="landing.features.intro" />
                        <Translate className="features__description" component="p" content="landing.features.description" />

                        <ul className="features__list">
                            <li className="features__item">
                                <div className="features__img-wrap">
                                    <img src={feature_01} alt="" />
                                </div>
                                <div className="features__inner">
                                    <Translate className="features__subtitle" component="span" content="landing.features.title_01" />
                                    <Translate className="features__text" component="p" content="landing.features.text_01" />
                                </div>
                            </li>
                            <li className="features__item features__item--reverse">
                                <div className="features__img-wrap">
                                    <img src={feature_02} alt="" />
                                </div>
                                <div className="features__inner">
                                    <Translate className="features__subtitle" component="span" content="landing.features.title_02" />
                                    <Translate className="features__text" component="p" content="landing.features.text_02" />
                                </div>
                            </li>
                            <li className="features__item">
                                <div className="features__img-wrap">
                                    <img src={feature_03} alt="" />
                                </div>
                                <div className="features__inner">
                                    <Translate className="features__subtitle" component="span" content="landing.features.title_03" />
                                    <Translate className="features__text" component="p" content="landing.features.text_03" />
                                </div>
                            </li>
                            <li className="features__item features__item--reverse">
                                <div className="features__img-wrap">
                                    <img src={feature_04} alt="" />
                                </div>
                                <div className="features__inner">
                                    <Translate className="features__subtitle" component="span" content="landing.features.title_04" />
                                    <Translate className="features__text" component="p" content="landing.features.text_04" />
                                </div>
                            </li>
                            <li className="features__item">
                                <div className="features__img-wrap">
                                    <img src={feature_05} alt="" />
                                </div>
                                <div className="features__inner">
                                    <Translate className="features__subtitle" component="span" content="landing.features.title_05" />
                                    <Translate className="features__text" component="p" content="landing.features.text_05" />
                                </div>
                            </li>
                            <li className="features__item features__item--reverse">
                                <div className="features__img-wrap">
                                    <img src={feature_06} alt="" />
                                </div>
                                <div className="features__inner">
                                    <Translate className="features__subtitle" component="span" content="landing.features.title_06" />
                                    <Translate className="features__text" component="p" content="landing.features.text_06" />
                                </div>
                            </li>
                        </ul>

                        <div className="features__footer">
                            <Translate className="features__footer-text" component="p" content="landing.features.footer" />
                            {currentAccount ?
                                <span onClick={this.loginForm.bind(this)} className="start-header__btn">
                                    <Translate component="span" content="landing.login" />
                                </span>
                                :
                                <Link to={`/registration`} className="start-header__btn">
                                    <Translate component="span" content="landing.registration" />
                                </Link>
                            }
                        </div>
                    </div>
                </section>

                <section className="aducation" ref="aducationBlock">
                    <div className="start__container">
                        <Translate className="aducation__title" component="p" content="landing.aducation.title" />
                        <Translate className="aducation__intro" component="p" content="landing.aducation.intro" />

                        <div className="aducation__inner">
                            <div className="aducation__left">
                                <Translate className="aducation__description" component="p" content="landing.aducation.description" />
                                <Translate className="aducation__text" component="p" content="landing.aducation.text" />
                            </div>

                            <div className="aducation__right">
                                <ul className="aducation__list">
                                    <li className="aducation__item">
                                        <img src={sphere_01} alt="" />
                                    </li>
                                    <li className="aducation__item aducation__item--odd">
                                        <img src={sphere_02} alt="" />
                                    </li>
                                    <li className="aducation__item">
                                        <img src={sphere_03} alt="" />
                                    </li>
                                    <li className="aducation__item aducation__item--odd">
                                        <img src={sphere_04} alt="" />
                                    </li>
                                    <li className="aducation__item">
                                        <img src={sphere_05} alt="" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {referralAccount ?
                            <a href={`https://life-balance.ntz.team/?r=${referralAccount}`} className="start-header__btn">
                                <Translate component="span" content="landing.aducation.btn" />
                            </a>
                            :
                            <a href="https://life-balance.ntz.team" className="start-header__btn">
                                <Translate component="span" content="landing.aducation.btn" />
                            </a>
                        }


                        <Translate unsafe className="aducation__info" component="p" content="landing.aducation.info" />
                    </div>
                </section>

                <section className="partners" ref="partnersBlock">
                    <div className="start__container">
                        <div className="partners__header">
                            <Translate className="partners__title" component="p" content="landing.partners.title" />
                            <Translate className="partners__description" component="p" content="landing.partners.description" />
                        </div>

                        <ul className="partners__list">
                            <li className="partners__item">
                                <img className="partners__img-logo" src={merchant_01} alt="" />
                                <div className="partners__item-inner">
                                    <span className="partners__name">Tooligram</span>
                                    <Translate className="partners__item-text" component="span" content="landing.partners.merchant_01" />
                                </div>
                            </li>
                           {/*  <li className="partners__item">
                                <img className="partners__img-logo" src={merchant_02} alt="" />
                                <div className="partners__item-inner">
                                    <span className="partners__name">Neoron</span>
                                    <Translate className="partners__item-text" component="span" content="landing.partners.merchant_02" />
                                </div>
                            </li> */}
                            <li className="partners__item">
                                <img className="partners__img-logo" src={merchant_03} alt="" />
                                <div className="partners__item-inner">
                                    <span className="partners__name">Eco-Ideal</span>
                                    <Translate className="partners__item-text" component="span" content="landing.partners.merchant_03" />
                                </div>
                            </li>
                            <li className="partners__item">
                                <img className="partners__img-logo" src={merchant_04} alt="" />
                                <div className="partners__item-inner">
                                    <span className="partners__name">Nollam Lab</span>
                                    <Translate className="partners__item-text" component="span" content="landing.partners.merchant_04" />
                                </div>
                            </li>
                            <li className="partners__item">
                                <img className="partners__img-logo" src={merchant_05} alt="" />
                                <div className="partners__item-inner">
                                    <span className="partners__name">Empireo</span>
                                    <Translate className="partners__item-text" component="span" content="landing.partners.merchant_05" />
                                </div>
                            </li>
                        </ul>

                        <div className="partners__inner">
                            <div className="partners__left">
                                <Translate className="partners__title partners__title--left" component="span" content="landing.partners.reg-title" />

                                {currentAccount ?
                                    <span onClick={this.loginForm.bind(this)} className="start-header__btn">
                                        <Translate component="span" content="landing.login" />
                                    </span>
                                    :
                                    <Link to={`/registration`} className="start-header__btn">
                                        <Translate component="span" content="landing.registration" />
                                    </Link>
                                }
                            </div>
                            <div className="partners__right">
                                <Translate className="partners__reg-text partners__reg-text--top" component="span" content="landing.partners.reg-text_01" />
                                <Translate className="partners__reg-text" component="span" content="landing.partners.reg-text_02" />
                                <a className="partners__email" href="mailto: submit@ntz.team ">submit@ntz.team </a>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="techno" ref="technoBlock">
                    <div className="start__container">
                        <Translate className="techno__title" component="p" content="landing.techno.title" />
                        <ul className="techno__list">
                            <li className="techno__item">
                                <img className="techno__img" src={tech_01} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_01" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_02} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_02" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_03} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_03" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_04} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_04" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_05} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_05" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_06} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_06" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_07} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_07" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_08} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_08" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_09} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_09" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_10} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_10" />
                            </li>
                            <li className="techno__item">
                                <img className="techno__img" src={tech_11} alt="" />
                                <Translate className="techno__text" component="span" content="landing.techno.text_11" />
                            </li>
                        </ul>

                    </div>
                    <div className="techno__join-wrap">
                        {currentAccount ?
                            <span >
                                <Translate className="techno__join" component="p" content="landing.techno.join" />
                            </span>
                            :
                            <Link to={`/registration`}>
                                <Translate className="techno__join" component="p" content="landing.techno.join" />
                            </Link>
                        }
                    </div>
                </section>


                {/*  EVENTS BLOCK */}

                {/*            
                    <Link to={`/registration`} className="start__btn">
                                    <Translate component="span" content="landing.registration" />
                                </Link>
                                
                                
                    {events_list && events_list.length > 0 ?
                        <div className="landing-events">
                            <div className="landing-content__layout-center">
                                <Translate className="landing-content__subtitle" component="h3" content="landing.events" />

                                {events_list.map(event =>
                                    <ul className="landing-events__list" key={event.event_id}>
                                        <li className="landing-events__item">
                                            <div className="landing-events__inner">
                                                <img className="landing-events__img" src={speaker} />
                                                <span className="landing-events__data">{event.event_speaker}</span>
                                            </div>

                                        </li>
                                        <li className="landing-events__item">
                                            <div className="landing-events__inner">
                                                <img className="landing-events__img" src={date} />
                                                <span className="landing-events__data">{event.event_date}</span>
                                            </div>
                                        </li>
                                        <li className="landing-events__item">
                                            <div className="landing-events__inner">
                                                <img className="landing-events__img" src={place} />
                                                <span className="landing-events__data">{event.event_address}</span>
                                                <p>{event.event_description}</p>
                                            </div>
                                        </li>
                                    </ul>)}
                            </div>
                        </div>
                        : null} */}


                <div className="landing-footer landing-footer--start">
                    <div className="landing-footer__inner">
                        <div className="landing-footer__logo-wrap">
                            <img src={reg_img} alt="" />
                            <span>NTZ</span>
                        </div>

                        <div>
                            <div className="landing-footer__contacts">
                                <a className="landing-footer__text" href="tel:+79039610952">+7&nbsp;(903)&nbsp;961-09-52</a>
                                <a className="landing-footer__text" href="mailto:myecoideal@gmail.com ">myecoideal@gmail.com </a>

                            </div>
                            <ul className="landing-footer__list">
                                <li className="landing-footer__item">
                                    <a className="landing-footer__link" href="https://beta.companieshouse.gov.uk/company/11309904" target="_blank">&copy; NTZ Group Limited</a>
                                </li>
                                <li className="landing-footer__item">
                                    <a className="landing-footer__link" href="docs/policy.pdf" download>
                                        <Translate content="landing.license_02" component="span" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default connect(Index, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {};
    }
});