import React from "react";
import { connect } from "alt-react";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import AccountNameInput from "./../Forms/AccountNameInput";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../LoadingIndicator";
import Translate from "react-translate-component";
import { ChainStore, FetchChain, key } from "bitsharesjs/es";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import { Link } from "react-router/es";
import AccountSelector from "../Account/AccountSelector";
import ls from "common/localStorage";


var reg_img = require("assets/app_img/reg.png");
var hidePass = require("assets/app_img/show-pass.png");
var showPass = require("assets/app_img/hide-pass.png");
let accountStorage = new ls("__graphene__");

class Registration extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super();
        this.state = {
            validAccountName: false,
            accountName: "",
            description: "",
            reg_name: "",
            reg_surname: "",
            reg_email: "",
            reg_phone: "",
            validPassword: false,
            registrar_account: null,
            regLoading: false,
            step: 1,
            userPassword: "",
            confirm_password: "",
            understand_1: false,
            validPasswordFormat: true,
            password_error: null,
            password_input_reset: Date.now(),
            account_name: '',
            account: null,
            referralAccount: '',
            user_name: '',
            passType: 'password'
        };


        this.onFinishConfirm = this.onFinishConfirm.bind(this);
        this.accountNameInput = null;
        this.showHidePass = this.showHidePass.bind(this);
    }

    componentWillMount() {
        var currentAccount = AccountStore.getState().currentAccount;
        if (currentAccount) {
            this.context.router.push("/");
        };
        if (!WalletDb.getWallet()) {
            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: true
            });
        }
    }

    componentDidMount() {
        ReactTooltip.rebuild();
        if (AccountStore.getState().referralAccount) {
            this.setState({
                referralAccount: AccountStore.getState().referralAccount
            })
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state);
    }

    showHidePass(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            passType: this.state.passType === 'input' ? 'password' : 'input'
        })
    }

    isValid() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
        if (!WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return valid && this.state.understand_1;
    }

    onAccountNameChange(e) {
        const state = {};
        if (e.valid !== undefined) state.validAccountName = e.valid;
        if (e.value !== undefined) state.accountName = e.value;
        this.setState(state);
    }

    onFinishConfirm(confirm_store_state) {
        if (confirm_store_state.included && confirm_store_state.broadcasted_transaction) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, { [this.state.accountName]: true }).then(() => {
                this.props.router.push("/wallet/backup/create?newAccount=true");
            });
        }
    }

    _unlockAccount(name, password) {
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: true
        });

        WalletDb.validatePassword(password, true, name);
        WalletUnlockActions.checkLock.defer();
    }

    createAccount(name, description, password, reg_name, reg_surname, reg_email, reg_phone) {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = accountStorage.get("referralAccount");
        let promoValue = AccountStore.getState().promoValue;
        this.setState({ regLoading: true });

        AccountActions.createAccountWithPassword(name, description, password, this.state.registrar_account, referralAccount || this.state.registrar_account, 0, refcode, promoValue, reg_name, reg_surname, reg_email, reg_phone).then(() => {

            AccountActions.setPasswordAccount(name);
            AccountActions.setCurrentAccount(name);
            // User registering his own account
            if (this.state.registrar_account) {
                FetchChain("getAccount", name, undefined, { [name]: true }).then(() => {
                    this.setState({
                        step: 2,
                        regLoading: false,
                    });
                    this._unlockAccount(name, password);
                });
                TransactionConfirmStore.listen(this.onFinishConfirm);
            } else { // Account registered by the faucet

                FetchChain("getAccount", name, undefined, { [name]: true }).then(() => {
                    this.setState({
                        step: 2,
                        user_name: name
                    });

                    this._unlockAccount(name, password);
                });
            }
        }).catch(error => {
            console.log("ERROR AccountActions.createAccount", error);
            let error_msg = error.base && error.base.length && error.base.length > 0 ? error.base[0] : "unknown error";
            if (error.remote_ip) error_msg = error.remote_ip[0];
            notify.addNotification({
                message: `Failed to create account: ${name} - ${error_msg}`,
                level: "error",
                autoDismiss: 10
            });
            this.setState({ regLoading: false });
        });
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.isValid()) return;
        let user_name = this.accountNameInput.getValue();
        let description = this.state.description;
        let password = this.state.userPassword;
        let reg_name = this.refs.reg_name.value;
        let reg_surname = this.refs.reg_surname.value;
        let reg_email = this.refs.reg_email.value;
        let reg_phone = this.refs.reg_phone.value;

        this.createAccount(user_name, description, password, reg_name, reg_surname, reg_email, reg_phone);
    }

    onPassEnter(e) {
        this.setState({ userPassword: e.target.value });

        var regexp = /^[a-zA-Z0-9]+$/i;
        var passValue = e.target.value;
        if (!regexp.test(passValue) || (e.target.value.length < 12)) {
            this.setState({
                validPasswordFormat: false
            })
        }
        else {
            this.setState({
                validPasswordFormat: true
            })
        }
    }

    _onInput(value, e) {
        this.setState({
            [value]: value === "confirm_password" ? e.target.value : !this.state[value],
            validPassword: value === "confirm_password" ? e.target.value === this.state.userPassword : this.state.validPassword
        });
    }

    accountChanged(account_name) {
        if (!account_name) this.setState({ account: null });
        this.setState({ account_name, error: null });
    }
    onAccountChanged(account) {
        this.setState({ account, error: null });
        accountStorage.set("referralAccount", account.get("name"));
    }

    loginForm() {
        WalletUnlockActions.unlock().then(() => {
            AccountActions.tryToSetCurrentAccount();

            let currentAccount = AccountStore.getState().currentAccount;

            if (window.innerWidth < 600) {
                this.context.router.push(`mobile/account/${currentAccount}/dashboard`);
            }
            else
                this.context.router.push(`account/${currentAccount}/dashboard`);
        });
    }

    _onNavigate(route, e) {
        e.preventDefault();
        this.context.router.push(route);
    }

    _renderAccountCreateForm() {

        let { registrar_account } = this.state;
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let registrar = registrar_account ? ChainStore.getAccount(registrar_account) : null;
        let { account_name, from_error } = this.state;
        if (registrar) {
            if (registrar.get("lifetime_referrer") == registrar.get("id")) {
                isLTM = true;
            }
        }
        let tabIndex = 1;

        this.state.regLoading;


        return (
            <div className="landing__wrap landing__reg-wrap">
                <div className="landing__logo-wrap">
                    <img className="landing__logo" src={reg_img} />
                    <p className="landing__title">NTZ</p>
                </div>

                <div className="landing-form__wrap landing-form__wrap--reg">
                    <div className="landing-form__reg-header">
                        <Translate className="landing-form__tip landing-form__tip--got-acc" component="span" content="landing.got-account" />
                        <span onClick={this.loginForm.bind(this)} className="landing-form__link" >
                            <Translate component="span" content="landing.authorization" />
                        </span>

                        <Link className="landing-form__link landing-form__link--index" onClick={this._onNavigate.bind(this, "/")}>
                            <Translate content="account.index" />
                        </Link>

                        <Translate className="landing-form__subtitle" component="p" content="landing.subtitle" />

                    </div>

                    <form
                        className="landing-form"
                        id="landingFormReg"
                        onSubmit={this.onSubmit.bind(this)}
                    >
                        <div className="landing-form__inner">
                            <label className="landing-form__group" htmlFor="landingFormEmail">
                                <Translate className="landing-form__tip landing-form__tip--required" component="span" content="landing.email-label" />
                                <input
                                    ref="reg_email"
                                    type="email"
                                    id="landingFormEmail" className="landing-form__field"
                                    required />
                            </label>

                            <label className="landing-form__group" htmlFor="landingFormPhone">
                                <Translate className="landing-form__tip landing-form__tip--required" component="span" content="landing.phone-label" />
                                <input
                                    ref="reg_phone"
                                    type="tel"
                                    id="landingFormPhone" className="landing-form__field"
                                    required />
                            </label>

                            <label className="landing-form__group" htmlFor="landingFormLogin">
                                <Translate className="landing-form__tip landing-form__tip--required" component="span" content="landing.login-reg-label" />
                                <AccountNameInput
                                    ref={(ref) => { if (ref) { this.accountNameInput = ref.refs.nameInput; } }}
                                    cheapNameOnly={!!firstAccount}
                                    onChange={this.onAccountNameChange.bind(this)}
                                    accountShouldNotExist={true}
                                />
                            </label>

                            <label className="landing-form__group" htmlFor="landingFormRefLogin">
                                <Translate className="landing-form__tip landing-form__tip--required" component="span" content="landing.referral" />
                                {this.state.referralAccount ?
                                    <AccountSelector
                                        ref="account_input"
                                        accountName={this.state.referralAccount}
                                        onChange={this.accountChanged.bind(this)}
                                        onAccountChanged={this.onAccountChanged.bind(this)}
                                        account={this.state.referralAccount}
                                        error={from_error}
                                        tabIndex={tabIndex++}
                                        id="landingFormRefLogin"
                                    />
                                    :
                                    <AccountSelector
                                        ref="account_input"
                                        accountName={account_name}
                                        onChange={this.accountChanged.bind(this)}
                                        onAccountChanged={this.onAccountChanged.bind(this)}
                                        account={account_name}
                                        error={from_error}
                                        tabIndex={tabIndex++}
                                        id="landingFormRefLogin"
                                    />
                                }
                            </label>

                            <label className="landing-form__group landing-form__field-wrap" htmlFor="landingFormPass">
                                <Translate className="landing-form__tip landing-form__tip--required" component="span" content="landing.pass-reg-label" />
                                <input
                                    ref="password_input"
                                    type={this.state.passType}
                                    id="landingFormPass" className="landing-form__field"
                                    onChange={this.onPassEnter.bind(this)}
                                    required
                                />
                                <img
                                    className="landing-form__pass-img"
                                    src={this.state.passType == 'password' ? showPass : hidePass}
                                    id="showPassBtn"
                                    onClick={this.showHidePass}
                                />

                                {this.state.confirm_password && this.state.confirm_password !== this.state.userPassword ?
                                    <div className="has-error reg-form__error"><Translate content="wallet.confirm_error" /></div> : null}
                                {!this.state.validPasswordFormat ?
                                    <div className="has-error reg-form__error"><Translate content="wallet.pass_invalid" /></div> : null}
                            </label>

                            <label className="landing-form__group" htmlFor="landingFormPassConfirm">
                                <Translate className="landing-form__tip landing-form__tip--required" component="span" content="landing.pass-confirm-label" />
                                <input
                                    type="password"
                                    name="password"
                                    id="landingFormPassConfirm" className="landing-form__field"
                                    value={this.state.confirm_password} onChange={this._onInput.bind(this, "confirm_password")}
                                    required
                                />
                            </label>

                            <label className="landing-form__group" htmlFor="landingFormName">
                                <Translate className="landing-form__tip" component="span" content="landing.name-label" />
                                <input
                                    ref="reg_name"
                                    type="text"
                                    id="landingFormName" className="landing-form__field"
                                />
                            </label>

                            <label className="landing-form__group" htmlFor="landingFormSurname">
                                <Translate className="landing-form__tip" component="span" content="landing.surname-label" />
                                <input
                                    ref="reg_surname"
                                    type="text"
                                    id="landingFormSurname" className="landing-form__field"
                                />
                            </label>

                            <div className="confirm-checks" onClick={this._onInput.bind(this, "understand_1")}>
                                <label className="landing-form__agreement-wrap">
                                    <input type="checkbox" onChange={() => { }} checked={this.state.understand_1} className="landing-form__tick" />
                                    <Translate content="landing.agreement" className="landing-form__tip" />
                                </label>
                            </div>
                        </div>

                        {/* Submit button */}
                        {this.state.regLoading ?
                            <LoadingIndicator type="three-bounce" /> :
                            <button className="landing-form__btn">
                                <Translate component="span" content="landing.login-btn" />
                            </button>}
                    </form>
                </div>
                <div className="landing-footer">
                    <div className="landing-footer__inner">
                        <div className="landing-footer__logo-wrap">
                            <img src={reg_img} alt="" />
                            <span>NTZ</span>
                        </div>

                        <div>
                            <div className="landing-footer__contacts">
                                <a className="landing-footer__text" href="tel:+79039610952">+7&nbsp;(903)&nbsp;961-09-52</a>
                                <a className="landing-footer__text" href="mailto:myecoideal@gmail.com ">myecoideal@gmail.com </a>

                            </div>
                            <ul className="landing-footer__list">
                                <li className="landing-footer__item">
                                    <a className="landing-footer__link" href="https://beta.companieshouse.gov.uk/company/11309904" target="_blank">&copy; NTZ Group Limited</a>
                                </li>
                                <li className="landing-footer__item">
                                    <a className="landing-footer__link" href="docs/policy.pdf" download>
                                        <Translate content="landing.license_02" component="span" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    _onAccountCreated() {
        let user_name = this.state.user_name;
        if (window.innerWidth < 600) {
            this.context.router.push(`mobile/account/${user_name}/dashboard`);
        }
        else {
            this.context.router.push(`account/${user_name}/dashboard`);
        }
    }

    render() {
        let { step } = this.state;

        return (
            <div>
                {step === 1 ? this._renderAccountCreateForm()
                    :
                    step === 2 ? this._onAccountCreated() :
                        this.context.router.push("/")
                }
            </div>
        );
    }
}

export default connect(Registration, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {};
    }
});