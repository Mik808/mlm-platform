import React from "react";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";

var userIcon = require("assets/app_img/user-icon.png");

class AccountInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        title: React.PropTypes.string,
        image_size: React.PropTypes.object.isRequired,
        my_account: React.PropTypes.bool,
    }

    static defaultProps = {
        title: null,
        image_size: { height: 40, width: 40 }
    }

    render() {
        let { account } = this.props;

        return (
            <div className="user-account__wrap">
                <div className="user-account__inner">
                    <div className="user-account__img">
                        <img src={userIcon} />
                    </div>

                    <span className="user-account__title">{account.get("name")}</span>
                </div>
            </div>
        );
    }
}

export default BindToChainState(AccountInfo);
