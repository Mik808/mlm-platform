import React from "react";
import {Link} from "react-router/es";
import Translate from "react-translate-component";
import {ChainStore} from "bitsharesjs/es";
import ChainTypes from "../Utility/ChainTypes";
import BindToChainState from "../Utility/BindToChainState";
import Statistics from "./Statistics";
import TimeAgo from "../Utility/TimeAgo";
import HelpContent from "../Utility/HelpContent";
import accountUtils from "common/account_utils";
import FormattedAsset from "../Utility/FormattedAsset";
import utils from "common/utils";
import WalletActions from "actions/WalletActions";
import {Apis} from "bitsharesjs-ws";




class FeeHelp extends React.Component {
    static propTypes = {
        dprops: ChainTypes.ChainObject.isRequired
    };
    static defaultProps = {
        dprops: "2.1.0"
    };

    render() {
        let {dprops} = this.props;

        return (
            <HelpContent
                {...this.props}
                path="components/AccountMembership"
                section="fee-division"
                nextMaintenanceTime={{time: dprops.get("next_maintenance_time")}}
            />
        );
    }
}
FeeHelp = BindToChainState(FeeHelp);


class VestingBalance extends React.Component {

    _onClaim(claimAll, e) {
        e.preventDefault();
        WalletActions.claimVestingBalance(this.props.account.id, this.props.vb, claimAll);
    }

    render() {
        let {vb} = this.props;
        if (!this.props.vb) {
            return null;
        }
        // let vb = ChainStore.getObject( this.props.vb );
        // if (!vb) {
        //     return null;
        // }

        let cvbAsset, vestingPeriod, earned, secondsPerDay = 60 * 60 * 24,
            availablePercent, balance;
        if (vb) {
            balance = vb.balance.amount;
            cvbAsset = ChainStore.getAsset(vb.balance.asset_id);
            earned = vb.policy[1].coin_seconds_earned;
            vestingPeriod = vb.policy[1].vesting_seconds;
            availablePercent = vestingPeriod === 0 ? 1 : earned / (vestingPeriod * balance);
        }

        if (!cvbAsset) {
            return null;
        }

        if (!balance) {
            return null;
        }

        return (
            <div style={{paddingBottom: "1rem"}}>
                <div>
                    <div className="grid-content no-padding">
                        <Translate component="h4" className="referral-membership__subtitle" content="account.vesting.balance_number" id={vb.id} />

                        <table className="table key-value-table">
                            <tbody>
                                <tr>
                                    <td><Translate content="account.member.cashback"/> </td>
                                    <td><FormattedAsset amount={vb.balance.amount} asset={vb.balance.asset_id} /></td>
                                </tr>
                                <tr>
                                    <td><Translate content="account.member.available" /></td>
                                    <td>{utils.format_number(availablePercent * 100, 2)}% / <FormattedAsset amount={availablePercent * vb.balance.amount} asset={cvbAsset.get("id")} /></td>
                                </tr>
                                <tr>
                                    <td colSpan="2" style={{textAlign: "right"}}>
                                        <button onClick={this._onClaim.bind(this, false)} className="referral-membership__withdrow-btn"><Translate content="account.member.claim" /></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        );
    }
}


class AccountMembership extends React.Component {

    constructor() {
        super();

        this.state = {
            vbs: null
        };
    }

    static propTypes = {
        account: ChainTypes.ChainAccount.isRequired,
        gprops: ChainTypes.ChainObject.isRequired,
        core_asset: ChainTypes.ChainAsset.isRequired
    };
    static defaultProps = {
        gprops: "2.0.0",
        core_asset: "1.3.0"
    };

    upgradeAccount(id, lifetime, e) {
        e.preventDefault();
        AccountActions.upgradeAccount(id, lifetime);
    }

    componentWillMount() {
        let account = this.props.account.toJS();

        accountUtils.getFinalFeeAsset(this.props.account, "account_upgrade");
        this.retrieveVestingBalances.call(this, account.id);
    }

    retrieveVestingBalances(accountId){
        Apis.instance().db_api().exec("get_vesting_balances", [
            accountId
        ]).then(vbs => {
            this.setState({vbs});
        }).catch(err => {
            console.log("error:", err);
        });
    }


    render() {
        let {gprops, core_asset} = this.props;
        let account = this.props.account.toJS();

        let ltr = ChainStore.getAccount( account.lifetime_referrer, false );
        if( ltr ) account.lifetime_referrer_name = ltr.get('name');
        let ref = ChainStore.getAccount( account.referrer, false );
        if( ref ) account.referrer_name = ref.get('name');
        let reg = ChainStore.getAccount( account.registrar, false );
        if( reg ) account.registrar_name = reg.get('name');

        let account_name = account.name;

        let network_fee  = account.network_fee_percentage/100;
        let lifetime_fee = account.lifetime_referrer_fee_percentage/100;
        let referrer_total_fee = 100 - network_fee - lifetime_fee;
        let referrer_fee  = referrer_total_fee * account.referrer_rewards_percentage/10000;
        let registrar_fee = 100 - referrer_fee - lifetime_fee - network_fee;

        let lifetime_cost = gprops.getIn(["parameters", "current_fees", "parameters", 8, 1, "membership_lifetime_fee"]) * gprops.getIn(["parameters", "current_fees", "scale"]) / 10000;
        let annual_cost = gprops.getIn(["parameters", "current_fees", "parameters", 8, 1, "membership_annual_fee"]) * gprops.getIn(["parameters", "current_fees", "scale"]) / 10000;


        let member_status = ChainStore.getAccountMemberStatus(this.props.account);
        let membership = "account.member." + member_status;
        let expiration = null
        if( member_status === "annual" )
           expiration = (<span>(<Translate content="account.member.expires"/> <TimeAgo time={account.membership_expiration_date} />)</span>)
        let expiration_date = account.membership_expiration_date;
        if( expiration_date === "1969-12-31T23:59:59" )
           expiration_date = "Пожиезненно"
        else if( expiration_date === "1970-01-01T00:00:00" )
           expiration_date = "N/A"

        let {vbs} = this.state;

        let balances = "";
        if(vbs) {
            balances = vbs.map(vb => {
                if (vb.balance.amount) {
                    return <VestingBalance key={vb.id} vb={vb} account={account}/>;
                }
            }).filter(a => {
                return !!a;
            });
        }


        return (
            <div className="grid-content" style={{overflowX: "hidden"}}>
                <div className="content-block no-margin">
                    <h3><Translate content={membership}/> {expiration}</h3>
                    { member_status=== "lifetime" ? null : (
                       <div>
                           <div className="large-6 medium-8">
                               <HelpContent path="components/AccountMembership" section="lifetime" feesCashback={100 - network_fee} price={{amount: lifetime_cost, asset: core_asset}}/>
                               <div className="referral-membership__btn-wrap">
                                   {member_status === "lifetime" ? null : (
                                   <div className="acc-btn__inner" style={{marginBottom: '15px'}} onClick={this.upgradeAccount.bind(this, account.id, true)}>
                                       <Translate content="account.member.upgrade_lifetime"/>
                                   </div>)}
                                   {member_status === "annual" || member_status === "lifetime" ? null : (
                                   <div className="acc-btn__inner" onClick={this.upgradeAccount.bind(this, account.id, false)}>
                                       <Translate content="account.member.upgrade_annual"/>
                                   </div>)}
                               </div>
                            </div>
                       <br/><hr/>
                       </div>
                    )}
                </div>
                <div className="content-block no-margin referral-membership__table">
                    <div className="no-margin grid-block vertical large-horizontal">
                        <div className="no-margin grid-block">
                            <div className="referral-membership__grid">
                                <div>
                                    <h4 className="referral-membership__subtitle"><Translate content="account.member.referral_link"/></h4>
                                    <Translate content="account.member.referral_text"/>:
                                    <div className="referral-membership__link">{`https://netizens/?r=${account.name}`}</div>
                                </div>
                                <h4 className="referral-membership__subtitle"><Translate content="account.member.fee_allocation"/></h4>
                                <table className="table key-value-table">
                                    <tbody>
                                        <tr>
                                            <td><Translate content="account.member.network_percentage"/></td>
                                            <td>{network_fee}%</td>
                                        </tr>
                                        <tr>
                                            <td><Translate content="account.member.registrar"/>  &nbsp;
                                            (<Link to={`account/${account.registrar_name}/overview`}>{account.registrar_name}</Link>)
                                            </td>
                                            <td>{registrar_fee}%</td>
                                        </tr>
                                        <tr>
                                            <td><Translate content="account.member.referrer"/>  &nbsp;
                                            (<Link to={`account/${account.referrer_name}/overview`}>{account.referrer_name }</Link>)
                                            </td>
                                            <td>{referrer_fee}%</td>
                                        </tr>
                                        <tr>
                                            <td><Translate content="account.member.membership_expiration"/> </td>
                                            <td>{expiration_date}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <h4 className="referral-membership__subtitle" style={{paddingTop: "1rem"}}><Translate content="account.member.fees_cashback"/></h4>
                                <table className="table key-value-table">
                                    <Statistics stat_object={account.statistics}/>
                                </table>
                            </div>
                        </div>
                        <div className="referral-membership__grid" style={{overflowX: "hidden"}}>
                            {!balances.length ? (
                            <h4 className="referral-membership__subtitle" style={{paddingTop: "1rem"}}>
                                <Translate content={"account.vesting.no_balances"}/>
                            </h4>) : balances}
                        </div>
                        <div className="referral-membership__grid" style={{overflow: "scroll"}}>
                            <table className="referral-table">
                                <tbody>
                                    <tr className="referral-table__header">
                                        <th className="referral-table__item"><Translate content={"referral-data.level"}/></th>
                                        <th className="referral-table__item">%</th>
                                        <th className="referral-table__item"><Translate content={"referral-data.ref-number"}/></th>
                                        <th className="referral-table__item"><Translate content={"referral-data.am-referrals"}/></th>
                                        <th className="referral-table__item"><Translate content={"referral-data.lm-referrals"}/></th>
                                        <th className="referral-table__item"><Translate content={"referral-data.ref-active"}/></th>
                                    </tr>
                                    <tr className="referral-table__string">
                                        <td className="referral-table__item">1</td>
                                        <td className="referral-table__item">21</td>
                                        <td className="referral-table__item">16</td>
                                        <td className="referral-table__item">11</td>
                                        <td className="referral-table__item">5</td>
                                        <td className="referral-table__item">5</td>
                                    </tr>
                                    <tr className="referral-table__string">
                                        <td className="referral-table__item">2</td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                    </tr>
                                    <tr className="referral-table__string">
                                        <td className="referral-table__item">3</td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                    </tr>
                                    <tr className="referral-table__string">
                                        <td className="referral-table__item">4</td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                        <td className="referral-table__item"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="grid-block large-7">
                            <div className="grid-content">
                                <FeeHelp
                                    account={account_name}
                                    networkFee={network_fee}
                                    referrerFee={referrer_fee}
                                    registrarFee={registrar_fee}
                                    lifetimeFee={lifetime_fee}
                                    referrerTotalFee={referrer_total_fee}
                                    maintenanceInterval={gprops.getIn(["parameters", "maintenance_interval"])}
                                    vestingThreshold={{amount: gprops.getIn(["parameters", "cashback_vesting_threshold"]) , asset: core_asset}}
                                    vestingPeriod={gprops.getIn(["parameters", "cashback_vesting_period_seconds"]) /60/60/24}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
AccountMembership.VestingBalance = VestingBalance;
AccountMembership = BindToChainState(AccountMembership);

export default AccountMembership;
