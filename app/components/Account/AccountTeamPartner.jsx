import React from "react";
import Translate from "react-translate-component";
import Message from "../Message/Message";
import counterpart from "counterpart";


var partner1 = require("assets/app_img/partner1.png");
var partner2 = require("assets/app_img/partner2.png");
var partner3 = require("assets/app_img/partner3.png");
var message = require("assets/app_img/message.png");

class AccountTeamPartner extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };
    constructor(props, context) {
        super(props);

        this.last_path = null;

        this.state = {
            active: context.location.pathname,
            account: this.props.account
        };

        this.openMessageModal = this.openMessageModal.bind(this);
    }

    _copyToBuffer() {
        var copyBtn = document.getElementById(this.props.partnerData.id + "copyBtnRef");
        copyBtn.classList.remove('referral-link__note--fadeIn');

        var copyText = document.getElementById(this.props.partnerData.id + "_refLink").innerText;
        var temp = document.createElement('textarea');

        temp.value = copyText;
        temp.style.position = 'absolute';
        temp.style.left = '-9999px';
        document.body.appendChild(temp);

        temp.select();
        document.execCommand('copy', false, copyText);
        document.body.removeChild(temp);
        copyBtn.classList.add('referral-link__note--fadeIn');
    }

    _onNavigate(route, e) {
        e.preventDefault();
        this.context.router.push(route);
    }

    openMessageModal() {
        document.getElementById("messageOverlay_"+this.props.partnerData.id).classList.add('message__overlay--active');
    }


    render() {
        let statuses = [
            counterpart.translate("statuses.status_01"), 
            counterpart.translate("statuses.status_02"), 
            counterpart.translate("statuses.status_03"), 
            counterpart.translate("statuses.status_04")
        ];
        let statusBlock;

        if (this.props.partnerData.status == 1) {
            statusBlock = <img className="user-team__img" style={{ width: 20, height: 20 }} src={partner1} />
        }

        if (this.props.partnerData.status == 2) {
            statusBlock = <img className="user-team__img" style={{ width: 20, height: 20 }} src={partner2} />
        }

        if (this.props.partnerData.status == 3) {
            statusBlock = <img className="user-team__img" style={{ width: 20, height: 20 }} src={partner3} />
        }

        return (
            <li className="user-team__item" id={this.props.partnerData.name}>
                <Message 
                    from_account={this.state.account}
                    to_name={this.props.partnerData.name}
                    to_id={this.props.partnerData.id}
                />
                <div className="user-team__block user-team__block--header">
                    <div className="user-team__header">
                        <a onClick={this._onNavigate.bind(this, `/account/${this.props.partnerData.name}/my-team/`)} className="user-team__name">{this.props.partnerData.name}</a>
                        <img onClick={this.openMessageModal} className="user-team__message" src={message} />
                    </div>


                    {!this.props.partnerData.status == 0 ?
                        <div className="user-team__inner">
                            {statusBlock}
                            <span className="user-team__data user-team__data--status">{statuses[this.props.partnerData.status]}</span>
                        </div>
                        : null}


                    <div className="user-team__inner user-team__inner--link" id={this.props.partnerData.id + "_refLink"}>
                        <span className="user-team__data user-team__data--url">https://ntz.team/?r={this.props.partnerData.name}
                        </span>
                    </div>

                    <div className="user-team__copy-btn-wrap">
                        <Translate className="referral-link__note" id={this.props.partnerData.id + "copyBtnRef"} content="account.note-copy" />
                        <div onClick={this._copyToBuffer.bind(this)}><Translate className="user-team__copy-btn" content="account.ref-copy" />
                        </div>
                    </div>
                </div>
                <div className="user-team__block">
                    <div className="user-team__inner">
                        <span className="user-team__data">
                            <Translate content="account.leader-level" component="span" />
                        </span>
                        <span className="user-team__data user-team__data--value">{this.props.partnerData.leaders_level}</span>
                    </div>
                    <div className="user-team__inner">
                        <span className="user-team__data">
                            <Translate content="account.total-levels" component="span" />
                        </span>
                        <span className="user-team__data user-team__data--value">{this.props.partnerData.depth}</span>
                    </div>
                    <div className="user-team__inner">
                        <span className="user-team__data">
                            <Translate content="account.leaders-number" component="span" />
                        </span>
                        <span className="user-team__data user-team__data--value">{this.props.partnerData.total_leaders}</span>
                    </div>
                    <div className="user-team__inner">
                        <span className="user-team__data">
                            <Translate content="account.total-members" component="span" />
                        </span>
                        <span className="user-team__data user-team__data--value">{this.props.partnerData.total_down}</span>
                    </div>
                </div>
            </li>
        );
    }
}

export default AccountTeamPartner;
