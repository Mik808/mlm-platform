import React from "react";
import Translate from "react-translate-component";
import { Apis } from "bitsharesjs-ws";
import classnames from "classnames";
import AccountStore from "stores/AccountStore";


class PaymentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: 0,
            email: '',
            currentAccount: '',
            amountValue: 0,
            ntzValue: 0
        };
        this.getNtzExchangeRate = this.getNtzExchangeRate.bind(this);
        this.getUserEmail = this.getUserEmail.bind(this);
        this.isValid = this.isValid.bind(this);
    }
    static contextTypes = {
        router: React.PropTypes.object
    };
    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var amountParam = parseFloat(url.searchParams.get("amount"));


        if (amountParam) {
            var ntzRate = this.state.data;
            var amountValue = amountParam * ntzRate;
            this.setState({
                ntzValue: amountParam,
                amountValue: amountValue
            })
        }
        this.setState({
            currentAccount: AccountStore.getState().currentAccount
        })
    }
    getUserEmail(e) {
        this.setState({
            email: e.target.value
        })
    }
    getNtzExchangeRate(e) {
        var ntzValue = e.target.value;
        var ntzRate = 1;
        var amountValue = ntzValue * ntzRate;

        this.setState({
            ntzValue: ntzValue,
            amountValue: amountValue
        })
    }
    isValid() {
        if (document.getElementById('paymentAgreement').checked = true) {
            document.getElementById("paymentBtn").disabled = false;
        }
        else {
            document.getElementById("paymentBtn").disabled = true;
        }
    }
    onSubmit(e) {
        e.preventDefault();
        const xhr = new XMLHttpRequest();
        let email = this.state.email;
        let amount = this.state.amountValue;
        let currentAccount = this.state.currentAccount;
		console.log('TCL: PaymentForm -> onSubmit -> currentAccount', currentAccount)
        let ntz = this.state.ntzValue;

        xhr.open('POST', 'https://payment.ntz.team/requestpayment/', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        var data = 'email=' + email + '&account=' + currentAccount + '&amount=' + amount + '&ntz=' + ntz;
        xhr.send(data);

        xhr.onreadystatechange = () => {
            if (xhr.status == 200) {
                console.log(xhr.responseText);
                window.location = xhr.responseText;
            }
            else {
                alert("Error");
            }
        }
    }
    render() {
        return (
            <div className="payment-form__wrap">
                <Translate content="payment.title" className="partnership__header" component="h1"/>

                <form className="payment-form" onSubmit={this.onSubmit.bind(this)}>
                    {/* <div className="payment-form__description">
                        <Translate content="payment.description" component="span" />
                    </div>
                    <div className="payment-form__ex-wrap">
                        <Translate content="payment.rate" />
                        <span className="payment-form__rate">1
                            <Translate content="payment.rub" />
                        </span>
                    </div> */}

                    <fileldset>
                        <div className="payment-form__inner">
                            <label className="payment-form__label" htmlFor="userMail">
                                <Translate content="payment.user_email" />
                            </label>
                            <input type="email" name="email" id="userMail" ref="emailRef" autoComplete='email' onChange={this.getUserEmail.bind(this)} required autoFocus />
                        </div>

                        <div className="payment-form__inner">
                            <label className="payment-form__label" htmlFor="userNTZ">
                                <Translate content="payment.user_NTZ" />
                            </label>
                            <input className="settings__item" value={this.state.ntzValue} type="number" onChange={this.getNtzExchangeRate.bind(this)} name="NTZ_amount" id="userNTZ" ref="ntzRef"  required />
                        </div>

                        <div className="payment-form__inner payment-form__inner--hidden">
                            <label className="payment-form__label" htmlFor="amount">
                                <Translate content="payment.user_amount" />
                            </label>
                            <input className="settings__item" type="number" name="amount" id="amount" ref="amountRef" value={this.state.amountValue} disabled required />
                        </div>
                    </fileldset>

                    <div className="payment-form__inner">
                        <label htmlFor="paymentAgreement" className="payment-form__agree">
                            <input className="payment-form__checkbox" type="checkbox" id="paymentAgreement" name="paymentAgreement" onClick={this.isValid.bind(this)} />
                            <Translate content="payment.agree" />
                        </label>
                    </div>

                    {/* BUTTON */}
                    <div className="content-block">
                        <button className={classnames("button button--payment")} type="submit" value="Submit" id="paymentBtn" disabled>
                            <Translate component="span" content="payment.add_btn" />
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
export default PaymentForm;