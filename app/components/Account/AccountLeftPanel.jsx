import React from "react";
import { PropTypes } from "react";
import { Link } from "react-router/es";
import ReactTooltip from "react-tooltip";
import AccountInfo from "./AccountInfo";
import Translate from "react-translate-component";
import AccountActions from "actions/AccountActions";
import SettingsActions from "actions/SettingsActions";
import QRCode from 'qrcode.react';
import Utils from "common/utils.js";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import { isIOS } from "react-device-detect";
import TotalBalanceValue from "../Utility/TotalBalanceValue";
import { Apis } from "bitsharesjs-ws";
import ls from "common/localStorage";
import AccountStore from "stores/AccountStore";
import counterpart from "counterpart";

const STORAGE_KEY = "__graphene__";
let ss = new ls(STORAGE_KEY);

var refLink = require("assets/app_img/ref-link.png");
var partnership = require("assets/app_img/partner_balance.png");
var login = require("assets/app_img/login.png");
var logout = require("assets/app_img/logout.png");
var transactions = require("assets/app_img/op_history.png");
var team = require("assets/app_img/team.png");
var topMenu = require("assets/app_img/top-menu.png");
var userSettings = require("assets/app_img/settings.png");
var market = require("assets/app_img/market.png");
var deposit = require("assets/app_img/deposit.png");
var docs = require("assets/app_img/docs.png");
var transfer = require("assets/app_img/transfer.png");
var explorer = require("assets/app_img/explorer.png");
var events = require("assets/app_img/events.png");
var reg_img = require("assets/app_img/reg.png");
var promo = require("assets/app_img/promo.png");
var withdraw = require("assets/app_img/withdraw.png");
var support = require("assets/app_img/headset.png");

class AccountLeftPanel extends React.Component {

    static propTypes = {
        account: React.PropTypes.object.isRequired,
        linkedAccounts: PropTypes.object
    };

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.last_path = null;

        this.state = {
            showAdvanced: props.viewSettings.get("showAdvanced", false),
            qrSize: 150,
            qrCodeShown: false,
            userId: this.props.account.get('id'),
            invoiceUnpaid: [],
            renderInvoices: false,
            locale: ss.get("settings_v3").locale
            // currentAccount: AccountStore.getState().currentAccount
        };
        this.selectValue = this.selectValue.bind(this);
        this.showUserMenu = this.showUserMenu.bind(this);
        this.hideUserMenu = this.hideUserMenu.bind(this);
    }
    shouldComponentUpdate(nextProps, nextState) {
        const changed = this.last_path !== window.location.hash;
        this.last_path = window.location.hash;
        return (
            changed ||
            this.props.account !== nextProps.account ||
            this.props.isMyAccount !== nextProps.isMyAccount ||
            this.props.linkedAccounts !== nextProps.linkedAccounts ||
            this.state.showAdvanced !== nextState.showAdvanced ||
            this.props.wallet_locked !== nextProps.wallet_locked ||
            this.props.locked !== nextProps.locked ||
            this.props.current_wallet !== nextProps.current_wallet
        )
    }

    componentWillUnmount() {
        ReactTooltip.hide();
    }

    componentDidMount() {
        let userID = this.state.userId;
        let currentDate = Date.now();

        Apis.instance().db_api().exec("get_invoices", [userID, 0]).then(invoiceUnpaid => {
            for (var i = 0; i < invoiceUnpaid.length; i++) {
                if (Date.parse(invoiceUnpaid[i]['expiration']) > currentDate) {
                    var invoiceUnpaidActual = { id: invoiceUnpaid[i]['id'] }
                    this.state.invoiceUnpaid.push(invoiceUnpaidActual);
                }
            }
            this.setState({
                renderInvoices: true
            })
        }).catch(err => {
            console.log("error:", err);
        });
    }

    onLinkAccount(e) {
        e.preventDefault();
        AccountActions.linkAccount(this.props.account.get("name"));
    }

    onUnlinkAccount(e) {
        e.preventDefault();
        AccountActions.unlinkAccount(this.props.account.get("name"));
    }

    _toggleAdvanced(e) {
        e.preventDefault();
        let newState = !this.state.showAdvanced;
        this.setState({
            showAdvanced: newState
        });

        SettingsActions.changeViewSetting({ showAdvanced: newState });
    }

    _toggleQRcode() {
        if (this.state.qrCodeShown) {
            this.setState({
                qrCodeShown: false
            });
            document.getElementById('QRcodeWrap').classList.add('acc-btn__qr--hide');
        }
        else {
            this.setState({
                qrCodeShown: true
            });
            document.getElementById('QRcodeWrap').classList.remove('acc-btn__qr--hide');
        }
    }

    _onNavigate(route, e) {
        e.preventDefault();
        this.context.router.push(route);
    }

    _copyToBuffer() {
        var copyBtn = document.getElementById('copyBtn');
        copyBtn.classList.remove('referral-link__note--fadeIn');

        var copyText = document.getElementById('refLink').value;
        var temp = document.createElement('textarea');

        temp.value = copyText;
        temp.style.position = 'absolute';
        temp.style.left = '-9999px';
        document.body.appendChild(temp);

        temp.select();
        document.execCommand('copy', false, copyText);
        document.body.removeChild(temp);
        copyBtn.classList.add('referral-link__note--fadeIn');
    }

    _toggleLock(e) {
        e.preventDefault();
        if (WalletDb.isLocked()) {
            WalletUnlockActions.unlock().then(() => {
                AccountActions.tryToSetCurrentAccount();
            });
        } else {
            WalletUnlockActions.lock();
            location.reload();
        }
    }
    selectValue(id, e) {
        document.getElementById(id).select()
    }

    showUserMenu() {
        document.getElementById('userLoginMenu').classList.remove('user-login__list--hidden');
    }

    hideUserMenu() {
        document.getElementById('userLoginMenu').classList.add('user-login__list--hidden');
    }


    render() {
        let { account, wallet_locked, isMyAccount } = this.props;
        let account_name = account.get("name");
        let width = window.innerWidth;
        var dashboard;
        var referralSection;
        var invoiceUnpaidNum = this.state.invoiceUnpaid.length;
        let locale = this.state.locale;

        referralSection = <div className="referral-link">
            <input
                className="referral-link__ref"
                id="refLink"
                defaultValue={`https://ntz.team/?r=${account_name}`}
                onClick={this.selectValue.bind(this, 'refLink')}
            />
            {!isIOS ?
                <div className="acc-btn acc-btn--wrap">
                    <Translate className="referral-link__note" id="copyBtn" content="account.note-copy" />
                    <div className="acc-btn__block" onClick={this._copyToBuffer.bind(this)}><Translate className="acc-btn__inner" content="account.ref-copy" /></div>
                </div>
                : null
            }
        </div>

        dashboard = width > 600 ?
            <Link to={`/account/${account_name}/dashboard`}>
                <div className="user-account__item">
                    <img style={{ width: 30, height: 30 }} src={transactions} />
                    <Translate className="user-account__link" component="span" content="header.account_tr" />
                </div>
            </Link>
            :
            <Link to={`/account/${account_name}/overview`}>
                <div className="user-account__item">
                    <img style={{ width: 30, height: 30 }} src={transactions} />
                    <Translate className="user-account__link" component="span" content="header.account_tr" />
                </div>
            </Link>

        let lock_unlock;

        if (WalletDb.isLocked()) {
            lock_unlock = <div>
                <img style={{ width: 22, height: 22 }} src={login} />
                <Translate className="user-login__link" content="account.login" />
            </div>
        }
        else {
            lock_unlock = <div>
                <img style={{ width: 22, height: 22 }} src={logout} />
                <Translate className="user-login__link" content="account.logout" />
            </div>
        }

        return (
            <div className="user-account__wrap">
                <div className="user-account__info" id="userAccountInfo">
                    <div className="user-login__wrap">
                        <img className="user-login__menu-icon" style={{ width: 25, height: 25 }} src={topMenu} onClick={this.showUserMenu.bind(this)} />

                        {/*TOP MENU*/}
                        <ul className="user-login__list user-login__list--hidden" id="userLoginMenu" onClick={this.hideUserMenu.bind(this)}>
                            <li className="user-login__item">
                                <img style={{ width: 25, height: 25 }} src={reg_img} />
                                <Link className="user-login__link" onClick={this._onNavigate.bind(this, "/")}>
                                    <Translate content="account.index" />
                                </Link>
                            </li>
                            <li className="user-login__item">
                                <img style={{ width: 25, height: 25 }} src={userSettings} />
                                <Link className="user-login__link" onClick={this._onNavigate.bind(this, "/settings")}>
                                    <Translate content="account.settings" />
                                </Link>
                            </li>
                            <li className="user-login__item">
                                <img style={{ width: 25, height: 25 }} src={explorer} />
                                <Link className="user-login__link" onClick={this._onNavigate.bind(this, "/explorer/blocks")}>
                                    <Translate content="account.explorer" />
                                </Link>
                            </li>
                            <li className="user-login__item" onClick={this._toggleLock.bind(this)}>
                                {lock_unlock}
                            </li>
                        </ul>
                    </div>
                    <AccountInfo
                        account={account.get("id")}
                        image_size={{ height: 40, width: 40 }}
                        my_account={isMyAccount}
                    />
                    <TotalBalanceValue.AccountWrapper label="header.account_value" accounts={[this.props.account]} inHeader={false} />
                </div>

                {isMyAccount ?
                    <div className="user-account__personal">
                        <button className="user-account__op-btn">
                            <Link className="user-account__tranfer-link" to={`/account/${account_name}/my-orders/`}>
                                <Translate content="account.my-orders" />
                                {invoiceUnpaidNum > 0 ?
                                    <span className="user-account__orders">{invoiceUnpaidNum}</span>
                                    : null
                                }
                            </Link>
                        </button>



                        {/* BUSINESS */}
                        {/*  <Link to={`/account/${account_name}/training/`} activeClassName="active">
                        <div className="training-menu__wrap">
                            <Translate className="training-menu__link " content="account.business"/>
                            <img className="training-menu__icon" src={training} />
                            <Translate className="training-menu__link" content="account.training"/>
                        </div>
                    </Link> */}



                        <div className="user-account__company-block">
                            {/* MARKETS */}
                            <Link to={locale === "ru" ? `/` : `/marketplace/`} className="user-account__item user-account__item--company" activeClassName="active">
                                <img style={{ width: 35, height: 35 }} src={market} />
                                <span className="user-event__link"> 
                                    {locale === "ru" ? counterpart.translate("account.store") : counterpart.translate("account.marketplace")}
                                </span>
                            </Link>

                            {/*EVENTS*/}
                            <Link to={`/events/`} className="user-account__item user-account__item--company" activeClassName="active">
                                <img style={{ width: 35, height: 35 }} src={events} />
                                <Translate className="user-event__link" content="account.tickets" />
                            </Link>

                            {/* PROMO */}
                            <Link to={`/promo/`} className="user-account__item user-account__item--company" activeClassName="active">
                                <img style={{ width: 35, height: 35 }} src={promo} />
                                <Translate className="user-event__link" content="account.promo" />
                            </Link>
                        </div>

                        {/* PAYMENT */}
                        {locale == "ru" ?
                            <Link to={`/account/${account_name}/payment`} activeClassName="active">
                                <div className="user-account__item">
                                    <img style={{ width: 30, height: 30 }} src={deposit} />
                                    <Translate className="user-account__link" component="span" content="account.balance" />
                                </div>
                            </Link>
                            : null}


                        {/*PARTNERS*/}
                        <Link to={`/account/${account_name}/vesting/`} activeClassName="active">
                            <div className="user-account__item">
                                <img style={{ width: 30, height: 30 }} src={partnership} />
                                <Translate className="user-account__link" component="span" content="account.vesting.title" />
                            </div>
                        </Link>

                        {/*TEAM*/}
                        <Link to={`/account/${account_name}/my-team/`} activeClassName="active">
                            <div className="user-account__item">
                                <img style={{ width: 30, height: 30 }} src={team} />
                                <Translate className="user-account__link" component="span" content="account.my-team" />
                            </div>
                        </Link>

                        <div className="user-account__item">
                            <div onClick={this._toggleQRcode.bind(this)}>
                                <img style={{ width: 30, height: 30 }} src={refLink} />
                                <Translate className="user-account__link" component="span" className="user-account__link" content="account.ref-link" />
                            </div>
                        </div>

                        {/*QRCODE*/}
                        <div className="acc-btn__qr--hide acc-btn__qr" id="QRcodeWrap">
                            {referralSection}
                            <QRCode size={this.state.qrSize} value={`https://ntz.team/?r=${account_name}`} />
                        </div>

                        {/*TRANSFER*/}
                        <Link to={`/account/${account_name}/transfer/`} activeClassName="active">
                            <div className="user-account__item">
                                <img style={{ width: 30, height: 30 }} src={transfer} />
                                <Translate className="user-account__link" component="span" className="user-account__link" content="account.pay" />
                            </div>
                        </Link>

                        {/*GATEWAY*/}
                        <Link to={`/account/${account_name}/gateway/`} activeClassName="active">
                            <div className="user-account__item">
                                <img style={{ width: 30, height: 30 }} src={withdraw} />
                                <Translate className="user-account__link" component="span" className="user-account__link" content="account.withdraw.title" />
                            </div>
                        </Link>

                        {/* OP HISTORY */}
                        {dashboard}

                        {/* SUPPORT */}
                        <Link to={`/account/${account_name}/support/`} activeClassName="active">
                            <div className="user-account__item">
                                <img style={{ width: 30, height: 30 }} src={support} />
                                <Translate className="user-account__link" component="span" className="user-account__link" content="account.votes.support" />
                            </div>
                        </Link>

                        {/*DOCUMENTS*/}
                        <a href="https://drive.google.com/drive/folders/1LDPTsm6g0x6rDIEZndaIR5z7EOU5GVyu" target="_blank">
                            <div className="user-account__item user-account__item--docs">
                                <img style={{ width: 30, height: 30 }} src={docs} />
                                <Translate className="user-account__link" component="span" className="user-account__link" content="account.documents" />
                            </div>
                        </a>
                        {/*<div className="user-account__item">
                            <img style={{width: 30, height: 30}} src={voting} />
                        <Link to={`/account/${account_name}/voting/`} className="user-account__link" activeClassName="active"><Translate content="account.voting"/></Link>
                    </div>
                    <div className="user-account__item">

                        <img style={{width: 30, height: 30}} src={access} />
                        <Link to={`/account/${account_name}/permissions/`} className="user-account__link" activeClassName="active"><Translate content="account.permissions"/></Link>
                    </div>*/}
                        {/*<div className="user-account__item">
                        <img style={{width: 30, height: 30}} src={whitelist} />
                        <Link to={`/account/${account_name}/whitelist/`} className="user-account__link" activeClassName="active"><Translate content="account.whitelist.title"/></Link>
                    </div>*/}
                    </div>
                    : null}
            </div>
        );
    }
}

export default AccountLeftPanel;
