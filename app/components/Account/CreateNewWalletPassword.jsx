import React from "react";
import { connect } from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import {Link} from "react-router/es";
import AccountSelect from "../Forms/AccountSelect";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../LoadingIndicator";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import {ChainStore, FetchChain, key} from "bitsharesjs/es";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import Icon from "../Icon/Icon";
import CopyButton from "../Utility/CopyButton";

class CreateNewWalletPassword extends React.Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    constructor() {
        super();
        this.state = {
            validAccountName: true,
            accountName: 'null',
            description: '',
            validPassword: true,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: true,
            step: 1,
            showPass: true,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(0, 45)
        };
        this.onFinishConfirm = this.onFinishConfirm.bind(this);
    }

    componentWillMount() {
        if (!WalletDb.getWallet()) {
            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: true
            });
        }
    }

    componentDidMount() {
        ReactTooltip.rebuild();
        
        let firstAccount = AccountStore.getMyAccounts().length;
        
        let dictJson = require("json-loader!common/10k_en.json");
        
        let brainFirst =dictJson[Math.floor(Math.random()*dictJson.length)];
        let brainSecond = dictJson[Math.floor(Math.random()*dictJson.length)];
            
        
        if (firstAccount > 0) {
            console.log("GOT ACCOUNTS!!!");
        }
        else {
            let name = (brainFirst + "-" + brainSecond).toLowerCase();
            this.setState({accountName: name});
            let password = this.state.generatedPassword;
            let description = this.state.description;

            this.createAccount(name, description, password);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state);
    }

    isValid() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
//        if (!WalletDb.getWallet()) {
//            valid = valid && this.state.validPassword;
//        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return valid;
    }

    onAccountNameChange(e) {
        const state = {};
        if(e.valid !== undefined) state.validAccountName = e.valid;
        if(e.value !== undefined) state.accountName = e.value;
        if (!this.state.show_identicon) state.show_identicon = true;
        this.setState(state);
    }

    onFinishConfirm(confirm_store_state) {
        if(confirm_store_state.included && confirm_store_state.broadcasted_transaction) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {[this.state.accountName]: true}).then(() => {
                this.props.router.push("/wallet/backup/create?newAccount=true");
            });
        }
    }

    _unlockAccount(name, password) {
        WalletDb.validatePassword(password, true, name);
        WalletUnlockActions.checkLock.defer();
    }

    createAccount(name, description, password) {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = AccountStore.getState().referralAccount;
        let promoValue = AccountStore.getState().promoValue;
        this.setState({loading: true});
        
        AccountActions.createAccountWithPassword(name, description, password, this.state.registrar_account, referralAccount || this.state.registrar_account, 0, refcode, promoValue).then(() => {
            AccountActions.setPasswordAccount(name);
            AccountActions.setCurrentAccount(name);
            // User registering his own account
            if(this.state.registrar_account) {
                FetchChain("getAccount", name, undefined, {[name]: true}).then(() => {
                    this.setState({
                        step: 2,
                        loading: false
                    });
                    this._unlockAccount(name, password);
                });
                TransactionConfirmStore.listen(this.onFinishConfirm);
            } else { // Account registered by the faucet
                FetchChain("getAccount", name, undefined, {[name]: true}).then(() => {
                    this.setState({
                        step: 2
                    });
                    this._unlockAccount(name, password);
                });

            }
        }).catch(error => {
            console.log("ERROR AccountActions.createAccount", error);
            let error_msg = error.base && error.base.length && error.base.length > 0 ? error.base[0] : "unknown error";
            if (error.remote_ip) error_msg = error.remote_ip[0];
            notify.addNotification({
                message: `Failed to create account: ${name} - ${error_msg}`,
                level: "error",
                autoDismiss: 10
            });
            this.setState({loading: false});
        });
    }

    onSubmit(e) {
        e.preventDefault();
        
        let name = this.state.accountName;
        let password = this.state.generatedPassword;
        let description = this.state.description;
        let validAccountName = this.state.validAccountName;
        
        console.log(validAccountName);
        console.log(name, description, password);
        this.createAccount(name, description, password);
    }

    onRegistrarAccountChange(registrar_account) {
        this.setState({registrar_account});
    }

    // showRefcodeInput(e) {
    //     e.preventDefault();
    //     this.setState({hide_refcode: false});
    // }

//    _onInput(value, e) {
//        this.setState({
//            [value]: value === "confirm_password" ? e.target.value : !this.state[value],
//            validPassword: value === "confirm_password" ? e.target.value === this.state.generatedPassword : this.state.validPassword
//        });
//    }

    _renderAccountCreateForm() {
        let firstAccount = AccountStore.getMyAccounts().length;
        
        if (firstAccount > 0) {
            return (
                <div>
                    <p style={{display: "block", textAlign: "center"}}>
                        <Translate content="wallet.account_generated" />
                    </p>
                    <div className="button success" style={{width: "50%", marginTop: "30px"}} onClick={() => {
                        SettingsActions.changeSetting({setting: "passwordLogin", value: true});
                        WalletUnlockActions.unlock.defer();
                    }}>
                        <Translate content="header.unlock_short" />
                    </div>
                </div>
            )
        }
        else {
            
        let {registrar_account} = this.state;
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account ? ChainStore.getAccount(registrar_account) : null;
        if (registrar) {
            if( registrar.get( "lifetime_referrer" ) == registrar.get( "id" ) ) {
                isLTM = true;
            }
        }

        let buttonClass = classNames("submit-button button no-margin");
        
        return (
            <div style={{textAlign: "left"}}>
                <form
                    style={{minWidth: "20rem", maxWidth: "30rem", paddingBottom: "30px"}}
                    onSubmit={this.onSubmit.bind(this)}
                    noValidate
                >
                <section style={{marginBottom: "0.7rem"}}>
                    <label className="left-label"><Translate content="wallet.contract_public" />&nbsp;&nbsp;<span className="tooltip" data-html={true} data-tip={counterpart.translate("tooltip.generate")}></span></label>
                    <div style={{paddingBottom: "0.5rem"}}>
                        <span className="inline-label">
                            <input 
                                name="accountname"
                                id="accountname"
                                onChange={this.onAccountNameChange.bind(this)}
                                type="text" 
                                className="contract-description" 
                                style={{width: "100%", fontSize: "80%"}} 
                                value={this.state.accountName}/>
                        </span>
                    </div>
                </section>

                <section>
                    <label className="left-label"><Translate content="wallet.generated" />&nbsp;&nbsp;<span className="tooltip" data-html={true} data-tip={counterpart.translate("tooltip.generate")}></span></label>
                    <div style={{paddingBottom: "0.5rem"}}>
                        <span className="inline-label">
                            <input 
                            style={{fontSize: "80%"}} 
                            onChange={this.onAccountNameChange.bind(this)}
                            value={this.state.generatedPassword} 
                            type="text"/>
                        </span>
                    </div>
                </section>
            <br />

                {/* Submit button */}
                {this.state.loading ?  <LoadingIndicator type="three-bounce"/> : <button style={{width: "100%"}} className={buttonClass}><Translate content="account.create_account" /></button>}

                {/* Backup restore option */}
                {/* <div style={{paddingTop: 40}}>
                    <label>
                        <Link to="/existing-account">
                            <Translate content="wallet.restore" />
                        </Link>
                    </label>

                    <label>
                        <Link to="/create-wallet-brainkey">
                            <Translate content="settings.backup_brainkey" />
                        </Link>
                    </label>
                </div> */}

                {/* Skip to step 3 */}
                {/* {(!hasWallet || firstAccount ) ? null :<div style={{paddingTop: 20}}>
                    <label>
                        <a onClick={() => {this.setState({step: 3});}}><Translate content="wallet.go_get_started" /></a>
                    </label>
                </div>} */}
            </form>
                {/* <br />
                <p>
                    <Translate content="wallet.bts_rules" unsafe />
                </p> */}
            </div>
            );
        }
    }

    _renderAccountCreateText() {
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div>
                <h4 style={{fontWeight: "bold", paddingBottom: 15}}><Translate content="wallet.wallet_password" /></h4>

                <Translate style={{textAlign: "left"}} unsafe component="p" content="wallet.create_account_password_text" />

                <Translate style={{textAlign: "left"}} component="p" content="wallet.create_account_text" />

                {firstAccount ?
                    null :
                    <Translate style={{textAlign: "left"}} component="p" content="wallet.not_first_account" />}
            </div>
        );
    }

    _renderBackup() {
        let passwordAccount = AccountStore.getState().passwordAccount;
        return (
            <div className="backup-submit">
                <p className="backup-submit__text"><Translate unsafe content="wallet.password_crucial" /></p>
                <section>
                <div>
                   <span className="inline-label">
                    <input
                        type="text" 
                        className="contract-description" 
                        style={{width: "100%", fontWeight: "bold", color: "#fe4647"}} 
                        value={this.state.generatedPassword}/>
                    <CopyButton
                        text={this.state.generatedPassword}
                        tip="tooltip.copy_password"
                        dataPlace="top"
                    />
                    </span>
                </div>
                </section>
                <div className="divider" />
                <p className="txtlabel warning"><Translate unsafe content="wallet.password_lose_warning" /></p>

                <div style={{width: "100%"}} onClick={() => {this.context.router.push(`account/${passwordAccount}/dashboard`)}} className="button"><Translate content="wallet.ok_done" /></div>
            </div>
        );
    }

    _renderGetStarted() {

        return (
            <div>
                <table className="table">
                    <tbody>

                        <tr>
                            <td><Translate content="wallet.tips_dashboard" />:</td>
                            <td><Link to="/dashboard"><Translate content="header.dashboard" /></Link></td>
                        </tr>

                        <tr>
                            <td><Translate content="wallet.tips_account" />:</td>
                            <td><Link to={`/account/${this.state.accountName}/overview`} ><Translate content="wallet.link_account" /></Link></td>
                        </tr>

                        <tr>
                            <td><Translate content="wallet.tips_deposit" />:</td>
                            <td><Link to="/deposit-withdraw"><Translate content="wallet.link_deposit" /></Link></td>
                        </tr>



                        <tr>
                            <td><Translate content="wallet.tips_transfer" />:</td>
                            <td><Link to="/transfer"><Translate content="wallet.link_transfer" /></Link></td>
                        </tr>

                        <tr>
                            <td><Translate content="wallet.tips_settings" />:</td>
                            <td><Link to="/settings"><Translate content="header.settings" /></Link></td>
                        </tr>
                    </tbody>

                </table>
            </div>
        );
    }

    _renderGetStartedText() {

        return (
            <div>
                <p style={{fontWeight: "bold"}}><Translate content="wallet.congrat" /></p>

                <p><Translate content="wallet.tips_explore_pass" /></p>

                <p><Translate content="wallet.tips_header" /></p>

                <p className="txtlabel warning"><Translate content="wallet.tips_login" /></p>
            </div>
        );
    }

    render() {
        let {step} = this.state;
        // let my_accounts = AccountStore.getMyAccounts();
        // let firstAccount = my_accounts.length === 0;
        return (
            <div className="sub-content">
                <div className="grid-block wrap vertical">
                        {step === 2 ? <p style={{fontWeight: "bold"}}>
                            <Translate content={"wallet.step_" + step} />
                        </p> : null}

                        {step === 3 ? this._renderGetStartedText() : null}

                        {step === 1 ? (
                            <div>
                                {this._renderAccountCreateForm()}
                            </div>
                        ) : step === 2 ? this._renderBackup() :
                                this._renderGetStarted()
                        }


                </div>
            </div>
        );
    }
}

export default connect(CreateNewWalletPassword, {
    listenTo() {
        return [AccountStore];
    },
    getProps() {
        return {};
    }
});
