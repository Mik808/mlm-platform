import React from "react";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import AccountStore from "stores/AccountStore";

var add = require("assets/app_img/plus.png");

class Support extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: AccountStore.getState().currentAccount,
            password: AccountStore.getState().passwordAccount,
            userEmail: "",
            userText: "",
            emailError: "validation.emptyString",
            textareaError: "validation.emptyString",
            userFileName: false
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
    }

    handleInputChange(event) {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            [name]: value
        });
    }

    handleFileChange(e) {
        this.setState({ userFileName: e.target.files[0].name });
    }

    clearForm() {
        this.setState({ userEmail: "" });
        this.setState({ userText: "" });
        this.setState({ userFileName: false });
    }

    handleSubmit(event) {
        event.preventDefault();

        // vallidate form
        if (this.state.userEmail === "") {
            this.setState({ emailError: "validation.emptyEmail" })
            return false;
        }
        const emailRegExp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if (!emailRegExp.test(this.state.userEmail)) {
            this.setState({ emailError: "validation.invalidEmail" })
            return false;
        }

        if (this.state.userText === "") {
            this.setState({ textareaError: "validation.emptyQuestion" });
            return false;
        }

        const form = new FormData;
        form.append("username", this.state.userName);
        form.append("useremail", this.state.userEmail);
        form.append("userquestion", this.state.userText);
        if (this.state.userFileName) {
            form.append('userfile', this.refs.fileInput.files[0], this.refs.fileInput.files[0].name);
        }


        // for (var pair of form.entries()) {
        //     console.log(pair[0] + ', ' + pair[1]);
        // }

        fetch('https://ntz.team/mail', {
            method: 'POST',
            body: form,
        })
            .then(data => {
                console.log('Success:', data);
                this.clearForm();
                alert("Обращение отправлено");
            })
            .catch((error) => {
                console.error("data is not sent, error");
                console.error('Error:', error);
                alert("Ошибка отправки");
            });
    }

    render() {
        return (
            <div>
                {this.state.password ? (
                    <div>
                        <Translate className="support__title" component="h3" content="support.feedback" />
                        <form className="support__form" onSubmit={this.handleSubmit}>
                            <div className="support__input-block">
                                <Translate className="support__label" component="label" content="support.account_name" />
                                <input type="text" name="userName" value={this.state.userName} onChange={this.handleInputChange} />
                            </div>
                            <div className="support__input-block">
                                <Translate className="support__label" component="label" content="support.email" />
                                <input type="text" name="userEmail" value={this.state.userEmail} onChange={this.handleInputChange} />
                                <Translate className="support__email-error" component="span" content={this.state.emailError} />
                            </div>
                            <div className="support__input-block">
                                <Translate className="support__label" component="label" content="support.question" />
                                <textarea id="story" name="story" rows="5" cols="33" name="userText" value={this.state.userText} onChange={this.handleInputChange}></textarea>
                                <Translate className="support__email-error" component="span" content={this.state.textareaError} />
                            </div>
                            <label className="support__upload">
                                <label className="support__label">{this.state.userFileName ? this.state.userFileName : counterpart.translate("support.attach_file")}</label>
                                <img style={{ width: 30, height: 30 }} src={add} />
                                <input type="file" name="userfile" ref="fileInput" onChange={this.handleFileChange} />
                            </label>
                            <button className="support__submit" >
                                <Translate component="span" content="chat.send" />
                            </button>
                        </form>
                    </div>
                )
                    : (<Translate component="div" content="support.log_in" />)

                }
            </div>
        )
    }
}

export default Support;