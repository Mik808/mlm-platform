import React from "react";
import Translate from "react-translate-component";
import {Link} from "react-router/es";
import AccountStore from "stores/AccountStore";
import WalletDb from "stores/WalletDb";
import WalletUnlockActions from "actions/WalletUnlockActions";
import AccountActions from "actions/AccountActions";


var accountBack = require("assets/app_img/back-arrow.png");
let width = window.innerWidth;


class AccountBackLink extends React.Component {

    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            locked: WalletDb.isLocked()
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        return (
            this.props.account !== nextProps.account ||
            this.props.wallet_locked !== nextProps.wallet_locked ||
            nextState.locked !== this.state.locked
        )
    }
    _accountUnlock(e) {
        e.preventDefault();
        let currentAccount = AccountStore.getState().currentAccount;

        WalletUnlockActions.unlock().then(() => {
            AccountActions.tryToSetCurrentAccount();
            if (width > 600) {
                this.context.router.push(`/account/${currentAccount}/dashboard`);
            }else {
                this.context.router.push(`mobile/account/${currentAccount}/dashboard`)
            }
            this.setState({
                locked: WalletDb.isLocked()
            });
        })
        
    }

    render() {
        let currentAccount = AccountStore.getState().currentAccount;
        let accountLink;
        
        if(WalletDb.isLocked()) {
            accountLink = <Link onClick={this._accountUnlock.bind(this)}>
                <img style={{width: 37, height: 37}} src={accountBack}/>
                <span className="user-back__text-wrap">
                    <Translate className="user-back__link" component="span"  content="header.account"/>
                </span>  
            </Link>
        }
        else {
            if(width > 600) { 
                accountLink = <Link to={`/account/${currentAccount}/dashboard`}>
                    <img style={{width: 37, height: 37}} src={accountBack} />
                    <span className="user-back__text-wrap">
                        <Translate className="user-back__link" component="span"  content="header.account"/>
                    </span>
                </Link>
            } 
            if(width < 600) {
                accountLink = <Link to={`mobile/account/${currentAccount}/dashboard`}>
                    <img style={{width: 37, height: 37}} src={accountBack} />
                    <span className="user-back__text-wrap">
                        <Translate className="user-back__link" component="span"  content="header.account"/>
                    </span>
                </Link>
            }
        }

        return (
            <div className="user-back">
                {accountLink}
            </div>
        );
    }
}

export default AccountBackLink;