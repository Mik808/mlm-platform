import React from "react";
import Translate from "react-translate-component";
import FormattedAsset from "../Utility/FormattedAsset";
import { ChainStore } from "bitsharesjs/es";
import WalletActions from "actions/WalletActions";
import { Apis } from "bitsharesjs-ws";
import AccountActions from "actions/AccountActions";
import counterpart from "counterpart";
import ls from "common/localStorage";

const STORAGE_KEY = "__graphene__";
let ss = new ls(STORAGE_KEY);

var gift = require("assets/app_img/gift.png");

class VestingBalance extends React.Component {

    _onClaim(claimAll, e) {
        e.preventDefault();
        WalletActions.claimVestingBalance(this.props.account.id, this.props.vb, claimAll);
    }

    render() {
        let { vb } = this.props;
        if (!this.props.vb) {
            return null;
        }

        let cvbAsset, vestingPeriod, earned, secondsPerDay = 60 * 60 * 24,
            availablePercent, balance;
        if (vb) {
            balance = vb.balance.amount;
            cvbAsset = ChainStore.getAsset(vb.balance.asset_id);
            earned = vb.policy[1].coin_seconds_earned;
            vestingPeriod = vb.policy[1].vesting_seconds;
            availablePercent = vestingPeriod === 0 ? 1 : earned / (vestingPeriod * balance);
        }

        if (!cvbAsset) {
            return null;
        }

        if (!balance) {
            return null;
        }

        return (
            <div style={{ paddingBottom: "1rem" }}>
                <div className="grid-content no-padding partnership__table-wrap">
                    <Translate component="h5" content="account.vesting.balance_number" />

                    <table className="table key-value-table partnership__table">
                        <tbody>
                            <tr>
                                <td><Translate content="account.member.cashback" /> </td>
                                <td><FormattedAsset amount={vb.balance.amount} asset={vb.balance.asset_id} /></td>
                            </tr>
                        </tbody>
                    </table>
                    <button onClick={this._onClaim.bind(this, false)} className="settings__btn-ok"><Translate content="account.member.claim" /></button>
                </div>
            </div>
        );
    }
}

var chip = require("assets/app_img/chip-icon.png");
var telegram = require("assets/app_img/telegram-icon.png");

class AccountVesting extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            vbs: null,
            locale: ss.get("settings_v3").locale,
            currentAccount: this.props.account.get('name')
        };
    }


    componentWillMount() {
        this.retrieveVestingBalances.call(this, this.props.account.get("id"));
    }


    componentWillUpdate(nextProps) {
        let newId = nextProps.account.get("id");
        let oldId = this.props.account.get("id");

        if (newId !== oldId) {
            this.retrieveVestingBalances.call(this, newId);
        }
    }

    retrieveVestingBalances(accountId) {
        Apis.instance().db_api().exec("get_vesting_balances", [
            accountId
        ]).then(vbs => {
            this.setState({ vbs });
        }).catch(err => {
            console.log("error:", err);
        });
    }

    upgradeStatusAccount(id, statusType, e) {
        e.preventDefault();
        // let statusPrice = [0, 600000000, 3000000000, 6000000000];
        // let currentStatusPrice = statusPrice[statusType];
        let currentAccount = this.state.currentAccount;

        if (this.state.locale == "ru") {
            window.location = `https://statusesru.ntz.team/?r=${currentAccount}`
        }
        else if (this.state.locale == "ua") {
            window.location = `https://statusesua.ntz.team/?ntz_login=${currentAccount}`
        }
        else {
            window.location = `https://statuses.ntz.team/?ntz_login=${currentAccount}`
        }

        // Apis.instance().db_api().exec("get_account_balances", [
        //     id,
        //     ['1.3.0']]).then(userBalance => {
        //         if (userBalance[0]["amount"] >= currentStatusPrice) {
        //             AccountActions.upgradeStatusAccount(id, statusType);
        //         }
        //         else {
        //             if (this.state.locale == "ru") {
        //                 window.location = `https://statusesru.ntz.team/?r=${currentAccount}`
        //             }
        //             else if (this.state.locale == "ua") {
        //                 window.location = `https://statusesua.ntz.team/?ntz_login=${currentAccount}`
        //             }
        //             else {
        //                 window.location = `https://statuses.ntz.team/?ntz_login=${currentAccount}`
        //             }

        //         }
        //     }).catch(err => {
        //         console.log("error:", err);
        //     });
    }

    // renderGiftBtn(locale) {
    //     switch (locale) {
    //         case "ua":
    //             return (
    //                 <a href={`https://statusesua.ntz.team/?ntz_login=${this.state.currentAccount}`} className="partnership__gift-btn" target="_blank">
    //                     <img style={{ width: 25, height: 25 }} src={gift} />
    //                     <Translate content="account.vesting.get-set-btn" />
    //                 </a>
    //             )
    //         case "ru":
    //             return (
    //                 <a href={`https://quickstart.ntz.team/?r=${this.state.currentAccount}`} className="partnership__gift-btn" target="_blank">
    //                     <img style={{ width: 25, height: 25 }} src={gift} />
    //                     <Translate content="account.vesting.get-set-btn" />
    //                 </a>
    //             )
    //         default:
    //             return (
    //                 <a href={`https://statuses.ntz.team/?ntz_login=${this.state.currentAccount}`} className="partnership__gift-btn" target="_blank">
    //                     <img style={{ width: 25, height: 25 }} src={gift} />
    //                     <Translate content="account.vesting.get-set-btn" />
    //                 </a>
    //             )
    //     }
    // }

    render() {
        let mobile = window.innerWidth;
        let { vbs } = this.state;
        if (!vbs || !this.props.account || !this.props.account.get("vesting_balances")) {
            return null;
        }

        let account = this.props.account.toJS();

        let balances = vbs.map(vb => {
            if (vb.balance.amount) {
                return <VestingBalance key={vb.id} vb={vb} account={account} />;
            }
        }).filter(a => {
            return !!a;
        });

        let referalStatus = this.props.account.get("referral_status_type");
        let stDate = new Date();
        let statuses = [
            counterpart.translate("statuses.status_01"),
            counterpart.translate("statuses.status_02"),
            counterpart.translate("statuses.status_03"),
            counterpart.translate("statuses.status_04")
        ];
        let statusExpirationDateRaw = this.props.account.get("referral_status_expiration_date");

        if (statusExpirationDateRaw) {
            var myStdate = new Date(statusExpirationDateRaw + "Z");
            var statusExpirationDate = myStdate.toLocaleString();
        }

        let upgradeBtn;

        upgradeBtn = (<button className="acc-btn__inner partnership__btn" onClick={this.upgradeStatusAccount.bind(this, account.id, this.props.account.get("referral_status_type"))}><Translate content="account.vesting.extend" /></button>)

        let locale = this.state.locale;
        let currentAccount = this.state.currentAccount;

        return (
            <div className="partnership">
                <div className="partnership__wrap">

                    <section className="partnership__info">
                        <div className="partnership__inner">
                            <Translate className="partnership__subtitle" content="account.vesting.current-status" component="h3" />
                            <span className="partnership__subtitle partnership__subtitle--current">{statuses[referalStatus]}</span>
                        </div>

                        {!referalStatus == 0 ?
                            <div className="partnership__date-block">
                                <div className="partnership__inner partnership__inner--date">
                                    <Translate className="partnership__subtitle" content="account.vesting.expiration" component="h3" />
                                    <span className="partnership__subtitle partnership__subtitle--current">{statusExpirationDate}</span>
                                </div>
                                {upgradeBtn}
                            </div>
                            : null}
                        {referalStatus == 0 ? null :
                            <a className="partnership__chat" href={mobile < 600 ? "https://t.me/joinchat/AmNxAVMda7fWpA4sc5ZVig" : "https://tgme.pro//joinchat/AmNxAVMda7fWpA4sc5ZVig"} target="_blank">
                                <img className="partnership__img" src={telegram} width={30} height={30} />
                                <Translate className="partnership__link" component="span" content="account.vesting.telegram" />
                            </a>}
                        {referalStatus == 3 ? null :
                            <Translate className="partnership__header" content="account.vesting.upgrade" component="h3" />}

                        <ul className="partnership-card__list">
                            {referalStatus == 1 || referalStatus == 2 || referalStatus == 3 || mobile < 600 ? null :
                                <li className="partnership-card__item partnership-card__item--standart" onClick={this.upgradeStatusAccount.bind(this, account.id, 1)}>
                                    <div className="partnership-card__inner">
                                        <img src={chip} />
                                        <Translate className="partnership-card__title" component="span" content="account.vesting.partner1" />
                                    </div>
                                    <span className="partnership-card__price">{locale === "ru" ? "6 600" : "6 000"} NTZ</span>
                                </li>}
                            {(referalStatus == 2 || referalStatus == 3 || mobile < 600) ? null :
                                <li className="partnership-card__item partnership-card__item--business" onClick={this.upgradeStatusAccount.bind(this, account.id, 2)}>
                                    <div className="partnership-card__inner">
                                        <img src={chip} />
                                        <Translate className="partnership-card__title" component="span" content="account.vesting.partner2" />
                                    </div>
                                    <span className="partnership-card__price">{locale === "ru" ? "33 000" : "30 000"} NTZ</span>
                                </li>}
                            {referalStatus == 3 || mobile < 600 ? null :
                                <li className="partnership-card__item partnership-card__item--vip" onClick={this.upgradeStatusAccount.bind(this, account.id, 3)}>
                                    <div className="partnership-card__inner">
                                        <img src={chip} />
                                        <Translate className="partnership-card__title partnership-card__title--vip" component="span" content="account.vesting.partner3" />
                                    </div>
                                    <span className="partnership-card__price partnership-card__price--vip">{locale === "ru" ? "66 000" : "60 000"} NTZ</span>
                                </li>}
                        </ul>

                        {/*GIFT SET*/}
                        {/* {referalStatus < 3 ?
                            <div className="partnership__gift-wrap">
                                <Translate className="partnership__gift-text" content="account.vesting.get-set-text" component="p" />
                                {this.renderGiftBtn(locale)}
                            </div>
                            : null
                        } */}

                        {/*STATUS DETAIL*/}
                        {referalStatus == 3 || mobile < 600 ? null :
                            <table className="partnership-detail" cellSpacing="0">
                                <tbody>
                                    <tr className="partnership-detail__header">
                                        <th className="partnership-detail__info"></th>
                                        <th><Translate content="account.vesting.partner1" /></th>
                                        <th><Translate content="account.vesting.partner2" /></th>
                                        <th><Translate content="account.vesting.partner3" /></th>
                                    </tr>
                                    <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.price" /></td>
                                        <td>{locale === "ru" ? "6 600" : "6 000"} NTZ</td>
                                        <td>{locale === "ru" ? "33 000" : "30 000"} NTZ</td>
                                        <td>{locale === "ru" ? "66 000" : "60 000"} NTZ</td>
                                    </tr>
                                    <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.evaliable-ref-levels" /></td>
                                        <td>1-3</td>
                                        <td>1-6</td>
                                        <td>1-8</td>
                                    </tr>
                                    <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.reward" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                    </tr>
                                    <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.personal-activity" /></td>
                                        <td>5 000 NTZ</td>
                                        <td>3 500 NTZ</td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.disabled" /></td>
                                    </tr>
                                    <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.leader-reward" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.disabled" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                    </tr>
                                    <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.compression" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        <td><Translate className="partnership-detail__text" component="span" content="account.vesting.disabled" /></td>
                                    </tr>
                                    {/* <tr>
                                        <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.bonus" /></td>
                                        <td>{locale === "ru" ? "6 600" : "6 000"} NTL</td>
                                        <td>{locale === "ru" ? "33 000" : "30 000"} NTL</td>
                                        <td>{locale === "ru" ? "66 000" : "60 000"} NTL</td>
                                    </tr> */}
                                </tbody>
                            </table>}

                        {referalStatus == 1 || referalStatus == 2 || referalStatus == 3 || mobile > 600 ? null :
                            <div className="partnership-detail__table-wrap">
                                <div className="partnership-card__item partnership-card__item--standart" onClick={this.upgradeStatusAccount.bind(this, account.id, 1)}>
                                    <div className="partnership-card__inner">
                                        <img src={chip} />
                                        <Translate className="partnership-card__title" component="span" content="account.vesting.partner1" />
                                    </div>
                                    <span className="partnership-card__price">{locale === "ru" ? "6 600" : "6 000"} NTZ</span>
                                </div>
                                <table className="partnership-detail partnership-detail--standart" cellSpacing="0">
                                    <tbody>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.price" /></td>
                                            <td>{locale === "ru" ? "6 600" : "6 000"} NTZ</td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.evaliable-ref-levels" /></td>
                                            <td>1-3</td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.reward" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.personal-activity" /></td>
                                            <td>5 000 NTZ</td>

                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.leader-reward" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.compression" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        {/* <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.bonus" /></td>
                                            <td>{locale === "ru" ? "6 600" : "6 000"} NTL</td>
                                        </tr> */}
                                    </tbody>
                                </table>
                                <button className="partnership__btn partnership__btn--stand" onClick={this.upgradeStatusAccount.bind(this, account.id, 1)}><Translate content="account.vesting.сhoose" /></button>
                            </div>
                        }
                        {referalStatus == 2 || referalStatus == 3 || mobile > 600 ? null :
                            <div className="partnership-detail__table-wrap">
                                <div className="partnership-card__item partnership-card__item--business" onClick={this.upgradeStatusAccount.bind(this, account.id, 2)}>
                                    <div className="partnership-card__inner">
                                        <img src={chip} />
                                        <Translate className="partnership-card__title" component="span" content="account.vesting.partner2" />
                                    </div>
                                    <span className="partnership-card__price">33 000 NTZ</span>
                                </div>
                                <table className="partnership-detail partnership-detail--business" cellSpacing="0">
                                    <tbody>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.price" /></td>
                                            <td>{locale === "ru" ? "33 000" : "30 000"} NTZ</td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.evaliable-ref-levels" /></td>
                                            <td>1-6</td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.reward" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.personal-activity" /></td>
                                            <td>3 500 NTZ</td>

                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.leader-reward" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.compression" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        {/* <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.bonus" /></td>
                                            <td>{locale === "ru" ? "33 000" : "30 000"} NTL</td>
                                        </tr> */}
                                    </tbody>
                                </table>
                                <button className="partnership__btn partnership__btn--business" onClick={this.upgradeStatusAccount.bind(this, account.id, 2)}><Translate content="account.vesting.сhoose" /></button>
                            </div>
                        }
                        {referalStatus == 3 || mobile > 600 ? null :
                            <div className="partnership-detail__table-wrap">
                                <div className="partnership-card__item partnership-card__item--vip" onClick={this.upgradeStatusAccount.bind(this, account.id, 3)}>
                                    <div className="partnership-card__inner">
                                        <img src={chip} />
                                        <Translate className="partnership-card__title partnership-card__title--vip" component="span" content="account.vesting.partner3" />
                                    </div>
                                    <span className="partnership-card__price partnership-card__price--vip">{locale === "ru" ? "66 000" : "60 000"} NTZ</span>
                                </div>
                                <table className="partnership-detail partnership-detail--vip" cellSpacing="0">
                                    <tbody>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.price" /></td>
                                            <td>{locale === "ru" ? "66 000" : "60 000"} NTZ</td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.evaliable-ref-levels" /></td>
                                            <td>1-8</td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.reward" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.personal-activity" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.disabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.leader-reward" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.enabled" /></td>
                                        </tr>
                                        <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.compression" /></td>
                                            <td><Translate className="partnership-detail__text" component="span" content="account.vesting.disabled" /></td>
                                        </tr>
                                        {/* <tr>
                                            <td className="partnership-detail__info"><Translate className="partnership-detail__text" component="span" content="account.vesting.bonus" /></td>
                                            <td>{locale === "ru" ? "66 000" : "60 000"} NTL</td>
                                        </tr> */}
                                    </tbody>
                                </table>
                                <button className="partnership__btn partnership__btn--vip" onClick={this.upgradeStatusAccount.bind(this, account.id, 3)}><Translate content="account.vesting.сhoose" /></button>
                            </div>
                        }
                    </section>
                </div>
                <div className="partnership__vesting">
                    <Translate className="partnership__header" content="account.vesting.header" component="h3" />
                    <Translate content="account.vesting.explain" component="p" />
                    {!balances.length ? (
                        <h4 style={{ paddingTop: "1rem" }}>
                            <Translate content={"account.vesting.no_balances"} />
                        </h4>) : balances}
                </div>
            </div>
        );
    }
}

AccountVesting.VestingBalance = VestingBalance;
export default AccountVesting;
