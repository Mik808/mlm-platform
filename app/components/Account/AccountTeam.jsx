import React from 'react';
import Translate from "react-translate-component";
import LoadingIndicator from "../LoadingIndicator";
import AccountTeamPartner from "./AccountTeamPartner";
import { color } from 'd3-color';
import { interpolateRgb } from 'd3-interpolate';
import LiquidFillGauge from 'react-liquid-gauge';
import { ChainStore } from "bitsharesjs/es";
import counterpart from "counterpart";

let startColor = '#6495ed';
let endColor = '#dc143c';


class AccountTeam extends React.Component {
    static contextTypes = {
        location: React.PropTypes.object.isRequired,
        router: React.PropTypes.object.isRequired
    };
    constructor(props, context) {
        super(props);

        this.state = {
            active: context.location.pathname,
            userRef: '',
            parous: {},
            partnerData: [],
            currentAccount: this.props.account.get('id'),
            showClients: false,
            findAccount: '',
            value: 50,
            accountObj: ChainStore.getAccount(this.props.account.get('id'))

        };

        this.getStructure = this.getStructure.bind(this);
        this.hideClients = this.hideClients.bind(this);
        this.filterShownAcoounts = this.filterShownAcoounts.bind(this);
        this.showAllAcoounts = this.showAllAcoounts.bind(this);
    }

    componentWillMount() {
        this.getStructure(this.props.account.get('id'));
    }

    componentWillReceiveProps(newProps) {
        if (this.props.account.get('id') != newProps.account.get('id')) {
            this.setState({
                userRef: '',
                parous: {},
                partnerData: []
            },
                () => {
                    this.getStructure(newProps.account.get('id'));
                });
        }
    }

    getStructure(acc_id) {
        if (this.state.showClients) {
            var url = 'https://ntz.team/structure/cache/' + acc_id + '-c.json';
        }
        else {
            var url = 'https://ntz.team/structure/cache/' + acc_id + '-p.json';
        }

        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    parous: data,
                    userRef: data['referrer_name']
                })
                var partnersFilter = this.state.parous['firstline'];
                var partnersName = [];

                if (partnersFilter) {
                    for (var i = 0; i < partnersFilter.length; i++) {
                        partnersName.push(partnersFilter[i]['name']);
                    }
                    this.setState({
                        partnersName: partnersName
                    })
                }
            }
            );
    }

    _onNavigate(route, e) {
        e.preventDefault();
        this.context.router.push(route);
    }

    hideClients() {
        let checked = this.state.showClients;
        if (checked) {
            checked = false
        }
        else {
            checked = true
        }
        this.setState({
            showClients: checked,
            parous: {},
            partnerData: []
        },
            () => {
                this.getStructure(this.props.account.get('id'));
            });
    }

    filterShownAcoounts() {
        let value = this.refs.account_filter.value.trim();
        value = value.toLowerCase();

        if (value !== "") {
            this.setState({
                findAccount: value,
            })
            var elems = document.getElementsByClassName('user-team__item');
            for (var i = 0; i < elems.length; i++) {
                elems[i].classList.add('user-team__item--hide');
            }
            var partnersName = this.state.partnersName;
            var resultarray = partnersName.filter(a => a.includes(value));

            if (this.state.parous['firstline'] && resultarray.length > 0) {
                resultarray.map((item, i) =>
                    document.getElementById(item).classList.remove('user-team__item--hide')
                )
            }
        }
    }

    showAllAcoounts() {
        var elems = document.getElementsByClassName('user-team__item');
        for (var i = 0; i < elems.length; i++) {
            elems[i].classList.remove('user-team__item--hide');
        }
        document.getElementById('accountFilter').value = '';
    }


    render() {
        let partners = [];
        if (this.state.parous['firstline']) {
            partners = this.state.parous['firstline'];
        }
        let statuses = [
            counterpart.translate("statuses.status_01"),
            counterpart.translate("statuses.status_02"),
            counterpart.translate("statuses.status_03"),
            counterpart.translate("statuses.status_04")
        ];
        let userLavels = [
            counterpart.translate("leaders.none"),
            counterpart.translate("leaders.level_01"),
            counterpart.translate("leaders.level_02"),
            counterpart.translate("leaders.level_03"),
            counterpart.translate("leaders.level_04"),
            counterpart.translate("leaders.level_05"),
            counterpart.translate("leaders.level_06"),
            counterpart.translate("leaders.level_07"),
            counterpart.translate("leaders.level_08")
        ];

        const radius = 65;
        const interpolate = interpolateRgb(startColor, endColor);
        const fillColor = interpolate(this.state.value / 100);
        const gradientStops = [
            {
                key: '0%',
                stopColor: color(fillColor).darker(0.5).toString(),
                stopOpacity: 1,
                offset: '0%'
            },
            {
                key: '50%',
                stopColor: fillColor,
                stopOpacity: 0.75,
                offset: '50%'
            },
            {
                key: '100%',
                stopColor: color(fillColor).brighter(0.5).toString(),
                stopOpacity: 0.5,
                offset: '100%'
            }
        ];

        let personalActivity;
        let personalPercent = "%";
        let personalValue = 0;
        let nextLevel = 0;

        if (Math.round(this.state.parous['withdraw_multiplicator'])) {
            personalValue = Math.round(this.state.parous['withdraw_multiplicator'] * 100)
        }
        if (Math.round(this.state.parous['next_level_percent'] * 100)) {
            nextLevel = Math.round(this.state.parous['next_level_percent'] * 100)
        }

        if (this.state.parous['status'] === 3) {
            personalActivity = "♛";
            personalPercent = "";

        }
        else {
            personalActivity = Math.round(this.state.parous['withdraw_multiplicator'] * 100);
        }
        return (

            <div>
                {/*TUTOR*/}
                <div className="user-team__ref-wrap">
                    <Translate className="user-team__my-ref" content="account.tutor" component="span" />
                    <a onClick={this._onNavigate.bind(this, `/account/${this.state.userRef}/my-team/`)} className="user-team__my-ref user-team__my-ref--hightlighted user-team__my-ref--link" >{this.state.userRef}</a>
                </div>

                {/*NAME*/}
                <div className="user-team__ref-wrap">
                    <Translate className="user-team__my-ref" content="account.current-account" component="span" />
                    <span className="user-team__my-ref user-team__my-ref--hightlighted" >{this.state.parous['name']}</span>
                </div>

                {/*LEVEL*/}
                <div className="user-team__level-wrap" >
                    <Translate className="user-team__level" content="account.leader-level" component="span" />
                    <span className="user-team__level user-team__level--hightlighted">{userLavels[this.state.parous['leaders_level']]}</span>
                </div>

                <div className="user-team__info">
                    {/*USER INFO*/}
                    <div className="user-team__info-item user-team__info-item--general">
                        {/*status*/}
                        {this.state.parous['status'] !== 0 ?
                            <div className="user-team__inner user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.status" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{statuses[this.state.parous['status']]}</span>
                            </div>
                            : null}
                        {/*total_members*/}
                        <div className="user-team__inner user-team__inner--current">
                            <span className="user-team__data">
                                <Translate content="account.total-members" component="span" />
                            </span>
                            <span className="user-team__data user-team__data--value">{this.state.parous['total_down']}</span>
                        </div>
                        {/*total_levels*/}
                        <div className="user-team__inner  user-team__inner--current">
                            <span className="user-team__data">
                                <Translate content="account.total-levels" component="span" />
                            </span>
                            <span className="user-team__data user-team__data--value">{this.state.parous['depth']}</span>
                        </div>
                        {/*standart*/}
                        <div className="user-team__inner  user-team__inner--current">
                            <span className="user-team__data">
                                <Translate content="account.partner-num" component="span" />
                            </span>
                            <span className="user-team__data user-team__data--value">{this.state.parous['partner_basic']}</span>
                        </div>
                        {/*bisiness*/}
                        <div className="user-team__inner  user-team__inner--current">
                            <span className="user-team__data">
                                <Translate content="account.partner-plus-num" component="span" />
                            </span>
                            <span className="user-team__data user-team__data--value">{this.state.parous['partner_plus']}</span>
                        </div>
                        {/*vip*/}
                        <div className="user-team__inner  user-team__inner--current">
                            <span className="user-team__data">
                                <Translate content="account.partner-vip-num" component="span" />
                            </span>
                            <span className="user-team__data user-team__data--value">{this.state.parous['partner_vip']}</span>
                        </div>
                        {/*leaders_number*/}
                        <div className="user-team__inner  user-team__inner--current">
                            <span className="user-team__data">
                                <Translate content="account.leaders-number" component="span" />
                            </span>
                            <span className="user-team__data user-team__data--value">{this.state.parous['total_leaders']}</span>
                        </div>
                    </div>

                    {/*CURRENT MONTH*/}
                    {this.state.parous['current_month'] ?
                        <div className="user-team__info-item user-team__info-item--general">
                            <Translate className="user-team__info-title" content="account.stats.current-month" />
                            {/*personal_volume*/}
                            <div className="user-team__inner user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.stats.personal-volume" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['current_month']['personal_volume']}</span>
                            </div>
                            {/*firstline_volume*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.stats.first-line-volume" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['current_month']['firstline_volume']}</span>
                            </div>
                            {/*structure_volume*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.stats.structure-volume" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['current_month']['structure_volume']}</span>
                            </div>
                            {/*new_standart*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.partner-num" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['current_month']['new_standart']}</span>
                            </div>
                            {/*new_business*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.partner-plus-num" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['current_month']['new_business']}</span>
                            </div>
                            {/*new_vip*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.partner-vip-num" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['current_month']['new_vip']}</span>
                            </div>
                        </div>
                        : null}

                    {/*PREV MONTH*/}
                    {this.state.parous['previous_month'] ?
                        <div className="user-team__info-item user-team__info-item--general">
                            <Translate className="user-team__info-title" content="account.stats.last-month" />
                            {/*personal_volume*/}
                            <div className="user-team__inner user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.stats.personal-volume" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['previous_month']['personal_volume']}</span>
                            </div>
                            {/*firstline_volume*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.stats.first-line-volume" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['previous_month']['firstline_volume']}</span>
                            </div>
                            {/*structure_volume*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.stats.structure-volume" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['previous_month']['structure_volume']}</span>
                            </div>
                            {/*new_standart*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.partner-num" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['previous_month']['new_standart']}</span>
                            </div>
                            {/*new_business*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.partner-plus-num" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['previous_month']['new_business']}</span>
                            </div>
                            {/*new_vip*/}
                            <div className="user-team__inner  user-team__inner--current">
                                <span className="user-team__data">
                                    <Translate content="account.partner-vip-num" component="span" />
                                </span>
                                <span className="user-team__data user-team__data--value">{this.state.parous['previous_month']['new_vip']}</span>
                            </div>
                        </div>
                        : null}

                    <div className="user-team__info-item user-team__info-item--indicator">
                        <LiquidFillGauge
                            style={{ margin: '0 auto' }}
                            width={radius * 2}
                            height={radius * 2}


                            value={personalValue}
                            percent={personalPercent}
                            textSize={1}
                            textOffsetX={0}
                            textOffsetY={0}
                            textRenderer={(props) => {
                                const value = personalActivity;
                                const radius = Math.min(props.height / 2, props.width / 2);
                                const textPixels = (props.textSize * radius / 2);
                                const valueStyle = {
                                    fontSize: textPixels
                                };
                                const percentStyle = {
                                    fontSize: textPixels * 0.6
                                };

                                return (
                                    <tspan>
                                        <tspan className="value" style={valueStyle}>{value}</tspan>
                                        <tspan style={percentStyle}>{props.percent}</tspan>
                                    </tspan>
                                );
                            }}
                            riseAnimation
                            waveAnimation
                            waveFrequency={2}
                            waveAmplitude={1}
                            gradient
                            gradientStops={gradientStops}
                            circleStyle={{
                                fill: fillColor
                            }}
                            waveStyle={{
                                fill: fillColor
                            }}
                            textStyle={{
                                fill: color('#2d3135').toString(),
                                fontFamily: 'Roboto'
                            }}
                            waveTextStyle={{
                                fill: color('#2d3135').toString(),
                                fontFamily: 'Roboto'
                            }}
                        />
                        <div className="user-team__indicator-data">
                            <Translate content="account.personal-activity-indicator" component="span" />
                            {this.state.parous['status'] === 3 ?
                                <span>&infin;</span>
                                :
                                <span>{Math.round(this.state.parous['withdraw_multiplicator'] * 100)}%</span>
                            }
                        </div>
                    </div>

                    <div className="user-team__info-item user-team__info-item--indicator">
                        <LiquidFillGauge
                            style={{ margin: '0 auto' }}
                            width={radius * 2}
                            height={radius * 2}
                            value={nextLevel}
                            percent="%"
                            textSize={1}
                            textOffsetX={0}
                            textOffsetY={0}
                            textRenderer={(props) => {
                                const value = Math.round(props.value);
                                const radius = Math.min(props.height / 2, props.width / 2);
                                const textPixels = (props.textSize * radius / 2);
                                const valueStyle = {
                                    fontSize: textPixels
                                };
                                const percentStyle = {
                                    fontSize: textPixels * 0.6
                                };

                                return (
                                    <tspan>
                                        <tspan className="value" style={valueStyle}>{value}</tspan>
                                        <tspan style={percentStyle}>{props.percent}</tspan>
                                    </tspan>
                                );
                            }}
                            riseAnimation
                            waveAnimation
                            waveFrequency={2}
                            waveAmplitude={1}
                            gradient
                            gradientStops={gradientStops}
                            circleStyle={{
                                fill: fillColor
                            }}
                            waveStyle={{
                                fill: fillColor
                            }}
                            textStyle={{
                                fill: color('#2d3135').toString(),
                                fontFamily: 'Roboto'
                            }}
                            waveTextStyle={{
                                fill: color('#2d3135').toString(),
                                fontFamily: 'Roboto'
                            }}
                        />
                        <div className="user-team__indicator-data">
                            <Translate content="account.next-leader-level-indicator" component="span" />
                            <span>{100 - Math.round(this.state.parous['next_level_percent'] * 100)}%</span>
                        </div>
                    </div>
                </div>

                <Translate className="user-team__header" content="account.first-line" component="h2" />

                {this.state.parous['total_down'] > 0 ?

                    <div>

                        {/*SEARCH BLOCK*/}
                        <div className="user-team__search-wrap">
                            <input
                                className="user-team__acc-filter"
                                type="text"
                                name="accountFilter"
                                id="accountFilter"
                                placeholder={counterpart.translate("account.show-partner")}
                                ref="account_filter"
                                onChange={this.filterShownAcoounts.bind(this)}
                            />
                            <div className="user-team__cancel-btn" onClick={this.showAllAcoounts.bind(this)}><Translate content="account.show-all-acc" /></div>
                        </div>
                        {/*SEARCH BLOCK*/}


                        <input className="user-team__checkbox" type="checkbox" id="hideClients" name="hideClients" onClick={this.hideClients.bind(this)} defaultChecked={this.state.showClients}></input>

                        <label className="user-team__toggler" htmlFor="hideClients">
                            <Translate className="user-team__text" content="account.hide-clients" component="span" />
                        </label>
                        <ul className="user-team__list">
                            {partners.map(partner =>
                                <AccountTeamPartner
                                    key={partner.id}
                                    partnerData={partner}
                                    account={this.state.accountObj}
                                />)}
                        </ul>
                    </div>
                    : null
                }

                {
                    this.state.parous && this.state.parous['total_down'] == 0 ?
                        <Translate className="user-team__subheader" content="account.empty-first-line" component="h2" />
                        : null
                }

                {
                    this.state.parous['id'] ?
                        null
                        : <div style={{ textAlign: "center" }}>
                            <Translate className="centerDiv" content="loading.indicator" component="div" style={{ marginBottom: "40px", fontSize: "70px" }} />
                            <LoadingIndicator type="three-bounce" />
                        </div>
                }
            </div >
        );
    }
}

export default AccountTeam;