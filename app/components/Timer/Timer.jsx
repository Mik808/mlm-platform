import React from 'react';
import Translate from "react-translate-component";

class Timer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            time: {},
            seconds: Date.parse('February 1, 2021, 21:00:00 GMT') - new Date().getTime()
        };

        this.deadline = Date.parse('January 10, 2020');
        this.timer = 0;
        this.countDown = this.countDown.bind(this);
    }

    getTime(date) {
        let t = date;
        let seconds = Math.floor((t / 1000) % 60);
        let minutes = Math.floor((t / 1000 / 60) % 60);
        let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        let days = Math.floor(t / (1000 * 60 * 60 * 24));
        let timeObj = {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        }

        // console.log("timeObj", timeObj);
        return timeObj;
    }

    componentDidMount() {
        let timeLeft = this.getTime(this.state.seconds);
        this.setState({ time: timeLeft });

        if (this.timer == 0 && this.state.seconds > 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1000;
        this.setState({
            time: this.getTime(seconds),
            seconds: seconds,
        });
        // console.log("seconds", seconds);

        // Check if we're at zero.
        if (seconds < 1000 || seconds == 1000 ) {
            // console.log("0 seconds", this.state.seconds);
            clearInterval(this.timer);
            // this.setState({ seconds: ?? }); // set milliseconds to one week
        }
    }

    render() {
        // console.log("this.state", this.state);

        return (
            <div className="timer">
                <div className="timer__header">
                    <Translate content="timer.in_time" component="div" className="timer__title" />
                    <Translate content="timer.end" component="div" className="timer__sub-title" />
                </div>
                <div className="timer__inner">
                    <div className="timer__block">
                        <div className="timer__num">{this.state.time.days}</div>
                        <Translate content="timer.days" component="div" className="timer__text" />
                    </div>
                    <div className="timer__block">
                        <div className="timer__num">{this.state.time.hours}</div>
                        <Translate content="timer.hours" component="div" className="timer__text" />
                    </div>
                    <div className="timer__block">
                        <div className="timer__num">{this.state.time.minutes}</div>
                        <Translate content="timer.minutes" component="div" className="timer__text" />
                    </div>
                    <div className="timer__block">
                        <div className="timer__num">{this.state.time.seconds}</div>
                        <Translate content="timer.seconds" component="div" className="timer__text" />
                    </div>
                </div>

            </div>
        );
    }
}

export default Timer;