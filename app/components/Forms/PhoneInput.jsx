import React from 'react';
import InputMask from 'react-input-mask';
import Translate from "react-translate-component";
import counterpart from "counterpart";

class PhoneInput extends React.Component {

    constructor(props) {
        super(props);

        this.prefixes = [
            { country: "Russia", prefix: "+7", mask: "999 999 99 99" },
            { country: "Belarus", prefix: "+375", mask: "99 999 99 99" },
            { country: "Kazakhstan", prefix: "+7", mask: "999 999 99 99" },
            { country: "Ukraine", prefix: "+380", mask: "99 9999999" }
        ]

        this.state = {
            mask: "999 999 99 99",
            prefix: "",
        }

        this.handlePrefixSelect = this.handlePrefixSelect.bind(this);
    }

    handlePrefixSelect(e) {
        const country = this.prefixes[this.prefixes.findIndex(el => el.country === e.target.value)];
        this.setState({
            mask: country.mask,
            prefix: country.prefix
        });
    }

    render() {
        return (
            <div className="phone-input">
                <Translate className="checkout__input-label" component="label" content="checkout.phone" />
                <div className="phone-input__input-block">
                    <select name="" id="" onChange={this.handlePrefixSelect} className="phone-input__select">
                        {this.prefixes.map((el, i) => {
                            return <option key={i} value={el.country}>{`${el.prefix} ${counterpart.translate(`countries.${el.country}`)}`}</option>
                        })}
                    </select>
                    <InputMask mask={this.state.mask} maskChar="X" className="ntz-input" type="tel" name="phone" data={this.state.prefix} value={this.state.phone} onChange={this.props.getValue} required />
                </div>
            </div>
        )
    }
}

export default PhoneInput;