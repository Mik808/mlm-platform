import alt from "alt-instance";

class CartActions { 

    addProduct(product) {
        return product;
    }

    removeProduct(product) {
        return product.id;
    }

    addQuantity(id) {
        return id;
    }

    removeQuantity(id) {
        return id;
    }

    clearCart() {
        // console.log("clearCart action fired");
        return "";
    }

    setDeliveryPrice(price) {
        return price;
    }

    updateTaxDeliveryTotal() {
        // console.log("updateTaxDeliveryTotal action fired");
        return "";
    }

    setReferrer(referrer) {
        return referrer;
    }
}

export default alt.createActions(CartActions);