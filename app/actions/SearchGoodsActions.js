import alt from "alt-instance";

class SearchGoodsActions { 

    updateSearchResults(searchResults) {
        return searchResults;
      }

}

export default alt.createActions(SearchGoodsActions);