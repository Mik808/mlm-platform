import React from "react";

import { Router, Route, IndexRoute, browserHistory, hashHistory, Redirect } from "react-router/es";
import willTransitionTo from "./routerTransition";
import App from "./App";

// Components imported here for react hot loader (does not work with async route loading)
import Index from "./components/Index/Index";
import IndexMain from "./components/IndexMain/IndexMain";
import Registration from "./components/Index/Registration";
import DashboardContainer from "./components/Dashboard/DashboardContainer";
import Witnesses from "./components/Explorer/Witnesses";
import CommitteeMembers from "./components/Explorer/CommitteeMembers";
import FeesContainer from "./components/Blockchain/FeesContainer";
import BlocksContainer from "./components/Explorer/BlocksContainer";
import AssetsContainer from "./components/Explorer/AssetsContainer";
import AccountsContainer from "./components/Explorer/AccountsContainer";
import Explorer from "components/Explorer/Explorer";

import AccountPage from "./components/Account/AccountPage";
import AccountPageMobile from "./components/Account/AccountPageMobile";

import AccountOverview from "./components/Account/AccountOverview";
import AccountAssets from "./components/Account/AccountAssets";
import { AccountAssetCreate } from "./components/Account/AccountAssetCreate";
import AccountAssetUpdate from "./components/Account/AccountAssetUpdate";
import AccountVesting from "./components/Account/AccountVesting";
import AccountPermissions from "./components/Account/AccountPermissions";
import AccountWhitelist from "./components/Account/AccountWhitelist";
import AccountVoting from "./components/Account/AccountVoting";
import AccountOrders from "./components/Account/AccountOrders";
import AccountTeam from "./components/Account/AccountTeam";

import PaymentForm from "./components/Account/PaymentForm";
import ExchangeContainer from "./components/Exchange/ExchangeContainer";
import MarketsContainer from "./components/Exchange/MarketsContainer";
import Transfer from "./components/Transfer/Transfer";
import SettingsContainer from "./components/Settings/SettingsContainer";
import BlockContainer from "./components/Blockchain/BlockContainer";
import AssetContainer from "./components/Blockchain/AssetContainer";
import CreateAccount from "./components/Account/CreateAccount";
import CreateNewWalletPassword from "./components/Account/CreateNewWalletPassword";
import CreateAccountPassword from "./components/Account/CreateAccountPassword";
import { ExistingAccount, ExistingAccountOptions } from "./components/Wallet/ExistingAccount";
import { WalletCreate, CreateWalletFromBrainkey } from "./components/Wallet/WalletCreate";
import ImportKeys from "./components/Wallet/ImportKeys";
import { BackupCreate, BackupRestore } from "./components/Wallet/Backup";
import WalletChangePassword from "./components/Wallet/WalletChangePassword";
import { WalletManager, WalletOptions, ChangeActiveWallet, WalletDelete } from "./components/Wallet/WalletManager";
import BalanceClaimActive from "./components/Wallet/BalanceClaimActive";
import BackupBrainkey from "./components/Wallet/BackupBrainkey";
import Brainkey from "./components/Wallet/Brainkey";
import InitError from "./components/InitError";
import LoginSelector from "./components/LoginSelector";
import CreateWorker from "./components/Account/CreateWorker";

import DEXgateWay from "./components/DEXgateWay/DEXgateWay";
import BTCGateWay from "./components/BTCGateWay/BTCGateWay";

import MarketPlace from "./components/MarketPlace/MarketPlace";
import Promo from "./components/Promo/Promo";
import InvoiceList from "./components/Invoice/InvoiceList";
import InvoiceDetail from "./components/Invoice/InvoiceDetail";

import Events from "./components/Tickets/Events";
import Assembly from "./components/Tickets/Assembly";
import Training from "./components/Tickets/Training";

import Support from "./components/Account/Support";
import About from "./components/About/About";
import ProductPage from "./components/Product/ProductPage"
import Cart from "./components/Cart/Cart";
import Checkout from "./components/Checkout/Checkout";
import SearchResults from "./components/Search/SearchResults";
import CategoryPage from "./components/Categories/CategoryPage";
import ThankYouPage from "./components/ThankYouPage/ThankYouPage";

const history = __HASH_HISTORY__ ? hashHistory : browserHistory;

class Auth extends React.Component {
    render() { return null; }
}

const routes = (
    <Route path="/" component={App} onEnter={willTransitionTo}>
        <IndexRoute component={IndexMain} />
        <Route path="/home" component={IndexMain} />
        <Route path="registration" component={Registration} />
        <Route path="/auth/:data" component={Auth} />
        <Route path="/dashboard" component={DashboardContainer} />
        <Route path="explorer" component={Explorer} />
        <Route path="/explorer/fees" component={FeesContainer} />
        <Route path="/explorer/blocks" component={BlocksContainer} />
        <Route path="/explorer/assets" component={AssetsContainer} />
        <Route path="/explorer/accounts" component={AccountsContainer} />
        <Route path="/explorer/witnesses" component={Witnesses} />
        <Route path="/explorer/committee-members" component={CommitteeMembers} />
        <Route path="/about/:section" component={About} />  
        <Route path="/product/:id" component={ProductPage} />  
        <Route path="/cart" component={Cart} /> 
        <Route path="/checkout" component={Checkout} />
        <Route path="/search" component={SearchResults} /> 
        <Route path="/category/:name" component={CategoryPage} />  
        <Route path="/success" component={ThankYouPage} />  

        <Route path="wallet" component={WalletManager} >
            {/* wallet management console */}
            <IndexRoute component={WalletOptions} />
            <Route path="change" component={ChangeActiveWallet} />
            <Route path="change-password" component={WalletChangePassword} />
            <Route path="import-keys" component={ImportKeys} />
            <Route path="brainkey" component={ExistingAccountOptions} />
            <Route path="create" component={WalletCreate} />
            <Route path="delete" component={WalletDelete} />
            <Route path="backup/restore" component={BackupRestore} />
            <Route path="backup/create" component={BackupCreate} />
            <Route path="backup/brainkey" component={BackupBrainkey} />
            <Route path="balance-claims" component={BalanceClaimActive} />
        </Route>

        <Route path="create-wallet" component={WalletCreate} />
        <Route path="create-wallet-brainkey" component={CreateWalletFromBrainkey} />

        <Route path="payment" component={PaymentForm} />

        <Route path="explorer/markets" component={MarketsContainer} />
        <Route path="market/:marketID" component={ExchangeContainer} />
        <Route path="settings" component={SettingsContainer} />
        <Route path="block/:height" component={BlockContainer} />
        <Route path="asset/:symbol" component={AssetContainer} />

        <Route path="create-account" component={LoginSelector}>
            <Route path="wallet" component={CreateAccount} />
            <Route path="password" component={CreateAccountPassword} />
            <Route path="generate" component={CreateNewWalletPassword} />
        </Route>

        <Route path="existing-account" component={ExistingAccount} >
            <IndexRoute component={BackupRestore} />
            <Route path="import-backup" component={ExistingAccountOptions} />
            <Route path="import-keys" component={ImportKeys} />
            <Route path="brainkey" component={Brainkey} />
            <Route path="balance-claim" component={BalanceClaimActive} />
        </Route>

        <Route path="/account/:account_name" component={AccountPage} >
            <IndexRoute component={AccountOverview} />
            <Route path="dashboard" component={AccountOverview} />
            <Route path="assets" component={AccountAssets} />
            <Route path="create-asset" component={AccountAssetCreate} />
            <Route path="update-asset/:asset" component={AccountAssetUpdate} />
            <Route path="vesting" component={AccountVesting} />
            <Route path="permissions" component={AccountPermissions} />
            <Route path="voting" component={AccountVoting} />
            <Route path="orders" component={AccountOrders} />
            <Route path="whitelist" component={AccountWhitelist} />
            <Route path="gateway" component={DEXgateWay} />
            <Route path="bts-gateway" component={BTCGateWay} />
            <Route path="transfer" component={Transfer} />
            <Route path="payment" component={PaymentForm} />
            <Route path="my-team" component={AccountTeam} />
            <Route path="my-orders" component={InvoiceList} />
            <Route path="support" component={Support} />
            <Route path="invoice/:id" component={InvoiceDetail} />
            {/* <Route path="training" component={Training} /> */}
            <Redirect from="overview" to="dashboard" />
        </Route>

        <Route path="/mobile/account/:account_name" component={AccountPageMobile} >
            <IndexRoute component={AccountOverview} />
            <Route path="dashboard" component={AccountOverview} />
            <Route path="assets" component={AccountAssets} />
            <Route path="create-asset" component={AccountAssetCreate} />
            <Route path="update-asset/:asset" component={AccountAssetUpdate} />
            <Route path="vesting" component={AccountVesting} />
            <Route path="permissions" component={AccountPermissions} />
            <Route path="voting" component={AccountVoting} />
            <Route path="orders" component={AccountOrders} />
            <Route path="whitelist" component={AccountWhitelist} />
            <Route path="gateway" component={DEXgateWay} />
            <Route path="bts-gateway" component={BTCGateWay} />
            <Route path="transfer" component={Transfer} />
            <Route path="payment" component={PaymentForm} />
            <Route path="my-team" component={AccountTeam} />
            <Route path="my-orders" component={InvoiceList} />
            <Route path="support" component={Support} />
            <Route path="invoice/:id" component={InvoiceDetail} />
            <Route path="training" component={Training} />
            <Redirect from="overview" to="dashboard" />
        </Route>

        <Route path="create-worker" component={CreateWorker} />

        <Route path="marketplace" component={MarketPlace} />
        <Route path="events" component={Events} />
        <Route path="promo" component={Promo} />
        
        <Route path="events/assembly" component={Assembly} />

        <Route path="/init-error" component={InitError} />
    </Route>
);

export default class Routes extends React.Component {
    render() {
        return <Router history={history} routes={routes} />;
    }
}
