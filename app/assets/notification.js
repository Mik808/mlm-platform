var applicationServerPublicKey = 'BGQ9IKWcQwLg9jEGK5A9Cyk5Q7-AEyFm98Rz-446Dilif9ZDs9GqoTeB_SeWiMMP3wkgomNp1C2hODdNjFfv8VI';

if ('serviceWorker' in navigator && 'PushManager' in window) {
  // console.log('Service Worker and Push is supported');

  navigator.serviceWorker.register('sw.js').then(function (swReg) {
    swRegistration = swReg;

    Notification.requestPermission(function (result) {
      if (result === 'granted') {
        console.log('notifications allowed');
        //      navigator.serviceWorker.ready.then(function(registration) {
        //        const title = 'Right';
        //        registration.showNotification('Right', {
        //          body: 'Ваш баланс изменился',
        //          vibrate: [200, 100, 200, 100, 200, 100, 400],
        //          icon: 'asset-symbols/app-logo/notification192.png',
        //          badge: 'asset-symbols/app-logo/notification72.png'
        //        });
        //      });
      }
    });
  }).catch(function (error) {
    console.error('Service Worker Error', error);
  });
} else {
  console.warn('Push messaging is not supported');
}