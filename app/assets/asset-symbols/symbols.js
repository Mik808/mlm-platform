// Core asset
require("file-loader?name=asset-symbols/[name].png!./ntz.png");

//manifest icons 
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/homescreen48.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/homescreen72.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/homescreen96.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/homescreen144.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/homescreen168.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/apple-touch-icon.png");

require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/notification192.png");

// Landing
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/reg-bg.png");
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/header-bg.png");
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/about-bg.png");
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/about-bg-mobile.png");
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/tech-bg.png");
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/training.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/logo.png");
require("file-loader?name=asset-symbols/app-logo/[name].png!./app-logo/logo_text.png");

//PROMO
require("file-loader?name=asset-symbols/ntz-img/[name].png!./ntz-img/promo-bg.png");

//ICONS
require("file-loader?name=asset-symbols/ntz-img/[name].jpg!./ntz-img/checkbox.jpg");