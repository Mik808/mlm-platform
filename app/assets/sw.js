var CACHE = 'netizens';

self.addEventListener('install', function (e) {
  e.waitUntil(caches.open(CACHE).then(function (cache) {
    return cache.addAll(['index.html']).then(function () {
      return self.skipWaiting();
    });
  }));
});

self.addEventListener('activate', function (event) {
  event.waitUntil(self.clients.claim());
  console.log('Now ready to handle fetches!');
});

self.addEventListener('fetch', function (event) {
  event.respondWith(caches.match(event.request, { ignoreSearch: true }).then(function (response) {
    return response || fetch(event.request);
  }));
});

/* NOTIFIVATIONS*/

var applicationServerPublicKey = 'BGQ9IKWcQwLg9jEGK5A9Cyk5Q7-AEyFm98Rz-446Dilif9ZDs9GqoTeB_SeWiMMP3wkgomNp1C2hODdNjFfv8VI';

function urlB64ToUint8Array(base64String) {
  var padding = '='.repeat((4 - base64String.length % 4) % 4);
  var base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');

  var rawData = window.atob(base64);
  var outputArray = new Uint8Array(rawData.length);

  for (var i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

self.addEventListener('push', function (event) {
  console.log('[Service Worker] Push Received.');
  console.log('[Service Worker] Push had this data: "' + event.data.text() + '"');

  var title = 'Push Netizens';
  var options = {
    body: 'Ваш баланс изменился',
    vibrate: [200, 100, 200, 100, 200, 100, 400],
    icon: 'asset-symbols/app-logo/notification192.png',
    badge: 'asset-symbols/app-logo/notification72.png'
  };
  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', function (event) {
  console.log('[Service Worker] Notification click Received.');
  event.notification.close();
  clients.openWindow('https://ntz.team/');
});
self.addEventListener('pushsubscriptionchange', function (event) {
  console.log('[Service Worker]: \'pushsubscriptionchange\' event fired.');
  var applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  event.waitUntil(self.registration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: applicationServerKey
  }).then(function (newSubscription) {
    // TODO: Send to application server
    console.log('[Service Worker] New subscription: ', newSubscription);
  }));
});