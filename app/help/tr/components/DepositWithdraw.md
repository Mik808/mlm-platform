[# receive]
### Kullanıcı adınız
Bitshares hesabınıza bir başkasından ya da bir borsadan NTZ gönderimi için, göndericiye kullanıcı adınızı vermeniz yeterlidir. NTZ'ler doğrudan kullanıcı adı belirtilerek gönderilir:

[# deposit-short]
### Kripto para yatır/çek
Başka blokzincirlerinden Bitshares hesabınıza (ör. BTC gibi) "coin" aktarmak için ya da TL, dolar gibi fiat para birimleriyle Bitshares'de işlem yapabilmek için lütfen aşağıdan bir transfer hizmet sağlayıcısı seçin.
