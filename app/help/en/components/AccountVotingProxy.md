If you don't fully understand the roles of Witnesses, Committee Members and Workers, you may elect to choose a Proxy. A proxy is an account that you trust to make decisions about the Right network on your behalf.

**Be sure to select publish changes above once you've made your selection**.
